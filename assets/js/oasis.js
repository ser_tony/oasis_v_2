(function() {

    window.Modals = {}
    window.Tooltips = {}

    Modals = (function() {

        Modals.prototype.authModalEvents = {

            enterPasswordChange: function(e) {
                if ($(this).val() !== $(this).data('genaratedpassword')) {
                    $('#passGenWrap')[$(this).val().length > 0 ? 'fadeOut' : 'fadeIn']();
                }
            },

            togglePasswordClick: function(e) {
                e.data.modal.authModalEvents.togglePasswordVisibility($(this).data('target'));
            },

            generatePasswordClick: function(e) {
                var selector = $(this).data('target'), 
                    modal = e.data.modal,
                    textToReplace, 
                    textToReplaceToggle;

                modal.authModalEvents.generatePassword();

                var generatedPassword = $(this).find('span').text();

                $(selector).val(generatedPassword)
                           .data('genaratedpassword', generatedPassword);

                modal.authModalEvents.togglePasswordVisibility(selector, 'text');
                modal.authModalEvents.generatePassword();

                textToReplace = $(this).data('placeholderText');
                textToReplaceToggle = $(this).data('placeholderTextToggle');
                
                $('#togglePassword').html(textToReplace);
                $('#togglePassword').data('toggleText', textToReplaceToggle);
            },

            generatePassword: function() {
                $('#passGen span').html(Math.random().toString(36).slice(-8) + Math.random().toString(36).slice(-8));
            },

            finishRegister: function(e) {
                e.preventDefault();
                if ($('#registerPopup input:invalid').length === 0) {
                    $('.popup_reg').hide();
                    $('.popup_enter').hide();
                    $('#afterReg').show();
                }
            },

            togglePasswordVisibility: function(selector, inputType) {
                var $trigger, triggerOldText, triggerText;
                if (inputType == null) {
                    inputType = void 0;
                }

                $trigger = $('#togglePassword');
                triggerText = $trigger.data('toggleText');
                triggerOldText = $trigger.html();
                if (inputType === void 0) {
                    $trigger.html(triggerText);
                    $trigger.data('toggleText', triggerOldText);
                }
                $(selector).get(0).type = $(selector).get(0).type === 'password' ? inputType || 'text' : inputType || 'password';
            },

            authNavTabClick: function(e) {
                e.preventDefault();

                var modal = e.data.modal, 
                    $targ = $($(this).attr('href'));
                
                modal.$allPopups.hide();
                $targ.show();

                modal.$popupBlock = $targ;

                setTimeout(function() {
                    $('#enterLogin, #enterLogin2').focus();
                }, 500);
                
                return false;
            }
        }

        Modals.prototype.bindEvents = function() {
            var that = this;
            
            $('.js-modalClose').on('click', {modal: this}, this.hide);
            $('.js-modalOpen').on('click', {modal: this}, this.show);
            
            $('.overlay').on('click', {modal: this}, this.hide);
            
            // Немного магадана
            $(document).on('keyup', function(e){
                if (e.keyCode == that.escKey) {
                    // Передаем ссылку на класс. Эмулируем структуру jquery
                    that.hide({data: { modal: that}});
                }
            });
        };

        Modals.prototype.bindAuthModalEvents = function() {

            $('#registerPassword').on('keyup', this.authModalEvents.enterPasswordChange);
            $('#newPassword').on('keyup', this.authModalEvents.enterPasswordChange);
            $('#togglePassword').on('click', { modal: this}, this.authModalEvents.togglePasswordClick);
            $('#passGen').on('click', { modal: this}, this.authModalEvents.generatePasswordClick);

            $('#register').on('click', { modal: this}, this.authModalEvents.finishRegister);
        
            $('.popup .popup__rightLink').on('click', { modal: this}, this.authModalEvents.authNavTabClick);

            this.authModalEvents.generatePassword();

            setTimeout(function() {
                $('#enterLogin, #enterLogin2').focus();
            }, 500);

        };

        Modals.prototype.bindShareModalEvents = function() {
            $('#shareAddUsersPopup .defaultTable_selectable').on('click', 'tr', function(){
                $('#shareAddUsersPopup .selected').removeClass('selected');
                $('#shareAddUsersPopup input[type=checkbox]').prop('checked', false);

                $(this).addClass('selected');
                $(this).find('input[type=checkbox]').prop('checked', true);
            });
        }

        Modals.prototype.cacheElements = function() {
            this.$allPopups = $('.popup');
            this.$overlay = $(".overlay");
        };

        Modals.prototype.show = function(e) {
            var that = e.data.modal;

            var idModal = $(this).data('show-popup');
            if (idModal) {
                that.$popupBlock = $('.' + idModal);
                that._show(idModal);
            }
        };

        Modals.prototype.forceShow = function(idModal, callback) {
            this.$popupBlock = $('.' + idModal);
            if (this.$popupBlock.length !== 0) {
                this._show(idModal);

                if (typeof callback !== 'undefined' && typeof callback == 'function') {
                    callback();
                }
            } else {
                return false;
            }
        };

        Modals.prototype.hide = function(e) {
            var that = e.data.modal;
            if (typeof that.$popupBlock !== 'undefined') {
                that.$popupBlock.hide();
            
                if (that.$popupBlock.attr('id') == 'shareAddUsersPopup') {
                    that.$popupBlock = $('.popup_shareSpreadsheet');
                } else {
                    $('body').css('overflow', 'auto');
                    that.$overlay.hide();
                    that.$allPopups.hide();
                }            
            }
            
        };

        Modals.prototype.forceHide = function(idModal, callback) {
            this.$popupBlock = $('.' + idModal);
            if (this.$popupBlock.length !== 0) {
                $('body').css('overflow', 'auto');
                this.$overlay.hide();
                this.$popupBlock.hide();

                if (typeof callback !== 'undefined' && typeof callback == 'function') {
                    callback();
                }
            } else {
                return false;
            }
        };

        Modals.prototype._show = function(idModal) {
            if ((idModal == 'popup_reg' || idModal == 'popup_changePassword') && this.authEventsNotInit === true) {
                this.authEventsNotInit = false;
                this.bindAuthModalEvents();
            } else if (idModal == 'popup_newUser') {
                this.bindAuthModalEvents();
            }

            if (idModal == 'popup_shareUserAdd') {
                this.bindShareModalEvents();
                setTimeout(function() {
                    $(window).resize();
                }, 350);
            }

            $('body').css('overflow', 'hidden');
            this.$overlay.show();

            if (idModal !== 'popup_shareUserAdd') {
                this.$allPopups.hide();
            }
            
            this.$popupBlock.show();
        };

        function Modals(popupId) {
            this.escKey = 27;
            this.authEventsNotInit = true;
            this.cacheElements();
            this.bindEvents();
        };

        return Modals;

    })();


    Tooltips = (function(){

        Tooltips.prototype.bindEvents = function() {
            $('html').on('click', '.js-tooltipRemove', this.remove);

            $(document).on('keyup', function(e){
                if (e.keyCode == 27) {
                    $('.js-tooltipRemove').trigger('click');
                    
                }
            });
        };

        Tooltips.prototype.create = function(object, type, text) {
            $('.js-tooltipRemove').trigger('click');
            
            var position = $(object).offset(),
                width = $(object).width(),
                template = '<span class="tooltip tooltip-' + type + '" style="left: ' + (position.left + width) + 'px; top: ' + (position.top - 42) + 'px">' + text + ' <a class="js-tooltipRemove tooltip__close" href="javascript:void(0)">&times;</a></span>';

            $('body').before(template);
            $('.tooltip').show();
        };

        Tooltips.prototype.remove = function() {
            $(this).parent().fadeOut(250, function(){
                $(this).remove();
            });
        };


        function Tooltips(tooltipId) {
            this.bindEvents();
        };

        return Tooltips;

    })();

    $(document).ready(function() {

        // Plugins 
        $(".select").chosen();
        $.each($('#footer .chzn-single'), function() {
            var targ = $(this).next();
            targ.css('top', "-" + (targ.height()) + "px");
        });

        if ($.fancybox) {
            $(".various").fancybox({
                maxWidth    : 1400,
                maxHeight   : 800,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none'
            });
        }
//TODO : beaty scroll
//        $('.scroller').baron({
//            bar: '.scroller__bar',
//            barOnCls: 'possibleScroll',
//            direction: 'v'
//        });

        $('#changeEmailButton').on('click', function(){
            $('#changeEmail-step1').removeClass('active');
            $('#changeEmail-step2').addClass('active');
        });

        $('.contentStatic h2').each(function(){
            $(this).wrap('<div class="contentStaticH2" />');
            var width = $(this).width(),
                pad = 260 - width;
            $(this).css('margin-left', pad + 'px');
        });

        // Index page

        $('.scroll-spy-nav a').on('click', function(){
            var id = $(this).attr('ef'),
                topPos;

            topPos = ~~$(id).position()['top'] - 48;
            window.scrollTo(0, topPos);

            return false;
        });

        // Feedback page

        if ($('#feedbackForm').length !== 0) {
            $('#feedbackEmail').focus();
        }

        // Parser page

        if ($(".resizable-handler").length !== 0) {
            if ($('.resizable-handler:eq(0)').length !== 0) {
                $('.resizable-handler:eq(0)').css({
                    position: 'absolute',
                    top: $('.resizable-handler:eq(0)').offset().top + 'px'
                });
            }

            if ($('.resizable-handler:eq(1)').length !== 0) {
                $('.resizable-handler:eq(1)').css({
                    position: 'absolute',
                    top: $('.resizable-handler:eq(1)').offset().top + 'px'
                });
            }

            $(".resizable-handler").draggable({
                axis: "y",
                drag: function(event, ui) {
                    var offset = $(this).offset(),
                        topBlock = $(this).data('resizetop'),
                        bottomBlock = $(this).data('resizebottom'),
                        windowH = $(window).height(),
                        heightTopMin = $('#' + topBlock).data('minh'),
                        heightBottomMin = $('#' + bottomBlock).data('minh');

                    if ($('.resizable:eq(0)').attr('id') == topBlock) {
                        if ($('.resizable-handler:eq(1)').length !== 0) {
                            var heightTop = offset.top - 48,
                                heightBottom = parseInt((($('.resizable-handler:eq(1)').offset().top) - offset.top), 10);
                        } else {
                            var heightTop = offset.top - 48,
                                heightBottom = parseInt((windowH - offset.top), 10);    
                        }
                    } else if ($('.resizable:eq(1)').attr('id') == topBlock) {
                        var heightTop = parseInt(((offset.top - $('.resizable-handler:eq(0)').offset().top)), 10),
                            heightBottom = parseInt(windowH - offset.top, 10);
                    }

                    if (heightTop < heightTopMin) {
                        heightTop = heightTopMin;
                    }

                    if (heightBottom < heightBottomMin) {
                        heightBottom = heightBottomMin;
                    }
                    
                    $('#' + topBlock).css({
                        height: heightTop + 'px',
                        minHeight: heightTop + 'px'
                    });

                    $('#' + bottomBlock).css({
                        height: heightBottom + 'px',
                        minHeight: heightBottom + 'px'  
                    });

                    $('.scroller').trigger('sizeChange');

                    if (heightTop == heightTopMin || heightBottom == heightBottomMin) {
                        //$(this).css('top', (offset.top + 15) + 'px');
                        return false;
                    }
                },

                stop: function() {
                    // Слегка смещаем resizable-handler 
                    var offset = $(this).offset(),
                        topBlock = $(this).data('resizetop'),
                        bottomBlock = $(this).data('resizebottom'),
                        heightTopMin = $('#' + topBlock).data('minh'),
                        heightBottomMin = $('#' + bottomBlock).data('minh');

                    if ($('#' + topBlock).height() == heightTopMin) {
                        $(this).css('top', (offset.top + 5) + 'px');
                    }

                    if ($('#' + bottomBlock).height() == heightBottomMin) {
                        $(this).css('top', (offset.top - 5) + 'px');
                    }         
                }
            });

            $(window).on('resize', function(){
                var windowH = $(window).height();

                if ($('.resizable-handler:eq(0)').length !== 0) {
                    var resizableHandler = $('.resizable-handler:eq(0)'),
                        resizableHandlerData = $(resizableHandler).data();

                    $('.resizable-handler:eq(0)').css('top', $('#' + resizableHandlerData.resizetop).height() + 'px');
                }

                if ($('.resizable-handler:eq(1)').length !== 0) {
                    var resizableHandler = $('.resizable-handler:eq(1)'),
                        resizableHandlerData = $(resizableHandler).data();

                    $('.resizable-handler:eq(1)').css('top', (windowH - $('#' + resizableHandlerData.resizebottom).height()) + 'px');
                }
            });
        }
    
        window.Modals.instance = new Modals();
        window.Tooltips.instance = new Tooltips();

        // TODO Сделать listener для IE
        
        window.addEventListener("offline", function(e) {
            $('#serverStatus').show();
        });

        window.addEventListener("online", function(e) {
            $('#serverStatus').hide();
        });

    });

}).call(this);