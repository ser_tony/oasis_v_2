/*global jQuery, Handlebars */
var edit = false;
jQuery(function ($) {
    'use strict';

    var Utils = {
        store: function (namespace, data) {
            if (arguments.length > 1) {
                return localStorage.setItem(namespace, JSON.stringify(data));
            } else {
                var store = localStorage.getItem(namespace);
                return (store && JSON.parse(store)) || [];
            }
        },

        getItem: function(id) {
            var data = this.store('OasisApp-parserGroups');
            for (var key in data) {
                if (data[key].id == id) {
                    return data[key];
                }
            }

            return false;
        },

        getRule: function(groupid, ruleid) {
            var data = this.store('OasisApp-parserGroups');
            for (var key in data) {
                if (data[key].id == groupid) {
                    for (var k in data[key].rulesList) {
                        if (data[key].rulesList[k].id == ruleid) {
                            return {groupKey: key, ruleKey: k};
                        }
                    }
                }
            }
        },

        cleanData: function(namespace) {
            return localStorage.removeItem(namespace);
        },

        createTestData: function(namespace) {
            var testParserMenu = [{
                id: 1,
                title: 'Новая группа правил',
                type: 1,
                rulesList: [{
                    title: 'Номер заказа',
                    isPaused: false,
                    isError: false
                },{
                    title: 'Клиент',
                    isPaused: true,
                    isError: false 
                },{
                    title: 'Общая цена всех заказов от клиента',
                    isPaused: false,
                    isError: false  
                },{
                    title: 'Доставка',
                    isPaused: false,
                    isError: true
                }]
            }];

            this.store(namespace, testParserMenu);
        }
    };

    var AppParser = {
        init: function () {
            
            this.ENTER_KEY = 13;
            this.ESC_KEY = 27;

            this.parserGroups = Utils.store('OasisApp-parserGroups');
            
            this.cacheElements();
            this.bindEvents();
            this.initGroup();

            this.render();
        },

        cacheElements: function () {
            this.menuList = Handlebars.compile($('#parserGroups-template').html());

            this.ruleForm = Handlebars.compile($('#parserRuleForm-template').html());
            this.ruleMenu = Handlebars.compile($('#parserRuleMenu-template').html());
            this.ruleGroupForm = Handlebars.compile($('#parserRuleGroupForm-template').html());
            
            this.sourceOrderLetterRow = Handlebars.compile($('#sourceOrderLetterRow-template').html());
            this.sourceOrderDefaultRow = Handlebars.compile($('#sourceOrderDefaultRow-template').html());

            this.groupTypes = $('#parserTabs input[name=parserType]');
            this.$navParser = $('#navParser');
            this.$navParserList = $('#navParser__list');
            this.$contentParser = $('#js-parserContent');

            this.$groupRuleForm = $('#parserForm');
            this.$ruleForm = $('#parserRuleForm');
           
        },

        bindEvents: function () {
            $('#parserCreateGroup').on('click', this.startGroupCreate);
            $('html').on('click', '#parserCreateGroupBox .btn_circle_delete', this.cancelGroupCreate);
            $('html').on('click', '#parserCreateGroupBox .btn_circle_edit', function(){

                var parent = $(this).parents('#parserCreateGroupBox');
                var input = $('#newParserGroup', parent);

                input.before('<a class="navParser__groupName js-parserGroupOpen" data-defaultname="Новая группа" id="newParserGroup" href="javascript:void(0)">' + input.val() + '</a>')
                     .remove();
            });

            $('html').on('click', '#js-chooseParserType', this.groupCreate);
           
            $(this.$navParser).on('click', '.navParser__toggle', function(){
                var $li = $(this).parent().parent();
                if ($($li).hasClass('opened')) {
                    $($li).removeClass('opened');
                } else {
                    $($li).addClass('opened');
                }
            });

            $(this.$navParser).on('click', '.btn_circle_edit', this.ruleEdit);
            $(this.$navParser).on('click', '.btn_circle_pause', this.rulePause);
            $(this.$navParser).on('click', '.btn_circle_delete', this.ruleDelete);
            $(this.$navParser).on('click', '.js-addNewRule', this.startRuleCreate);
            $(this.$navParser).on('click', '.js-parserGroupOpen', this.groupShow);
            $('html').on('click', '.navParser__ruleName', this.ruleShow);

            $('html').on('click', '#expessionAfterBlockShow', function(){
                $(this).addClass('hidden');
                $('#expessionAfterBlock, .expessionAfterBlockHide').removeClass('hidden');
            });
            $('html').on('click', '#expessionAfterBlockHide', function(){
                $('#expessionAfterBlock, .expessionAfterBlockHide').addClass('hidden');
                $('#expessionAfterBlockShow').removeClass('hidden');
            });

            $('html').on('click', '.js-addTableRow', {parser: this}, function(e){
                var parserClass = e.data.parser,
                    table = $(this).data('fortable');

                if (table == 'js-sourceOrderLetterTable') {
                    $('#js-sourceOrderLetterTable tbody').append(parserClass.sourceOrderLetterRow);
                } else if (table == 'js-sourceOrderDefaultTable') {
                    $('#js-sourceOrderDefaultTable tbody').append(parserClass.sourceOrderDefaultRow);
                }

                $(".select").chosen();
            });

            $('html').on('click', '.js-deleteTableRow', function(){
                var tableId = $(this).data('fortable'),
                    $tableRows = $('#' + tableId + ' tbody tr'),
                    $tableRow = $(this).parent().parent();

                if ($($tableRows).length >= 2) {
                    $($tableRow).remove();    
                } else {
                    $('.js-addTableRow[data-fortable="' + tableId + '"]').trigger('click');
                    $($tableRow).remove();
                }
            });

            $('html').on('click', '.js-showLetter', this.letterShow);

            var saveTimeout;
            $('#js-parserContent').on('blur', '#parserForm .textbox', {parser: this}, function(e){
                var appParser = e.data.parser;
                appParser.groupUpdate($(this));
            });

            $('#js-parserContent').on('keydown', '#parserForm .textbox', {parser: this}, function(e){
                var appParser = e.data.parser,
                    $input = $(this);

                if (typeof saveTimeout !== 'undefined') {
                    clearTimeout(saveTimeout);
                }
                
                saveTimeout = setTimeout(function(){
                    appParser.groupUpdate($input);
                }, 1000);
            });
        },

        initGroup: function() {
            if (this.parserGroups.length == 0) {
                var templateData = {
                    group: {},
                    isNew: true,
                    isFirst: true
                }
                this.$contentParser.html(this.ruleGroupForm(templateData));    
            } else {
                var group = this.parserGroups[0];
                if (group) {
                    var templateData = {
                        group: group,
                        isNew: false,
                        addType: (group.type == 1) ? true : false,
                        updateType: (group.type == 0) ? true : false
                    }

                    AppParser.$contentParser.html(AppParser.ruleGroupForm(templateData));
                    $('.select').chosen();
                    $('.scroller').trigger('sizeChange');
                }

            }            
        },

        render: function () {
            
            for (var key in this.parserGroups) {
                this.parserGroups[key].typeClass = (this.parserGroups[key].type == 1) ? 'addGroup' : 'updateGroup';
            }

            var templateData = {
                parserGroupsList: this.parserGroups
            }

            this.$navParserList.html(this.menuList(templateData));
        },

        startGroupCreate: function() {
            var $newParserGroup = $('#newParserGroup'),
                isFirst = (AppParser.parserGroups.length == 0) ? true : false,
                templateData = {
                    group: {},
                    isNew: true,
                    isFirst: isFirst
                };

            $('#parserCreateGroupBox').show();
            $newParserGroup.before('<input class="textbox textbox_small" type="text" name="new_group" id="newParserGroup" value="" data-defaultname="Новая группа">')
                           .remove();

            var $newParserGroup = $('#newParserGroup'),
                defaultname = $($newParserGroup).data('defaultname'),
                defaultnameCopy = 1;
            // Ищем похожие группы
            for (var key in AppParser.parserGroups) {
                if ((AppParser.parserGroups[key].title).indexOf(defaultname) !== -1) {
                    defaultnameCopy++;   
                }
            }

            defaultname = defaultname + ' ' + defaultnameCopy;

            $($newParserGroup).val(defaultname).focus().select();

            AppParser.$contentParser.html(AppParser.ruleGroupForm(templateData));

            $('#parserTabs').removeClass('hidden');
        },

        groupCreate: function() {
            if($('#newParserGroup').is('input')){
                $('#parserCreateGroupBox .btn_circle_edit').trigger('click');
            }
            var groupName = $.trim($('#newParserGroup').text()),
                isUnique = true,
                $type = $('#parserTabs input[name=parserType]:checked');

            
            if (groupName == '') {
                groupName = $('#newParserGroup').data('defaultname');
            }

            for (var key in AppParser.parserGroups) {
                if (AppParser.parserGroups[key].title == groupName) {
                    isUnique = false;
                }
            }

            if (isUnique) {
                $($type).addClass('parserType-checked');
                $(this).after('<span class="btn btn_green btn_triangled" style="cursor: default;">Сохранено</span>').remove();

                $('#parserTabs label').addClass('disabledLabel').attr('for', '');

                $('#js-parserGroupForm').removeClass('hidden');
                $('#parserCreateGroupBox').hide();

                var length = AppParser.parserGroups.length,
                    lastID = (length == 0) ? 1 : AppParser.parserGroups[length - 1].id + 1;               

                AppParser.parserGroups.push({
                    id: lastID,
                    title: groupName,
                    type: $($type).val(),
                    rulesList: []
                });

                Utils.store('OasisApp-parserGroups', AppParser.parserGroups);
                // TODO: ajaxSend

                AppParser.render();

                $(".select").chosen();

                setTimeout(function(){
                    /*console.log(AppParser.$navParserList.find('li[data-groupid=' + lastID + '] .navParser__toggle'));*/
                    AppParser.$navParserList.find('li[data-groupid=' + lastID + '] .navParser__toggle').trigger('click');
                    AppParser.$navParserList.find('li[data-groupid=' + lastID + ']').addClass('selectedGroup');
                }, 250);
            } else {
                window.Tooltips.instance.create($('#newParserGroup'), 'error', 'Группа с таким названием уже существует');
            }
        },

        cancelGroupCreate: function() {
            $('#parserCreateGroupBox input').val('');
            $('#parserCreateGroupBox').hide();
        },

        groupShow: function() {
            
            AppParser.cancelGroupCreate();

            $('.selectedRule').removeClass('selectedRule');
            $('.selected').removeClass('selected');
            $('.selectedGroup').removeClass('selectedGroup');
            $(this).parent().parent().addClass('selectedGroup');

            var id = $(this).data('groupid'),
                group = Utils.getItem(id);
            
            if (group) {
                var templateData = {
                    group: group,
                    isNew: false,
                    addType: (group.type == 1) ? true : false,
                    updateType: (group.type == 0) ? true : false
                }

                AppParser.$contentParser.html(AppParser.ruleGroupForm(templateData));
                $('.select').chosen();
                $('.scroller').trigger('sizeChange');
            }
            
        },

        groupUpdate: function(object) {
            $('#savingStatus').html('Сохраняем…')
                              .stop()
                              .fadeIn(150);

            var name = $(object).attr('name'),
                value = $(object).val(),
                id = $('#parserGroupFormID').val();

            for (var key in AppParser.parserGroups) {
                if (AppParser.parserGroups[key].id == id) {
                    if (typeof AppParser.parserGroups[key].letter == 'undefined') {
                        AppParser.parserGroups[key].letter = {}
                        AppParser.parserGroups[key].letter[name] = value;
                    } else {
                        AppParser.parserGroups[key].letter[name] = value;
                    }
                    
                }
            }

            Utils.store('OasisApp-parserGroups', AppParser.parserGroups);
            // TODO Убрать эмуляцию отправки через timeout
            // Сделать ajax запрос, fadeOut делать на success
            setTimeout(function(){
                $('#savingStatus').stop().html('Изменения сохранены');
            }, 500);
            
        },

        groupDelete: function(id) {
            for (var key in AppParser.parserGroups) {
                if (AppParser.parserGroups[key].id == id) {
                    AppParser.parserGroups.splice(key, 1);
                }
            }

            Utils.store('OasisApp-parserGroups', AppParser.parserGroups);
        },

        startRuleCreate: function() {
            var tpl = AppParser.ruleMenu;
            
            $(this).parent().before(tpl());
            var $inputRule = $(this).parent().prev().find('input'),
                groupId = AppParser.$navParserList.find('.opened').data('groupid'),
                defaultname = $inputRule.data('defaultname'),
                defaultnameCopy = 1;
                
            // Ищем похожие правила
            console.log(AppParser.parserGroups[groupId].rulesList);

            for (var key in AppParser.parserGroups) {
                if ((AppParser.parserGroups[key]).id == groupId) {
                    console.log((AppParser.parserGroups[key]).rulesList);
                    for (var ruleKey in (AppParser.parserGroups[key]).rulesList) {
                        if ((AppParser.parserGroups[key].rulesList[ruleKey].title).indexOf(defaultname) !== -1) {
                            defaultnameCopy++;
                        }
                    }
                }
            }

            defaultname = defaultname + ' ' + defaultnameCopy;

            $($inputRule).val(defaultname).focus().select();

            $(AppParser.$contentParser).html(AppParser.ruleForm());
            
            $('.select').chosen();
            $('.scroller').trigger('sizeChange');

            $(this).hide();
        },

        ruleCreate: function() {

        },

        ruleEdit: function(e) {
            var ruleEditButton = this,
                $rule = $(ruleEditButton).parent().prev(),
                $ruleLI = $($rule).parent(),
                $groupLI = $($ruleLI).parent().parent();
            
            if ($($rule).prop('tagName').toLowerCase() == 'a') {
                var ruleName = $($rule).text();
                $($rule).parent().addClass('navParser__status-edited');
                $($rule).before('<input class="navParser__ruleName textbox" type="text" value="' + ruleName + '">')
                        .remove();

                setTimeout(function(){
                    $(window).on('click', function(e){
                        if(!edit){
                            return
                        }
                        e = e || window.event

                        applyEdit(e.target || e.srcElement)

                    }).on('keyup', function(e){
                        ruleEditButton = $('.navParser__ruleName').next().find('.btn_circle_edit');
                        $rule = $(ruleEditButton).parent().prev();

                        if (e.keyCode === 13) {
                            applyEdit()
                        }else if(e.keyCode === 27){
                            $($rule).parent().removeClass('navParser__status-edited');
                            $($rule).before('<a class="navParser__ruleName" href="javascript:void(0)">' + ruleName + '</a>').
                                     remove();
                        }
                    });
                }, 0);
                
                edit = true;
            } else if ($($rule).prop('tagName').toLowerCase() == 'input') {
                applyEdit()
            }  

            function applyEdit(elem){
                console.log(elem)
                if($(elem).hasClass('navParser__ruleName')){
                    return
                }
                console.log(111)
                ruleEditButton = $('.navParser__ruleName').next().find('.btn_circle_edit');
                $rule = $(ruleEditButton).parent().prev(),
                $ruleLI = $($rule).parent(),
                $groupLI = $($ruleLI).parent().parent();

                var ruleName = $.trim($($rule).val());
            
                if (ruleName !== '') {
                    $($rule).parent().removeClass('navParser__status-edited');
                    $($rule).before('<a class="navParser__ruleName" href="javascript:void(0)">' + ruleName + '</a>').
                             remove();

                    if ($($ruleLI).data('ruleid') == 0) {
                        var groupid = $($groupLI).data('groupid');
                        for (var key in AppParser.parserGroups) {
                            if (AppParser.parserGroups[key].id == groupid) {
                                var rules = AppParser.parserGroups[key].rulesList,
                                    nextID = 0;
                                for (var k in rules) {
                                    nextID = Math.max(rules[k].id, nextID);
                                }

                                AppParser.parserGroups[key].rulesList.push({
                                    id: (nextID + 1),
                                    title: ruleName,
                                    isPaused: false,
                                    isError: false
                                });
                            }
                        } 
                    } else {
                        var groupid = $($groupLI).data('groupid'),
                            ruleid = $($ruleLI).data('ruleid');

                        for (var key in AppParser.parserGroups) {
                            if (AppParser.parserGroups[key].id == groupid) {
                                for (var k in AppParser.parserGroups[key].rulesList) {
                                    if (AppParser.parserGroups[key].rulesList[k].id == ruleid) {
                                        AppParser.parserGroups[key].rulesList[k].title = ruleName;
                                    }
                                }
                            }
                        }
                    }

                    $($groupLI).find('.js-addNewRule').show();
                    Utils.store('OasisApp-parserGroups', AppParser.parserGroups);
                } else {
                    window.Tooltips.instance.create($($rule), 'error', 'Введите имя для правила');
                }

                $(window).unbind('click').unbind('keyup');
            }          
        },

        rulePause: function() {

            if ($(this).hasClass('btn_circle_pause-disabled')) {
                return false;
            }

            var ruleEditButton = this,
                $rule = $(ruleEditButton).parent().prev(),
                $ruleLI = $($rule).parent(),
                $groupLI = $($ruleLI).parent().parent(),
                ruleid = $($ruleLI).data('ruleid'),
                groupid = $($groupLI).data('groupid');

            if ($($ruleLI).hasClass('navParser__status-paused')) {
                var isPaused = false;
                $($ruleLI).removeClass('navParser__status-paused');
            } else {
                var isPaused = true;
                $($ruleLI).addClass('navParser__status-paused');
            }

            if (groupid && ruleid) {
                for (var key in AppParser.parserGroups) {
                    if (AppParser.parserGroups[key].id == groupid) {
                        for (var k in AppParser.parserGroups[key].rulesList) {
                            if (AppParser.parserGroups[key].rulesList[k].id == ruleid) {
                                AppParser.parserGroups[key].rulesList[k].isPaused = isPaused;
                            }
                        }
                    }
                } 

                Utils.store('OasisApp-parserGroups', AppParser.parserGroups);
            }

        },

        ruleDelete: function() {
            var ruleEditButton = this,
                $rule = $(ruleEditButton).parent().prev(),
                $ruleLI = $($rule).parent(),
                $groupLI = $($ruleLI).parent().parent();
            
            var groupid = $($groupLI).data('groupid'),
                ruleid = $($ruleLI).data('ruleid');

            for (var key in AppParser.parserGroups) {
                if (AppParser.parserGroups[key].id == groupid) {
                    for (var k in AppParser.parserGroups[key].rulesList) {
                        if (AppParser.parserGroups[key].rulesList[k].id == ruleid) {
                            AppParser.parserGroups[key].rulesList.splice(k, 1);
                        }
                    }
                }
            }

            Utils.store('OasisApp-parserGroups', AppParser.parserGroups);
            $($ruleLI).remove();
            $($groupLI).find('.js-addNewRule').show();

        },

        ruleShow: function() {

            AppParser.cancelGroupCreate();

            var $rule = $(this),
                $ruleLI = $($rule).parent(),
                $groupLI = $($ruleLI).parent().parent();
            
            var groupid = $($groupLI).data('groupid'),
                ruleid = $($ruleLI).data('ruleid');

            $($groupLI).find('.selected').removeClass('selected');
            $('.selectedRule').removeClass('selectedRule');
            $('.selectedGroup').removeClass('selectedGroup');

            $($ruleLI).addClass('selected');
            $($groupLI).addClass('selectedRule');

            var keys = Utils.getRule(groupid, ruleid);
            if (keys != undefined && typeof keys.groupKey !== 'undefined' && typeof keys.ruleKey !== 'undefined') {

                $(AppParser.$contentParser).html(AppParser.ruleForm());
                $('.select').chosen();
                $('.scroller').trigger('sizeChange');
            }
        },

        letterShow: function() {
            // TODO: Полностью заменить аяксом
            var letter = $(this).data('letterid'),
                tpl = Handlebars.compile($('#recentMail' + letter + '-template').html());

            $('#recentMails .js-showLetter.active').removeClass('active');
            $(this).addClass('active');
            $('#parserCode code').html(tpl());

            $('.scroller').trigger('sizeChange');
        }
    };

    AppParser.init();

    $('#tableWithChanges').baron({
        bar: '.scroller__bar_h',
        barOnCls: 'possibleScroll',
        direction: 'h'
    });

});