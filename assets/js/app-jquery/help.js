/*global jQuery, Handlebars */
jQuery(function ($) {
    'use strict';

    window.Oasis.HelpPage = {}
    
    Oasis.HelpPage = {

        init: function() {

            this.linksOffsets = {};
            this.cachedSearch = {};
            
            this.gotoLinkActive = false;
            this.gotoLinkTimeout = false;

            this.cacheElements();
            this.bindEvents();
            this.initMenu();
        },

        cacheElements: function() {
            this.$nav = $('#helpNav__list');
            this.$search = $('#helpSearch');
            this.$content = $('#helpWrapper');

            this.templateHelpMenu = Handlebars.compile($('#helpMenu-template').html());
        },

        bindEvents: function() {
            $(this.$nav).on('click', 'a', this.gotoLink);
            $(this.$content).on('scroll', this.scroller);
            $(this.$search).on('keyup', this.startSearch);

        },

        calculateOffsets: function() {
            var content = this.$content;

            $('h1, h2, h3', content).not('.nosection').each(function(){
                Oasis.HelpPage.linksOffsets[$(this).offset().top - 79] = $(this).attr('id');
            });
        },

        initMenu: function() {
            var content = this.$content,
                oldDeep = 0, deep = 0, counter = 0,
                menu = [];

            $('h1, h2, h3', content).not('.nosection').each(function(){
                var tagName = this.nodeName.toLowerCase();

                if (tagName == 'h1') {

                    menu.push({'text': $(this).text(), 'href': '#' + $(this).attr('id')});
                    counter++;
                }  else if (tagName == 'h2') {
                    if (typeof menu[(counter-1)]['submenu'] == 'undefined') {
                        menu[(counter-1)]['submenu'] = [];
                    }

                    menu[(counter-1)]['submenu'].push({'text': $(this).text(), 'href': '#' + $(this).attr('id')});
                } else if (tagName == 'h3') {
                    var lastLevel2 = menu[(counter-1)]['submenu'].length - 1;
                    if (typeof menu[(counter-1)]['submenu'][lastLevel2]['submenu'] == 'undefined') {
                        menu[(counter-1)]['submenu'][lastLevel2]['submenu'] = [];
                    }
                    menu[(counter-1)]['submenu'][lastLevel2]['submenu'].push({'text': $(this).text(), 'href': '#' + $(this).attr('id')});
                }
            });

            this.renderMenu(menu);
        },

        renderMenu: function(menu) {
            var templateData = {
                helpNav: menu
            };

            this.$nav.html(this.templateHelpMenu(templateData));
            $('.scroller').trigger('sizeChange');
            this.calculateOffsets();
        },

        gotoLink: function() {           
            $(Oasis.HelpPage.$nav).find('.active').removeClass('active');
            $(this).addClass('active');
        },

        startSearch: function(e) {
            if (e.keyCode == 13) {
                $('.highlighted-search').removeClass('highlighted-search');
                Oasis.HelpPage.doSearch(this.value);
            }
        },

        doSearch: function(searchText) {
            if ($.trim(searchText) == '') {
                return;
            }
            
            var c = [],
                b = [];

            if (typeof this.cachedSearch[searchText] == 'undefined') {
                this.cachedSearch[searchText] = [];
                
                while(window.find(searchText, false, false, false, false, false, false)) {
                    var selection = document.getSelection().getRangeAt(0);
                    c.push(selection);
                    cachedSearch[searchText].push(selection);                
                }
            } else {
                c = cachedSearch[searchText];
            }

            for (var a = 0; a < c.length; ++a) {
                var d = c[a],
                    e = document.createElement("span");

                e.className = "highlighted-search";
                d.surroundContents(e);
                b.push(e)
            }
        },

        scroller: function() {
            var scroll = $(this).scrollTop(),
                offsets = [],
                currentMenuPosition = '';
            
            for (var offset in Oasis.HelpPage.linksOffsets) {
                offsets.push(offset);

                if (scroll < offset) {
                    $(Oasis.HelpPage.$nav).find('.active').removeClass('active');
                    $(Oasis.HelpPage.$nav).find('a[href="#' + Oasis.HelpPage.linksOffsets[offsets[offsets.length - 2]] + '"]').addClass('active');
                    break;
                }
            }
        }
    };


    // TODO: Все-таки решить вопрос с подключение скриптов к странице, 
    // возможно через require.js, чтобы не делать вот такие вот костыли
    $(document).ready(function(){
        if ($('#helpWrapper').length !== 0) {
            Oasis.HelpPage.init();            
        }
    });

});