function calcNumberAfterComma(value, number){
	if(value == undefined){
		return '';
	}else if(/\.|,/.test(value)){
		var index = 1;
		var amount = 0;
		var checkValue = '';

		for(var i=0;i<value.length;i++){
			if(value[i] != ',' && value[i] != '.'){
				index++
			}else{
				break;
			}
		}
		amount = number - (value.length - index);

		checkValue = value.replace(/\.|,/g, '')*1;

		if(checkValue != NaN && amount >= 0){
			for(var i=0;i<amount;i++){
				value += 0;
			}
		}else if(checkValue != NaN){
			amount = amount * -1;
			for(var i=0;i<amount;i++){
				value = value.replace(/\d$/, '');
			}
		}else{
			value = NaN;
		}	
	}else{
		if(value*1 != NaN){
			value += ',';
			for(var i=0;i<number;i++){
				value += 0;
			}
		}else{
			value = NaN;
		}	
	}

	return value
}

function formatter(row, cell, value, columnDef, dataContext) {
    return value == undefined ? '' : value;
}

function commaOneZero(row, cell, value, columnDef, dataContext) {
    return calcNumberAfterComma(value, 1);

}
function commaTwoZero(row, cell, value, columnDef, dataContext) {
	return calcNumberAfterComma(value, 2);

}
function commaThreeZero(row, cell, value, columnDef, dataContext) {
	return calcNumberAfterComma(value, 3);

}
function commaFourZero(row, cell, value, columnDef, dataContext) {
	return calcNumberAfterComma(value, 4);

}
