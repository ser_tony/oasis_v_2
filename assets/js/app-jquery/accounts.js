/*global jQuery, Handlebars */
jQuery(function ($) {
    'use strict';

    var Utils = {
        store: function (namespace, data) {
            if (arguments.length > 1) {
                return localStorage.setItem(namespace, JSON.stringify(data));
            } else {
                var store = localStorage.getItem(namespace);

                return (store && JSON.parse(store)) || [];
            }
        },

        cleanData: function(namespace) {
            return localStorage.removeItem(namespace);
        },

        createTestData: function(namespace) {
            var testAccounts = [{
                id: 1,
                firstname: 'Артем',
                lastname: '',
                username: 'Hoxeal',
                email: 'admin@oasis.io',
                lastmessage: new Date('2013-08-22 10:11'),
                isAdmin: true,
                isBlocked: false
            },{
                id: 2,
                firstname: 'Илья',
                lastname: 'Сачев',
                username: 'Redly',
                email: 'ilya.sachev@gmail.com',
                lastmessage: new Date('2013-09-01 19:12:13'),
                isAdmin: false,
                isBlocked: false
            },{
                id: 3,
                firstname: 'Константин',
                lastname: 'Константинопольский',
                username: 'Papa IX',
                email: 'kostantin.konstantinopolskii@gmail.com',
                lastmessage: new Date('2013-07-01 13:18:17'),
                isAdmin: false,
                isBlocked: false
            },{
                id: 4,
                firstname: 'Justin',
                lastname: 'Timberlake',
                username: 'Timberlake',
                email: 'timberlake@myspace.com',
                lastmessage: new Date('2013-08-06 22:01:00'),
                isAdmin: false,
                isBlocked: false
            },{
                id: 5,
                firstname: 'Антон',
                lastname: 'Привольнов',
                username: 'Anthony_x',
                email: 'Anthony_x@yandex.ru',
                lastmessage: new Date('2013-08-14 23:35:11'),
                isAdmin: false,
                isBlocked: true
            }];

            this.store(namespace, testAccounts);

        }
    };

    var AppAccounts = {
        init: function () {
            
            this.ENTER_KEY = 13;
            this.ESC_KEY = 27;

            this.accounts = Utils.store('OasisApp-accounts');

            if (this.accounts.length == 0) {
                Utils.createTestData('OasisApp-accounts');
                this.accounts = Utils.store('OasisApp-accounts');                
            }
            
            this.cacheElements();
            this.bindEvents();
            this.render();
        },

        cacheElements: function () {
            this.templateList = Handlebars.compile($('#accountList-template').html());
            
            this.$accountsList = $('#js-accountsList');
            this.$typeAccountsNav = $('#js-accountsTypes');
            this.$actionsAccountsNav = $('#js-accountsActions');
            this.$checkAllAccounts = $('#js-checkAllAccounts');
        },

        bindEvents: function () {
            this.$accountsList.on('click', 'tr', this.selectRow);
            this.$accountsList.on('click', '.js-userEdit', this.edit);
            this.$accountsList.on('click', 'input[type="checkbox"]', this.selectRowByCheckbox);

            this.$typeAccountsNav.on('click', 'a', this.showList);
            this.$checkAllAccounts.on('change', this.toggleAll);

            $('#js-blockAccountsConfirm').on('click', this.block);
            $('#js-deleteAccountsConfirm').on('click', this.delete);

            $('#js-accountEdit').on('click', this.edit);
            $('#js-editAccountConfirm').on('click', this.update);
            $('#js-newAccountConfirm').on('click', this.create);
        },

        render: function () {
            if (typeof this.filteredAccounts === 'undefined') {
                this.filteredAccounts = this.accounts;
            }

            // TODO Переделать через handlebars helper
            for (var key in this.filteredAccounts) {
                this.filteredAccounts[key].lastMessage_text = moment(this.filteredAccounts[key].lastmessage).format("DD.MM.YYYY в HH:mm");
            }

            var templateData = {
                accountsList: this.filteredAccounts 
            };

            this.$accountsList.html(this.templateList(templateData));
            this.changeMenu();
        },

        changeMenu: function() {
            
            var selectedLength = $(AppAccounts.$accountsList).find('.selected').length;

            if (selectedLength == 1) {
                $(AppAccounts.$actionsAccountsNav).find('.disabled').removeClass('disabled');
                var isBlockedText = $.trim($(AppAccounts.$accountsList).find('.selected .js-isBlocked').text())
                
                if (isBlockedText == 'Нет') {
                    $('#js-accountBlock').html('<span class="icon-block"></span> Заблокировать');
                    $('#js-blockAccountsConfirm').val('Заблокировать');
                    $('#js-blockAccountsText').html('заблокировать');
                } else {
                    $('#js-accountBlock').html('<span class="icon-block"></span> Разблокировать');
                    $('#js-blockAccountsConfirm').val('Разблокировать');
                    $('#js-blockAccountsText').html('разблокировать');
                }

            } else if (selectedLength > 1) {
                $(AppAccounts.$actionsAccountsNav).find('.disabled').removeClass('disabled');
                $('#js-accountEdit').addClass('disabled');

                var isNonBlocked = false;

                $.when(
                    $(AppAccounts.$accountsList).find('.selected').each(function(){
                        var isBlockedText = $.trim($(this).find('.js-isBlocked').text());
                        if (isBlockedText == 'Нет') {
                            isNonBlocked = true;
                        }
                    })
                ).then(function(){
                    if (isNonBlocked === true) {
                        $('#js-accountBlock').html('<span class="icon-block"></span> Заблокировать');
                        $('#js-blockAccountsConfirm').val('Заблокировать');
                        $('#js-blockAccountsText').html('заблокировать');
                    } else {
                        $('#js-accountBlock').html('<span class="icon-block"></span> Разблокировать');
                        $('#js-blockAccountsConfirm').val('Разблокировать');
                        $('#js-blockAccountsText').html('разблокировать');
                    }
                });
            } else {
                $(AppAccounts.$actionsAccountsNav).find('a').each(function(){
                    $(this).addClass('disabled');
                });
                $('#js-accountBlock').html('<span class="icon-block"></span> Заблокировать');
                $('#js-blockAccountsConfirm').val('Заблокировать');
                $('#js-blockAccountsText').html('заблокировать');
            }
        },

        // Controller action 
        // Ниже описаны методы, которые были бы помещены в привычные для MVC модели. Это работа с данными: создание, редактирование, удаление
        create: function (e) {
            e.preventDefault();

            var postData = {
                firstname: 'Не указано',
                username: $('#registerLogin').val(),
                email: $('#registerEmail').val()
            };

            var length = AppAccounts.accounts.length,
                lastID = AppAccounts.accounts[length - 1].id + 1;

            AppAccounts.accounts.push({
                id: lastID,
                firstname: postData.firstname,
                lastname: '',
                username: postData.username,
                email: postData.email,
                lastmessage: new Date(),
                isAdmin: false,
                isBlocked: false
            });

            // TODO Тут (вместо Utils.store) потом должен быть ajax запрос
            // Посылать все данные формы
            Utils.store('OasisApp-accounts', AppAccounts.accounts);

            $('#newUserPopup .js-modalClose').trigger('click');
            AppAccounts.render();
        },

        edit: function () {

            if ($(this).hasClass('disabled')) {
                return false;
            }

            var id = ($(this).hasClass('js-userEdit')) ? $(this).data('id') : $(AppAccounts.$accountsList).find('.selected').data('id'),
                user = AppAccounts.getAccount(id);

            $('#editAccount-login').val(user.username);
            $('#editAccount-email').val(user.email);
            $('#editAccount-name').val(user.firstname + ' ' + user.lastname);

            window.Modals.instance.forceShow('popup_editUser');
        },

        update: function (e) {
            e.preventDefault();

            var id = $(AppAccounts.$accountsList).find('.selected').data('id'),
                postData = {
                    firstname: $('#editAccount-name').val().split(' ')[0],
                    lastname: $('#editAccount-name').val().split(' ')[1],
                    username: $('#editAccount-login').val(),
                    email: $('#editAccount-email').val(),
                };

            for (var i = 0 in AppAccounts.accounts) {
                if (AppAccounts.accounts[i].id == id) {
                    // TODO Отрефакторить и убрать
                    // Потом нужна будет только postData
                    AppAccounts.accounts[i].firstname = $('#editAccount-name').val().split(' ')[0];
                    AppAccounts.accounts[i].lastname = $('#editAccount-name').val().split(' ')[1];
                    AppAccounts.accounts[i].username = $('#editAccount-login').val();
                    AppAccounts.accounts[i].email = $('#editAccount-email').val();
                }
            }

            // TODO Тут (вместо Utils.store) потом должен быть ajax запрос
            // Посылать id через запятую или объект с id
            Utils.store('OasisApp-accounts', AppAccounts.accounts);

            $('#editUserPopup .popup__close').trigger('click');
            AppAccounts.render();
        },

        block: function() {
            var ids = [],
                isNonBlocked = false;

            $.when(
                $(AppAccounts.$accountsList).find('.selected').each(function(){
                    ids.push($(this).data('id'));
                    var isBlockedText = $.trim($(this).find('.js-isBlocked').text());
                    if (isBlockedText == 'Нет') {
                        isNonBlocked = true;
                    }
                })
            ).then(function(){
                for (var i = 0 in AppAccounts.accounts) {
                    if (ids.indexOf(AppAccounts.accounts[i].id) !== -1) {
                        // Работает как toggle. Если был заблокирован, то разблокируем, если нет, то блокируем …
                        // AppAccounts.accounts[i].isBlocked = (AppAccounts.accounts[i].isBlocked == true) ? false : true;
                        if (isNonBlocked === true) {
                            AppAccounts.accounts[i].isBlocked = true;
                        } else {
                            AppAccounts.accounts[i].isBlocked = false;
                        }
                    }
                }
                
                // TODO Тут (вместо Utils.store) потом должен быть ajax запрос
                // Посылать id через запятую или объект с id
                Utils.store('OasisApp-accounts', AppAccounts.accounts);
                
                $('#blockPopup .popup__close').trigger('click');
                AppAccounts.render();
            });
        },

        delete: function () {
            var ids = [];

            $.when(
                $(AppAccounts.$accountsList).find('.selected').each(function(){
                    ids.push($(this).data('id'));
                })
            ).then(function(){

                for (var i = 0 in AppAccounts.accounts) {
                    if (ids.indexOf(AppAccounts.accounts[i].id) !== -1) {
                        (AppAccounts.accounts).splice(i, 1);
                    }
                }
                
                // TODO Тут (вместо Utils.store) потом должен быть ajax запрос
                // Посылать id через запятую или объект с id
                Utils.store('OasisApp-accounts', AppAccounts.accounts);
                
                $('#deletePopup .popup__close').trigger('click');

                AppAccounts.render();
            });
        },

        view: function() {

        },

        getList: function(type) {
            var possibleValues = ["all", "admins", "users", "blocked"],
                list = this.accounts,
                result = [];

            if (typeof type === 'undefined' || possibleValues.indexOf(type) === -1) {
                var type = 'all';
            }

            if (type == 'all') {
                result = list;
            } else {

                for (var key in list) {
                    if (type == 'blocked' && list[key].isBlocked === true) {
                        result.push(list[key]);
                    } else if (type == 'admins' && list[key].isAdmin === true && list[key].isBlocked === false) {
                        result.push(list[key]);
                    } else if (type == 'users' && list[key].isAdmin == false && list[key].isBlocked === false) {
                        result.push(list[key]);
                    }
                }
            }

            return result;
        },

        showList: function() {
            var type = $(this).data('type');

            $(AppAccounts.$typeAccountsNav).find('.active').removeClass('active');
            $(this).addClass('active');

            AppAccounts.filteredAccounts = AppAccounts.getList(type);
            AppAccounts.render();
        },

        getAccount: function(id) {
            var account = {};
            for (var i = 0 in this.accounts) {
                if (this.accounts[i].id == id) {
                    account = this.accounts[i];
                    break;
                }
            }

            return account;
        },

        toggleAll: function() {
            var isChecked = $(this).prop('checked');

            if (isChecked) {
                $(AppAccounts.$accountsList).find('input[type="checkbox"]').prop('checked', true);
                $(AppAccounts.$accountsList).find('tr').addClass('selected');
            } else {
                $(AppAccounts.$accountsList).find('input[type="checkbox"]').prop('checked', false);
                $(AppAccounts.$accountsList).find('.selected').removeClass('selected');
            }

            AppAccounts.changeMenu();
        },

        selectRow: function(e) {
            var $rows = $(AppAccounts.$accountsList).find('tr'),
                $active = $(AppAccounts.$accountsList).find('.active');

            if (!(e.metaKey || e.ctrlKey || e.shiftKey) && !$(this).hasClass('selected')) {
                $(AppAccounts.$accountsList).find('input[type="checkbox"]').prop('checked', false);
                $(AppAccounts.$accountsList).find('.selected').removeClass('selected');
            } else if (e.shiftKey) {

                var indexNext = $($rows).index($(this)),
                    indexPrev = $($rows).index($($active));

                var slice = $($rows).slice(Math.min(indexPrev, indexNext), Math.max(indexPrev, indexNext));

                $($active).removeClass('active');
                $(this).addClass('selected').addClass('active');
                $(this).find('input[type="checkbox"]').prop('checked', true);

                $(slice).each(function(){
                    $(this).addClass('selected');
                    $(this).find('input[type="checkbox"]').prop('checked', true);
                });
            }
            
            if (!e.shiftKey) {
                $($active).removeClass('active');
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');    
                    $(this).find('input[type="checkbox"]').prop('checked', false);
                } else {
                    $(this).addClass('selected').addClass('active');
                    $(this).find('input[type="checkbox"]').prop('checked', true);
                }    
            }
            
            AppAccounts.changeMenu();
        },

        selectRowByCheckbox: function(e){
            e.stopPropagation();

            var isChecked = $(this).prop('checked'),
                $active = $(AppAccounts.$accountsList).find('.active'),
                $tr = $(this).parent().parent();
                
            $($active).removeClass('active');

            if (isChecked) {
                $($tr).addClass('selected').addClass('active');
            } else {
                $($tr).removeClass('selected');
            }

            AppAccounts.changeMenu();
        }
    };

    AppAccounts.init();

});
