var conditionalFormattingArray = [];
var grid;

(function() {
	var allTableArray = [];
	var arrayHideShowColumn = [];
	var arrayHiddenColumn = [];
	var cellCopy = '';
	var cellCut = '';
	var colorCellArray = {};
	var commentsArray = {};
	var noteArray = [];
	var columnFormatter = '';
	var colorArray =  'bgcolor1 bgcolor2 bgcolor3 bgcolor4 bgcolor5'
					+ 'bgcolor6 bgcolor7 bgcolor8 bgcolor9 bgcolor10'
					+ 'bgcolor11 bgcolor12 bgcolor13 bgcolor14 bgcolor15'
					+ 'bgcolor16 bgcolor17 bgcolor18 bgcolor19 bgcolor20'
					+ 'bgcolor21 bgcolor22 bgcolor23 bgcolor24 bgcolor25'
					+ 'bgcolor26 bgcolor27 bgcolor28 bgcolor29 bgcolor30'
					+ 'bgcolor31 bgcolor32 bgcolor33 bgcolor34 bgcolor35'
					+ 'bgcolor36 bgcolor37 bgcolor38 bgcolor39 bgcolor40'
					+ 'bgcolor41 bgcolor42 bgcolor43 bgcolor44 bgcolor45'
					+ 'bgcolor46 bgcolor47 bgcolor48 bgcolor49 bgcolor50'
					+ 'bgcolor51 bgcolor52 bgcolor53 bgcolor54 bgcolor55'
					+ 'bgcolor56 bgcolor57 bgcolor58 bgcolor59 bgcolor60'
					+ 'bgcolor61 bgcolor62 bgcolor63 bgcolor64 bgcolor65'
					+ 'bgcolor66 white_bg green_bg pink_bg brawn_bg yellow_bg blue_bg';
	var fontColorArray =  'color1 color2 color3 color4 color5'
						+ 'color6 color7 color8 color9 color10'
						+ 'color11 color12 color13 color14 color15'
						+ 'color16 color17 color18 color19 color20'
						+ 'color21 color22 color23 color24 color25'
						+ 'color26 color27 color28 color29 color30'
						+ 'color31 color32 color33 color34 color35'
						+ 'color36 color37 color38 color39 color40'
						+ 'color41 color42 color43 color44 color45'
						+ 'color46 color47 color48 color49 color50'
						+ 'color51 color52 color53 color54 color55'
						+ 'color56 color57 color58 color59 color60'
						+ 'color61 color62 color63 color64 color65'
						+ 'color66';

	function arrayToArray(from){
		var to = [];

		for(var i=0;i<from.length;i++){
			to.push(from[i]);
		}

		return to
	}

	$('#tableTabs a').on('click', function(){
		if($(this).hasClass('active')){
			return false
		}
		$('#tableTabs .active').removeClass('active');
		$(this).addClass('active');
		$('#tableLeftColumn').toggleClass('view_actions');

		if($(this).hasClass('tableTabs__actions active')){
			$('.scroller_actions').baron({
				bar: '.scroller_active__track',
				barOnCls: 'possibleScroll',
				direction: 'v'
			});
		}

    	return false;
	});

	$('#showFilter').on('click', function(){
		$('#tableBody').toggleClass('show_filter');
		if($('#tableBody').hasClass('show_filter')){
			$('.slick-viewport').height($('.slick-viewport').height()-53)
		}else{
			$('.slick-viewport').height($('.slick-viewport').height()+53)
		}
	}); 

	$('#tableTabsSwitcher, #js-hideTablePanels').on('click', function(){
		$('#tableLeftColumn, .layoutWithColumn__wrapper').toggleClass('active');
		if($('#tableLeftColumn.active').length){
			$('.slick-viewport').width($('.slick-viewport').width()+255);
		}else{

			$('.slick-viewport').width($('.slick-viewport').width()-255);
		}
	});




/*****************************************************************************************
	function for add new class name
*****************************************************************************************/

	function addNewStyleClass(newClassName, removeClassName){
		var selected = grid.getActiveCell();
		var items = dataView.getItems();		
		var columnName = grid.getColumns()[parseInt(selected.cell)].name;
		var styleClassCellArray = {};
		var date = new Date();

		styleClassCellArray[selected.row] = styleClassCellArray[selected.row] ? styleClassCellArray[selected.row] : {};
		styleClassCellArray[selected.row][selected.cell] = newClassName;
		
		$('.slick-cell.selected').removeClass(removeClassName);

		grid.setCellCssStyles(date.getTime(), styleClassCellArray);	
	}


	$('#addNewTable, .js-addNewTable').on('click', function(){
		var columns = grid.getColumns();
		var index = $('.table_list.active').index()-1;

		var dataView;
		var data = [];
		var options = {
		    editable: true,
		    enableCellNavigation: true,
		    showHeaderRow: true,
		    headerRowHeight: 30,
		    explicitInitialization: true,
		    enableColumnReorder: true,
			autoEdit: false,
			enableAsyncPostRender:	true
		};

		allTableArray[index] = grid.getData();
	  

		function filter(item) {
		    for (var columnId in columnFilters) {
		      if (columnId !== undefined && columnFilters[columnId] !== "") {
		        var c = grid.getColumns()[grid.getColumnIndex(columnId)];
		        if (item[c.field] != columnFilters[columnId]) {
		          return false;
		        }
		      }
		    }
		    return true;
		}

	    var groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider();
		dataView = new Slick.Data.DataView({
			groupItemMetadataProvider: groupItemMetadataProvider,
			inlineFilters: true
		});
	    // prepare the data
	    for (var i = 0; i < 100; i++) {
			var d = (data[i] = {});

			d["id"] = i;
			d["num"] = i+1;
	    }

	    dataView = new Slick.Data.DataView();
	    grid = new Slick.Grid("#tableBody", dataView, columns, options);

	    // register the group item metadata provider to add expand/collapse group handlers
	    grid.registerPlugin(groupItemMetadataProvider);
	    grid.setSelectionModel(new Slick.CellSelectionModel());

		grid.onContextMenu.subscribe(function (e) {
			e.preventDefault();
			var cell = grid.getCellFromEvent(e);
			$("#contextMenu")
				.data("row", cell.row)
				.css("top", e.pageY)
				.css("left", e.pageX)
				.show();

			$("body").one("click", function () {
				$("#contextMenu").hide();
			});
		});

		dataView.onRowCountChanged.subscribe(function (e, args) {
			grid.updateRowCount();
			grid.render();
		});

		dataView.onRowsChanged.subscribe(function (e, args) {
			grid.invalidateRows(args.rows);
			grid.render();
		});

		grid.onClick.subscribe(function(e, args) {
			var columnName = grid.getColumns()[args.cell].id;
			var seletcNode = grid.getCellNode(args.row, args.cell);
		    $('#noteTooltip').hide();

		    if(/\sbgcolor\d/.test($(seletcNode).attr('class'))){
		    	var className = $(seletcNode).attr('class').split(/\s/);
		    	var index = 0;

		    	for(var i=0;i<className.length;i++){
		    		if(/bgcolor/.test(className[i])){
		    			index = className[i].replace(/\D/g, '');
		    		}
		    	}
		    	$('.sp-thumb-el').removeClass('sp-thumb-active');
		    	var color = $('.sp-thumb-el').eq(index*1-1).addClass('sp-thumb-active').attr('data-color');

		    	$('#setBgColor .sp-preview-inner').attr('data-id', index).css('background-color', color);
		    	$('#setBgColor input[type="checkbox"]')[0].checked = true;
		    }else{
		    	$('.sp-thumb-el').removeClass('sp-thumb-active');
		    	var color = $('.sp-thumb-el').eq(60).addClass('sp-thumb-active').attr('data-color');

		    	$('#setBgColor .sp-preview-inner').attr('data-id', 60).css('background-color', color);
		    	$('#setBgColor input[type="checkbox"]')[0].checked = false;
		    }

		    if(/\scolor\d/.test($(seletcNode).attr('class'))){
		    	var className = $(seletcNode).attr('class').split(/\s/);
		    	var index = 0;

		    	for(var i=0;i<className.length;i++){
		    		if(/color/.test(className[i])){
		    			index = className[i].replace(/\D/g, '');
		    		}
		    	}
		    	$('.sp-thumb-el').removeClass('sp-thumb-active');
		    	var color = $('.sp-thumb-el').eq(index*1-1).addClass('sp-thumb-active').attr('data-color');

		    	$('#setFontColor .sp-preview-inner').attr('data-id', index).css('background-color', color);
		    	$('#setFontColor input[type="checkbox"]')[0].checked = true;
		    }else{
		    	$('.sp-thumb-el').removeClass('sp-thumb-active');
		    	var color = $('.sp-thumb-el').eq(60).addClass('sp-thumb-active').attr('data-color');

		    	$('#setFontColor .sp-preview-inner').attr('data-id', 60).css('background-color', color);
		    	$('#setFontColor input[type="checkbox"]')[0].checked = false;
		    }

		    if(/comment/.test(columnName)){
		    	var cord = grid.getCellNodeBox(args.row, args.cell);
	    		var myTxt = '';
		    	if(commentsArray[args.cell] != undefined && 
		    		commentsArray[args.cell][args.row] != undefined){

		    		var row = commentsArray[args.cell][args.row];

		    		for(var i=0;i<row.length;i++){

		    			myTxt += '<div class="comment_row">'
		            		+ 	'<strong class="comment-author">' + row[i].author + '</strong>'
			            	+ 	'<span class="comment-date">' + row[i].date + '</span>'
			            	+ 	'<p class="comment-text">' + row[i].text + '</p>'		            		
							+   '<div class="comment-button">'
							+       '<a class="btn_circle_edit" href="javascript:void(0)"></a>'
							+       '<a class="btn_circle_delete" href="javascript:void(0)"></a>'
							+   '</div>'
			        		+'</div>';

		    		}

		    		$('#commentTooltip .comment_row__wrapper').html(myTxt);
			    	
		    	}else{
		    		$('#commentTooltip .comment_row__wrapper').html('');
		    	}
		    	

		    	$('#commentTooltip').css({'top' : 146 + cord.top, 
		    		'left' : $('#tableBody').offset().left + cord.left - 245, 
		    		'display':'block'
		    	});
		    
		    }else if($(grid.getCellNode(args.row, args.cell)).hasClass('cell_note')){
		    	var cord = grid.getCellNodeBox(args.row, args.cell);
		    	$('#addRemoveNote').addClass('active');

		    	for(var i=0;i<noteArray.length;i++){
					if(noteArray[i].row == args.row && noteArray[i].cell == args.cell){
						$('#noteTooltip').css({
							'top' : 146 + cord.top, 
							'left' : $('#tableBody').offset().left + cord.left + cord.width, 
							'display':'block'
						}).html(noteArray[i].text);
					}
				
				}
		    }else{
		    	$('#addRemoveNote').removeClass('active');
		    }

		});


		$(grid.getHeaderRow()).delegate(":input", "change keyup", function (e) {
			var columnId = $(this).data("columnId");
			if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				dataView.refresh();
			}
		});

		grid.onHeaderRowCellRendered.subscribe(function(e, args) {
			$(args.node).empty();
			$("<input type='text'>")
				.data("columnId", args.column.id)
				.val(columnFilters[args.column.id])
				.appendTo(args.node);
		});

		grid.init();

		dataView.beginUpdate();
		dataView.setItems(data);
		dataView.setFilter(filter);
		dataView.endUpdate();

		arrayHiddenColumn = grid.getColumns();
		
		$('.slick-viewport').append('<div class="scroller_table_body__track"><div class="scroller__bar"></div></div><div class="scroller__bar scroller__bar_h"></div>');

		$('.slick-viewport').baron({
	        bar: '.scroller_table_body__track',
	        barOnCls: 'possibleScroll',
	        direction: 'v'
	    });	   
	    $('.slick-viewport').baron({
	        bar: '.scroller__bar_h',
	        barOnCls: 'possibleScroll',
	        direction: 'h'
	    });

	    $('.table_list.active').removeClass('active').after('<div class="table_list active">лист ' + (index+2) + '</div>')
		index = index + 1 - 2;
		
		if(index >= 0){
			
			$('.table_other:eq(0)').addClass('show');
			$('.table_list').eq(index).css('margin-left', -100500);
		}

		return false
	});

	$('#controler').on('click', '.table_list', function(){
		var index = $(this).index()-1;
		var indexNow = $('.table_list.active').css('margin-left',-100500).index()-1;

		$('.table_list').removeClass('active');
		$(this).addClass('active');

		allTableArray[indexNow] = grid.getData();
		if(indexNow == $('.table_list').length-1){
			$('.table_other:eq(1)').addClass('show');
		}else if(index == $('.table_list').length-1){
			$('.table_other:eq(1)').removeClass('show');
		}
		if(index == 0 || index == 1){
			$('.table_other:eq(0)').removeClass('show');
		}else{
			$('.table_other:eq(1)').addClass('show');
		}
		if(index != 0){
			$('.table_list').eq(index-1).css('margin-left',20);
		}
		grid.setData(allTableArray[index]);
		grid.render();
	});

	$('.table_prev').on('click', function(){
		var active = $('.table_list.active');
		var length = $('.table_list').length;
		var index = active.index();
		var difference = length - index;

		if(index == 1){
			return
		}

		active.removeClass('active').css('margin-left', -100500)
		.prev().addClass('active')
		.prev().css('margin-left', 20);

		if(difference >= 0){
			$('.table_other:eq(1)').addClass('show');
			if(index < 4){
				$('.table_other:eq(0)').removeClass('show')
			}
		}

		allTableArray[index-1] = grid.getData();

		grid.setData(allTableArray[index-2]);
		grid.render();
	})
	$('.table_next').on('click', function(){
		var active = $('.table_list.active');
		var length = $('.table_list').length;
		var index = active.index();
		var difference = length - index;
		
		if(index == length){
			return
		}

		active.removeClass('active').next().addClass('active').css('margin-left', 20);

		active.prev().css('margin-left', -100500);

		if(index > 1){
			$('.table_other:eq(0)').addClass('show');
			
		}
		if($('.table_list.active').index()-1 == length-1 || $('.table_list.active').index()-2 == length-2){
			$('.table_other:eq(1)').removeClass('show');
		}else{
			$('.table_other:eq(1)').addClass('show');
		}

		allTableArray[index-1] = grid.getData();

		grid.setData(allTableArray[index]);
		grid.render();
	})
/*****************************************************************************************
	set comment row
*****************************************************************************************/

	$('#newComment').on('click', function(){
		var columns = grid.getColumns();		
		var index = grid.getActiveCell().cell;
		var newColumn = {
			'cssClass' : 'comment_column__cell',
			'defaultSortAsc' : true,	
			'field' : "Comment" + columns.length,
			'focusable' : true,
			'headerCssClass' : 'comment_column',
			'id' : columns.length,
			'minWidth' : 30,
			'name' : "Comment" + columns.length,
			'rerenderOnResize' : false,
			'resizable' : true,
			'selectable' : true,
			'sortable' : false,
			'width' : 300
		};

		if($('.slick-row.active').length){	
			columns.splice(index+1, 0, newColumn);

		}else{
			columns.push(newColumn);

		}	

		grid.setColumns(columns);

		return false;
	});


/*****************************************************************************************
	add new comment for comment row
*****************************************************************************************/

	$('#addCommentButton').on('click', function(){
		var selected = grid.getActiveCell();
		var dateObj = new Date(); 
		var date = dateObj.toString().replace(/^\w+\s|\sGMT.{5}|\s\d{4}/g, '').replace(/:\d{2}$/,'');
		var items = dataView.getItems();
		var rowName = grid.getColumns()[selected.cell].name;
		var val = $('#commentTooltipText').val();

		if(!commentsArray[selected.cell]){
			commentsArray[selected.cell] = [];
		}

		if(!commentsArray[selected.cell][selected.row]){
			commentsArray[selected.cell][selected.row] = [];
		}

		commentsArray[selected.cell][selected.row].push({
			'author' : 'administrator',
			'date' : date,
			'text' : val
		});

		items[selected.row][rowName] = val;
		grid.updateCell(selected.row, selected.cell);

		$('#commentTooltip').fadeOut(500);


		return false
		
	});

	$('#commentTooltip .js-modalClose').unbind('click').on('click', function(){
		$('#commentTooltip').fadeOut(500);

		return false
	});


/*****************************************************************************************
	set\remove color background for table cell
*****************************************************************************************/

	$('#setColorWrapper').on('click', 'a', function(){
		var color = $(this).attr('data-id');		

		addNewStyleClass(color, colorArray);

		return false;
	});

	$('#setBgColor label').on('click', function(){
		if($('#fontColor')[0].checked == false){
			var color = $('#setBgColor .sp-preview-inner').attr('data-id');
			color = color *1+1;		

			addNewStyleClass('bgcolor' + color, colorArray);

			$('#setBgColor input[type="checkbox"]')[0].checked = true;
		}else{
			addNewStyleClass('', colorArray);
			$('#setBgColor input[type="checkbox"]')[0].checked = false;
		}
		

		return false;
	});


	$('#setFontColor label').on('click', function(){
		if($('#textColor')[0].checked == false){
			var color = $('#setFontColor .sp-preview-inner').attr('data-id');
			color = color *1+1;		

			addNewStyleClass('color' + color, fontColorArray);

			$('#setFontColor input[type="checkbox"]')[0].checked = true;
		}else{
			addNewStyleClass('', fontColorArray);
			$('#setFontColor input[type="checkbox"]')[0].checked = false;
		}
		

		return false;
	});

/*****************************************************************************************
	set lock\onlock for table cell
*****************************************************************************************/

	$('#setLockCell').on('click', function(){

		addNewStyleClass('lock_cell', '')

		return false;
	});

	$('#setUnlockCell').on('click', function(){	

		addNewStyleClass('', 'lock_cell')

		return false;
	});

/*****************************************************************************************
	set\remove border for table cell
*****************************************************************************************/

	$('#setTotalBorder').on('click', function(){
		var left = [];
		var top = [];

		var minLeft = 0;
		var maxLeft = 0;
		var minTop = 0;
		var maxTop = 0;
		
		tempSelectedCell = $('.slick-cell.selected');

		tempSelectedCell.each(function(){
			left.push(this.className.split(/\s/)[1].replace(/l/, ''));
			top.push($(this).parent().index());
		});

		for(var i=0;i<left.length;i++){
			if(i == 0){
				minLeft = maxLeft = left[i];
				minTop = maxTop = top[i];
			}else{
				if(minLeft > parseInt(left[i])){
					minLeft = parseInt(left[i]);
				}
				if(maxLeft < parseInt(left[i])){
					maxLeft = parseInt(left[i]);
				}
				if(minTop > parseInt(top[i])){
					minTop = parseInt(top[i]);
				}
				if(maxTop < parseInt(top[i])){
					maxTop = parseInt(top[i]);
				}
			}				
		}
		
		minLeft = minLeft;
		maxLeft = maxLeft + 1;

		for(var i=minLeft;i<maxLeft;i++){
			grid.setActiveCell(i, i);
			var selected = grid.getActiveCell();
			console.log(selected)
			addNewStyleClass('borderTop', '')
			/*$('.slick-cell.selected.l' + i + '.r' + i).eq(0).addClass('borderTop');
			$('.slick-row').eq(maxTop).find('.slick-cell.selected.l' + i + '.r' + i).eq(0).addClass('borderBottom');*/
		}
		$('.slick-cell.selected.l' + minLeft + '.r' + minLeft).addClass('borderLeft');
		$('.slick-cell.selected.l' + (maxLeft-1) + '.r' + (maxLeft-1)).addClass('borderRight');
	})


/*****************************************************************************************
	edit events for table cell
*****************************************************************************************/

	$('#cellCopy, #contextCellCopy').on('click', function(){
		cellCopy = $('.slick-cell.active').text();
	});

	$('#cellPaste, #contextCellPaste').on('click', function(){
		var selected = grid.getActiveCell();
		var items = dataView.getItems();
		var rowName = grid.getColumns()[selected.cell].name;

		if(cellCopy.length){
			items[selected.row][rowName] = cellCopy;
			grid.updateCell(selected.row, selected.cell);

			cellCopy = '';

		}else if(cellCut.length){
			items[selected.row][rowName] = cellCut[1];
			grid.updateCell(selected.row, selected.cell);

			items[cellCut[0].row][rowName] = '';
			grid.updateCell(cellCut[0].row, cellCut[0].cell);

			cellCut = '';
		}
	});
	
	$('#cellCut').on('click', function(){
		cellCut = [grid.getActiveCell(), $('.slick-cell.active').text()];
	});


/*****************************************************************************************
	font style events for table cell\row
*****************************************************************************************/

	$('#setFontStyle').on('click', 'a', function(){
		var style = $(this).attr('data-id')
		$(this).toggleClass('active');

		if($(this).hasClass('active')){
			addNewStyleClass(style, '');
		}else{
			addNewStyleClass('', style);
		}

		return false;
	});

	$('#setAlignStyle').on('click', 'a', function(){
		
		addNewStyleClass($(this).attr('data-id'), 'text_left text_center text_right')

		return false;
	});

	$('body')
		.on('click', '#setFontSize_chzn li', function(){
			var removeStyle = 'font_size8 font_size9 font_size10 font_size11 font_size12 font_size14 font_size16 font_size18 font_size20 font_size22 font_size24 font_size26 font_size28 font_size36 font_size48 font_size72';
			
			addNewStyleClass('font_size'+$(this).text(), removeStyle);
		})
		.on('click', '#setFontFamily_chzn li', function(){
			var addStyle = $(this).text() == 'По умолчанию' ? 'font_family__Segoe_UI' : 'font_family__' + $(this).text().replace(/\s/g, '');
			var removeStyle = 'font_family__SegoeUI font_family__Arial font_family__ComicSansMS font_family__CourierNew font_family__Georgia font_family__Impact font_family__Tahoma font_family__TrebuchetMS font_family__TimesNewRoman font_family__Verdana';

			addNewStyleClass(addStyle, removeStyle);
		});


/********************************
	locked region popup window
*********************************/

		$('body').on('click', '.slick-header-column:eq(0)', function(){
			$('#lockedRegionPopup').fadeIn(500);
			$('.overlay').fadeIn(500);
		});

		$('#lockedRegionButton').on('click', function(){
			$('#lockedRegionPopup').fadeOut(500);
			$('.overlay').fadeOut(500);

			return false;
		});

/********************************
	function popup window
*********************************/

		$('#showConditionalFormatting').on('click', function(){
			$('#conditionalFormattingPopup').fadeIn(500);
			$('.overlay').fadeIn(500);

			$('.scroller_conditional').baron({
	            bar: '.scroller_conditional__track',
	            barOnCls: 'possibleScroll',
	            direction: 'v'
	        });
			
			return false;
		});

		$('#conditionalFormattingButton').on('click', function(){
			var columns = grid.getColumns();
			var index = grid.getActiveCell().cell;
			var columnName = columns[index].name
			var tempArray = {
				'name' : columnName,
				'value' : []
			};

			if(conditionalFormattingArray.length){
				for(var i=0;i<conditionalFormattingArray.length;i++){
					if(conditionalFormattingArray[i].name == columnName){
						conditionalFormattingArray.splice(i, 1);
					}
				}
			}

			$('.conditional_formatting_row').each(function(){
				var checkbox = $('input[type="checkbox"]', this); 
				var color = $('.sp-preview-inner', this);
				var formatterArray = {
					'input' : $('.textbox', this).val(),
					'bgcolor' : '',
					'color' : ''
				};
				if(checkbox[0].checked){
					formatterArray.bgcolor = 'bgcolor' + color.eq(0).attr('data-id');
				}
				if(checkbox[1].checked){
					formatterArray.color = 'color' + color.eq(1).attr('data-id');	
				}
				tempArray.value.push(formatterArray);
			});
			conditionalFormattingArray.push(tempArray);
			console.log(conditionalFormattingArray);

			columns[index].formatter = ifTextHave;

			grid.setColumns(columns);

			$('#conditionalFormattingPopup').fadeOut(500);
			$('.overlay').fadeOut(500);

			return false;
		});

		function ifTextHave(row, cell, value, columnDef, dataContext){
			for(var i=0;i<conditionalFormattingArray.length;i++){

				if(conditionalFormattingArray[i].name == columnDef.name){
					var formatting = conditionalFormattingArray[i].value;

					for(var j=0;j<formatting.length;j++){
						var reg = formatting[i].input;
						reg = new RegExp(reg);

						if(reg.test(value)){
							if(formatting[i].bgcolor.length){
								addNewStyleClass(formatting[i].bgcolor, colorArray);
							}
							if(formatting[i].color.length){
								addNewStyleClass(formatting[i].color, fontColorArray);
							}
						}
					}
				}
			}
			
			return value;
		}

		$('#addNewConditFormattingRow').on('click', function(){
			var rows = $('.conditional_formatting_row');
			var myTxt = '';

			myTxt+=	'<div class="conditional_formatting_row">'
		            +    '<div class="input_formatting_text">'
		            +       '<span class="block_title">Если</span>'
		            +        '<select class="select">'
		            +            '<option>текст содержит</option>'
		            +        '</select>'
		            +        '<input class="textbox" type="text">'
		            +    '</div>'
		            +    '<div class="apply_color">'
		            +        '<span class="block_title">Установить цвета</span>'
		            +        '<div class="colorsWrapper">'
		            +            '<div>'
		            +                '<div class="color">'
		            +                    '<input class="fontColorPanel" type="text">'
		            +                '</div>'
		            +                '<div class="color_input_wrapper">'
		            +                    '<input id="fontColor'+ rows.length +'" type="checkbox">'
		            +                '</div>'
		            +                '<label for="fontColor'+ rows.length +'">Цвет фона</label>'
		            +            '</div>'
		            +            '<div>'
		            +                '<div class="color">'
		            +                    '<input class="textColorPanel" type="text">'
		            +                '</div>'
		            +                '<div class="color_input_wrapper">'
		            +                    '<input id="textColor'+ rows.length +'" type="checkbox">'
		            +                '</div>'
		            +                '<label for="textColor'+ rows.length +'">Цвет текста</label>'
		            +            '</div>'
		            +        '</div>' 
		            +        '<a href=""></a>'
		            +    '</div>'
		            +'</div>'; 
            rows.eq(rows.length-1).after(myTxt);

            $(".conditional_formatting_row .fontColorPanel, .conditional_formatting_row .textColorPanel").spectrum({
				showPaletteOnly: true,
		    	showPalette:true,
				showInput: true,
				chooseText: "Выбрать",
				cancelText: "Отмена",
				palette: [
					["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)","rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)",
					"rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
					"rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
					"rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
					"rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
					"rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
					"rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
					"rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
					"rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
					"rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
					"rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)",
					"rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)",]
		      	]
			});

			$(".conditional_formatting_row .select").chosen();

			$('.scroller_conditional').baron({
	            bar: '.scroller_conditional__track',
	            barOnCls: 'possibleScroll',
	            direction: 'v'
	        });

			return false

		});

/********************************
	function popup window
*********************************/

		$('#setFunction').on('click', function(){
			$('#functionPopup').fadeIn(500);
			$('.overlay').fadeIn(500);

			$('.scroller_function').baron({
	            bar: '.scroller_function__track',
	            barOnCls: 'possibleScroll',
	            direction: 'v'
	        });
			
			return false;
		});

		$('#functionButton').on('click', function(){
			$('#functionPopup').fadeOut(500);
			$('.overlay').fadeOut(500);

			return false;
		});

/********************************
	number formatting popup window
*********************************/

		$('#numberFormatting').on('click', function(){
			$('#numberFormattingPopup').fadeIn(500);
			$('.overlay').fadeIn(500);

			$('.scroller_table').baron({
	            bar: '.scroller_table__track',
	            barOnCls: 'possibleScroll',
	            direction: 'v'
	        });

			return false;
		});

		$('#total_formatting tr').on('click', function(){
			columnFormatter = $(this).attr('data-formatter');
			$('#total_formatting tr').removeClass('active');
			$(this).addClass('active');
		});

		$('#numberFormattingButton').on('click', function(){
			var columns = grid.getColumns();
			var index = grid.getActiveCell().cell;

			switch (columnFormatter){
				case 'one':
					columns[index].formatter = commaOneZero;
				break;
				case 'two':
					columns[index].formatter = commaTwoZero;
				break;
				case 'three':
					columns[index].formatter = commaThreeZero;
				break;
				case 'four':
					columns[index].formatter = commaFourZero;
				break;
			}

			grid.setColumns(columns);

			$('#numberFormattingPopup').fadeOut(500);
			$('.overlay').fadeOut(500);

			return false;
		});

		$('.number_formatting__menu a').on('click', function(){
			$('.number_formatting__content_wrapper > div').hide();
			$('#' + $(this).attr('data-id')).show();
			$('.number_formatting__menu a').removeClass('active');
			$(this).addClass('active');

			return false;
		})

/********************************
	users access popup window
*********************************/

		$('#setAccess').on('click', function(){
			$('#accessPopup').fadeIn(500);
			$('.overlay').fadeIn(500);

			return false;
		});

		$('#accessButton').on('click', function(){
			$('#accessPopup').fadeOut(500);
			$('.overlay').fadeOut(500);

			return false;
		});

		$('#addNewUser').on('click', function(){
			$('#addUserPopup').fadeIn(500);
			$('.overlay_access').fadeIn(500);

			return false;
		});

		$('#addUserButton').on('click', function(){
			$('#addUserPopup').fadeOut(500);
			$('.overlay_access').fadeOut(500);

			return false;
		});

/********************************
	history popup window
*********************************/

		$('#showHistory').on('click', function(){
			$('#historyPopup').fadeIn(500);
			$('.overlay').fadeIn(500);

			return false;
		});

		$('#histiryButton').on('click', function(){
			$('#historyPopup').fadeOut(500);
			$('.overlay').fadeOut(500);

			return false;
		});


/********************************
	close popup window
*********************************/
	$('.authForm__btn_cancel, .popup__close, .overlay').on('click', function(){
		$('.popup').fadeOut(500);
		$('.overlay').fadeOut(500);

		return false;
	});

	$('#addUserPopup .authForm__btn_cancel,#addUserPopup .popup__close').unbind('click').on('click', function(){
		$('#addUserPopup').fadeOut(500);
		$('.overlay_access').fadeOut(500);

		return false;
	});


/***********************************************************************
	add\remove note for table cell
************************************************************************/
	$('#addRemoveNote').on('click', function(){
		var selected = grid.getActiveCell();
		$(this).toggleClass('active');

		if(grid.getActiveCell() && $(this).hasClass('active')){
			$('#notePopup').fadeIn(500);
			$('.overlay').fadeIn(500);

		}else if(grid.getActiveCell()){
			addNewStyleClass('', 'cell_note');

			for(var i=0;i<noteArray.length;i++){
				if(noteArray[i].row == selected.row && noteArray[i].cell == selected.cell){
					noteArray.splice(i, 1);
				}
			}
		    $('#noteTooltip').hide();
		}

		return false;
	});


	$('#notePopupButton').on('click', function(){
		var selected = grid.getActiveCell();
		var val = $('#notePopupText').val();

		selected.text = val

		addNewStyleClass('cell_note', '');
		noteArray.push(selected);

		$('#notePopup').fadeOut(500);
		$('.overlay').fadeOut(500);

		return false
	});

/********************************
	events for table cell\row
*********************************/

	$('#tableBody').on('click', '.slick-cell', function(){

		var columnNumber = this.className.split(/\s/)[2].replace(/r/, '');

		$('.slick-header-column.active').removeClass('active');
		$('.slick-header-column').eq(columnNumber).addClass('active');

		if(tempSelectedCell.length){
			tempSelectedCell.removeClass('borderTop borderRight borderBottom borderLeft');
		}		
	});

/********************************
	events with table column
*********************************/

	$('#hideShowColumn').on('click', function(){
		var myTxt = '';

		$('.hide_show__row').remove();

		for (var i = 1; i < arrayHiddenColumn.length; i++) {
			myTxt += '<div class="hide_show__row">'
			        +     '<div class="hide_show__row_name">'
			        +         '<strong>' + arrayHiddenColumn[i].name + '</strong>'
			        +         '<span></span>'
			        +     '</div>'
			        +  	  '<div data-columnid="' + i + '" class="scroll_button__wrapper ';
	        if(arrayHideShowColumn[i] == undefined || (arrayHideShowColumn[i] && !arrayHideShowColumn[i].showHide)){
    	 		myTxt +='active';
	        }

			myTxt +=         '">'
					+ 		  '<div class="scroll_button__content">'
			        +             '<div class="scroll_button__hide">Скрыт</div>'
			        +             '<div class="scroll_button__show">Показан</div>'
			        +         '</div>'                
			        +         '<span class="scroll_button"></span>'
			        +     '</div>'
			        + '</div>';
		}

		myTxt += '<div class="scroller_hideShow__track"><div class="scroller__bar"></div></div>';

		$('#hide_show__row_container').append(myTxt);

		$('#hideShowColumnPopup').fadeIn(500);
		$('.overlay').fadeIn(500);

		$('.hideShow_scroller').baron({
            bar: '.scroller_hideShow__track',
            barOnCls: 'possibleScroll',
            direction: 'v'
        });

		return false;
	})

	$('#hideShowColumnPopup').on('click', '.scroll_button__wrapper', function(){
		arrayHideShowColumn[$(this).attr('data-columnid')] = $(this).hasClass('active') ? {'showHide':true} : {'showHide':false};
		$(this).toggleClass('active');	
	});

	$('#hideShowColumnButton').on('click', function(){		
		var columns = arrayToArray(arrayHiddenColumn);
		
		for (var i=columns.length; i--;) {
			
			if(arrayHideShowColumn[i] && arrayHideShowColumn[i].showHide){
				
				columns.splice(i, 1);

			}
		};
		grid.setColumns(columns);

		$('#hideShowColumnPopup').fadeOut(500);
		$('.overlay').fadeOut(500);

		return false;
	});


	$("#addNewColumn").click(function() {
		var columns = grid.getColumns();
		var index = grid.getActiveCell().cell;
		var newColumn = {
			'defaultSortAsc' : true,	
			'field' : 'Column_' + columns.length,
			'focusable' : true,
			'headerCssClass' : null,
			'id' : columns.length,
			'minWidth' : 30,
			'name' : 'Column_' + columns.length,
			'rerenderOnResize' : false,
			'resizable' : true,
			'selectable' : true,
			'sortable' : false,
			'width' : 100,
			'editor' : Slick.Editors.Text
		};

		if($('.slick-cell.selected').length){				

			columns.splice(index+1, 0, newColumn);

			for(var i=0;i<arrayHiddenColumn.length;i++){
				if(arrayHiddenColumn[i].name == columns[index].name){
					arrayHiddenColumn.splice(i+1, 0, newColumn);
				}
			}

		}else{

			columns.push(newColumn);
			arrayHiddenColumn.push(newColumn);
		}	
		grid.setColumns(columns);
		
		return false;
	});

	$('#deleteColumn').on('click', function(){
		var columns = grid.getColumns();
		var index = $('.slick-header-column.active').index();

		columns.splice(index, 1);

		for(var i=0;i<arrayHiddenColumn.length;i++){
			if(arrayHiddenColumn[i].name == columns[index].name){
				arrayHiddenColumn.splice(i, 1);
			}
		}
		

		grid.setColumns(columns);

	});


	/********************************
		events with table row
	*********************************/
	

	$("#addTableRow").click(function() {
		var pageInfo = dataView.getPagingInfo();
		if($('.slick-row.active').length){
			var index = $('.slick-row.active').index();

			dataView.beginUpdate();
			dataView.insertItem(index, {'id': index, 'num' : index+1});
			dataView.endUpdate();

			for(var i=index+1;i<(pageInfo.totalRows+1);i++){
				var item = dataView.getItemByIdx(i);
				item.num = i+1;
				dataView.updateItem(item.id, item);
			}
		}else{
			dataView.beginUpdate();
			dataView.addItem({id: pageInfo.totalRows});
			dataView.endUpdate();
		}		

		return false;
	})

	$("#removeTableRow").click(function() {
		if($('.slick-row.active').length){
			var index = $('.slick-row.active').attr('id').replace(/row/,'');			
			var item = dataView.getItem(index);

			dataView.deleteItem(item.id);
			grid.invalidate();
			grid.render(); 
		}
		
		return false;
	})


  	$("#fontColorPanel, #textColorPanel").spectrum({
		showPaletteOnly: true,
    	showPalette:true,
		showInput: true,
		chooseText: "Выбрать",
		cancelText: "Отмена",
		palette: [
			["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)","rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)",
			"rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
			"rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
			"rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
			"rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
			"rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
			"rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
			"rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
			"rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
			"rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
			"rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)",
			"rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)",]
      	]
	});




	var dataView;
	var data = [];
	var options = {
	    editable: true,
	    enableCellNavigation: true,
	    showHeaderRow: true,
	    headerRowHeight: 30,
	    explicitInitialization: true,
	    enableColumnReorder: true,
		autoEdit: false,
		enableAsyncPostRender:	true
	};
  

	function filter(item) {
	    for (var columnId in columnFilters) {
	      if (columnId !== undefined && columnFilters[columnId] !== "") {
	        var c = grid.getColumns()[grid.getColumnIndex(columnId)];
	        if (item[c.field] != columnFilters[columnId]) {
	          return false;
	        }
	      }
	    }
	    return true;
	}

    var groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider();
	dataView = new Slick.Data.DataView({
		groupItemMetadataProvider: groupItemMetadataProvider,
		inlineFilters: true
	});
    // prepare the data
    for (var i = 0; i < 100; i++) {
		var d = (data[i] = {});

		d["id"] = i;
		d["num"] = i+1;
    }


    /******************* INIT TABLE PLUGIN *********************/
    dataView = new Slick.Data.DataView();
    grid = new Slick.Grid("#tableBody", dataView, columns, options);

    // register the group item metadata provider to add expand/collapse group handlers
    grid.registerPlugin(groupItemMetadataProvider);
    grid.setSelectionModel(new Slick.CellSelectionModel());

	grid.onContextMenu.subscribe(function (e) {
		e.preventDefault();
		var cell = grid.getCellFromEvent(e);
		$("#contextMenu")
			.data("row", cell.row)
			.css("top", e.pageY)
			.css("left", e.pageX)
			.show();

		$("body").one("click", function () {
			$("#contextMenu").hide();
		});
	});

	dataView.onRowCountChanged.subscribe(function (e, args) {
		grid.updateRowCount();
		grid.render();
	});

	dataView.onRowsChanged.subscribe(function (e, args) {
		grid.invalidateRows(args.rows);
		grid.render();
	});

	grid.onClick.subscribe(function(e, args) {
		var columnName = grid.getColumns()[args.cell].id;
		var seletcNode = grid.getCellNode(args.row, args.cell);
	    $('#noteTooltip').hide();

	    if(/\sbgcolor\d/.test($(seletcNode).attr('class'))){
	    	var className = $(seletcNode).attr('class').split(/\s/);
	    	var index = 0;

	    	for(var i=0;i<className.length;i++){
	    		if(/bgcolor/.test(className[i])){
	    			index = className[i].replace(/\D/g, '');
	    		}
	    	}
	    	$('.sp-thumb-el').removeClass('sp-thumb-active');
	    	var color = $('.sp-thumb-el').eq(index*1-1).addClass('sp-thumb-active').attr('data-color');

	    	$('#setBgColor .sp-preview-inner').attr('data-id', index).css('background-color', color);
	    	$('#setBgColor input[type="checkbox"]')[0].checked = true;
	    }else{
	    	$('.sp-thumb-el').removeClass('sp-thumb-active');
	    	var color = $('.sp-thumb-el').eq(60).addClass('sp-thumb-active').attr('data-color');

	    	$('#setBgColor .sp-preview-inner').attr('data-id', 60).css('background-color', color);
	    	$('#setBgColor input[type="checkbox"]')[0].checked = false;
	    }

	    if(/\scolor\d/.test($(seletcNode).attr('class'))){
	    	var className = $(seletcNode).attr('class').split(/\s/);
	    	var index = 0;

	    	for(var i=0;i<className.length;i++){
	    		if(/color/.test(className[i])){
	    			index = className[i].replace(/\D/g, '');
	    		}
	    	}
	    	$('.sp-thumb-el').removeClass('sp-thumb-active');
	    	var color = $('.sp-thumb-el').eq(index*1-1).addClass('sp-thumb-active').attr('data-color');

	    	$('#setFontColor .sp-preview-inner').attr('data-id', index).css('background-color', color);
	    	$('#setFontColor input[type="checkbox"]')[0].checked = true;
	    }else{
	    	$('.sp-thumb-el').removeClass('sp-thumb-active');
	    	var color = $('.sp-thumb-el').eq(60).addClass('sp-thumb-active').attr('data-color');

	    	$('#setFontColor .sp-preview-inner').attr('data-id', 60).css('background-color', color);
	    	$('#setFontColor input[type="checkbox"]')[0].checked = false;
	    }

	    if(/comment/.test(columnName)){
	    	var cord = grid.getCellNodeBox(args.row, args.cell);
    		var myTxt = '';
	    	if(commentsArray[args.cell] != undefined && 
	    		commentsArray[args.cell][args.row] != undefined){

	    		var row = commentsArray[args.cell][args.row];

	    		for(var i=0;i<row.length;i++){

	    			myTxt += '<div class="comment_row">'
	            		+ 	'<strong class="comment-author">' + row[i].author + '</strong>'
		            	+ 	'<span class="comment-date">' + row[i].date + '</span>'
		            	+ 	'<p class="comment-text">' + row[i].text + '</p>'		            		
						+   '<div class="comment-button">'
						+       '<a class="btn_circle_edit" href="javascript:void(0)"></a>'
						+       '<a class="btn_circle_delete" href="javascript:void(0)"></a>'
						+   '</div>'
		        		+'</div>';

	    		}

	    		$('#commentTooltip .comment_row__wrapper').html(myTxt);
		    	
	    	}else{
	    		$('#commentTooltip .comment_row__wrapper').html('');
	    	}
	    	

	    	$('#commentTooltip').css({'top' : 146 + cord.top, 
	    		'left' : $('#tableBody').offset().left + cord.left - 245, 
	    		'display':'block'
	    	});
	    
	    }else if($(grid.getCellNode(args.row, args.cell)).hasClass('cell_note')){
	    	var cord = grid.getCellNodeBox(args.row, args.cell);
	    	$('#addRemoveNote').addClass('active');

	    	for(var i=0;i<noteArray.length;i++){
				if(noteArray[i].row == args.row && noteArray[i].cell == args.cell){
					$('#noteTooltip').css({
						'top' : 146 + cord.top, 
						'left' : $('#tableBody').offset().left + cord.left + cord.width, 
						'display':'block'
					}).html(noteArray[i].text);
				}
			
			}
	    }else{
	    	$('#addRemoveNote').removeClass('active');
	    }

	});


	$(grid.getHeaderRow()).delegate(":input", "change keyup", function (e) {
		var columnId = $(this).data("columnId");
		if (columnId != null) {
			columnFilters[columnId] = $.trim($(this).val());
			dataView.refresh();
		}
	});

	grid.onHeaderRowCellRendered.subscribe(function(e, args) {
		$(args.node).empty();
		$("<input type='text'>")
			.data("columnId", args.column.id)
			.val(columnFilters[args.column.id])
			.appendTo(args.node);
	});

	grid.init();

	dataView.beginUpdate();
	dataView.setItems(data);
	dataView.setFilter(filter);
	dataView.endUpdate();

	arrayHiddenColumn = grid.getColumns();
	
	$('.slick-viewport').append('<div class="scroller_table_body__track"><div class="scroller__bar"></div></div><div class="scroller__bar scroller__bar_h"></div>');

	$('.slick-viewport').baron({
        bar: '.scroller_table_body__track',
        barOnCls: 'possibleScroll',
        direction: 'v'
    });	   
    $('.slick-viewport').baron({
        bar: '.scroller__bar_h',
        barOnCls: 'possibleScroll',
        direction: 'h'
    });

}).call(this);

var tempSelectedCell = [];

