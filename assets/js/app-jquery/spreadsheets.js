/*global jQuery, Handlebars */
jQuery(function ($) {
    'use strict';

    var Utils = {
        store: function (namespace, data) {
            if (arguments.length > 1) {
                return localStorage.setItem(namespace, JSON.stringify(data));
            } else {
                var store = localStorage.getItem(namespace);

                return (store && JSON.parse(store)) || [];
            }
        },

        cleanData: function(namespace) {
            return localStorage.removeItem(namespace);
        },

        createTestData: function(namespace) {
            var testSpreadsheets = [{
                id: 1,
                filename: "Максимальное количество пользователей",
                lastEdit: new Date('2013-09-01 12:10:11'),
                owner: "Константин Константинопольский",
                isDeleted: false,
            },{
                id: 2,
                filename: "Размер файлового хранилища",
                lastEdit: new Date('2013-09-02 18:13:12'),
                owner: "Константин Константинопольский",
                isDeleted: false
            },{
                id: 3,
                filename: "Цена подписки на пользователей",
                lastEdit: new Date('2013-08-01 22:10:11'),
                owner: "Артем",
                isDeleted: false
            },{
                id: 4,
                filename: "Цена подписки (Удалено)",
                lastEdit: new Date('2013-08-01 20:00:01'),
                owner: "Филлип Филлиппович",
                isDeleted: true
            },{
                id: 5,
                filename: "Магазин постеров",
                lastEdit: new Date('2013-09-03 12:10:11'),
                owner: "Артем",
                isDeleted: true
            },{
                id: 6,
                filename: "Гадкий Я",
                lastEdit: new Date('2013-09-02 18:13:12'),
                owner: "Грю Иванович",
                isDeleted: false
            },{
                id: 7,
                filename: "Тачки",
                lastEdit: new Date('2013-08-01 22:10:11'),
                owner: "Дисней",
                isDeleted: true
            },{
                id: 8,
                filename: "Апрельские тезисы",
                lastEdit: new Date('2013-04-21 09:00:01'),
                owner: "Владимир Ильич",
                isDeleted: false
            }];

            this.store(namespace, testSpreadsheets);

        }
    };

    var AppSpreadsheets = {
        init: function () {
            
            this.ENTER_KEY = 13;
            this.ESC_KEY = 27;

            this.spreadsheets = Utils.store('OasisApp-spreadsheets');

            if (this.spreadsheets.length == 0) {
                Utils.createTestData('OasisApp-spreadsheets');
                this.spreadsheets = Utils.store('OasisApp-spreadsheets');                
            }
            
            this.navType = 'all';

            this.cacheElements();
            this.bindEvents();
            this.render();
        },

        cacheElements: function () {

            this.templateList = Ember.TEMPLATES['app/spreadsheets/spreadsheets-list'];
            
            this.$spreadsheetsList = $('#js-spreadsheetsList');
            this.$typeSpreadsheetsNav = $('#js-spreadsheetsTypes');
            
            this.$actionsSpreadsheetsAllNav = $('#js-spreadsheetsAllActions');
            this.$actionsSpreadsheetsDeletedNav = $('#js-spreadsheetsDeletedActions');
            
            this.$checkAllSpreadsheets = $('#js-checkAllSpreadsheets');
        },

        bindEvents: function () {
            this.$spreadsheetsList.on('click', 'tr', this.selectRow);
            this.$spreadsheetsList.on('click', 'input[type="checkbox"]', this.selectRowByCheckbox);

            this.$typeSpreadsheetsNav.on('click', 'a', this.showList);
            this.$checkAllSpreadsheets.on('change', this.toggleAll);

            $('#js-deleteSpreadsheetConfirm').on('click', this.delete);

            $('#js-spreadsheetRename').on('click', this.rename);
            $('#js-spreadsheetRenameConfirm').on('click', this.renameUpdate);
            
            $('#js-spreadsheetShare').on('click', this.share);
            $('#js-spreadsheetHistory').on('click', this.showHistory);

            $('#js-spreadsheetCopy').on('click', this.copy);
            $('#js-spreadsheetCopyConfirm').on('click', this.copyMany);
            
            $('#js-spreadsheetDeleteConfirm').on('click', this.delete);
            $('#js-spreadsheetDeleteForeverConfirm').on('click', this.deleteForever);

            $('#js-speadsheetTruncateTrashConfirm').on('click', this.truncateTrash);

            $('#js-speadsheetRestore').on('click', this.restore);

            // TODO По идее вынести в общ хелпер, подумать куда
            $('html').on('click', '#js-historyVersionsTable tr', function(){
                $('#js-historyVersionsTable tr').removeClass('selected');
                $(this).addClass('selected');
            });

            $('#createTableCopyWithVersion').on('click', function(){
                AppSpreadsheets.copyMany();
                window.Modals.instance.forceHide('popup_historyVersionsSpreadsheet');
            });

            $('.js-shareDeleteUser').on('click', this.deleteSharedUser);
        },

        render: function () {

            if (typeof this.filteredSpreadsheets === 'undefined') {
                this.filteredSpreadsheets = this.getList('all');
            }



            for (var key = 0; key< this.filteredSpreadsheets.length; key++) {
                this.filteredSpreadsheets[key].lastEdit_text = moment(this.filteredSpreadsheets[key].lastEdit).format("DD.MM.YYYY в HH:mm");
            }

            var templateData = {
                spreadsheetsList: this.filteredSpreadsheets 
            };
            //debugger;
            //this.$spreadsheetsList.html(this.templateList(this, {data:{buffer:[]}}));
            this.changeMenu();
        },

        changeMenu: function() {
            
            var selectedLength = $(AppSpreadsheets.$spreadsheetsList).find('.selected').length,
                nav = (AppSpreadsheets.navType == 'all') ? AppSpreadsheets.$actionsSpreadsheetsAllNav : AppSpreadsheets.$actionsSpreadsheetsDeletedNav;

            if (selectedLength == 1) {
                $(nav).find('.disabled').removeClass('disabled');
            } else if (selectedLength > 1) {
                $(nav).find('.disabled').removeClass('disabled');

                if (AppSpreadsheets.navType == 'all') {
                    $('#js-spreadsheetShare').addClass('disabled');
                    $('#js-spreadsheetRename').addClass('disabled');
                    $('#js-spreadsheetHistory').addClass('disabled');    
                }
            } else {
                $(nav).find('a').each(function(){
                    if (!(AppSpreadsheets.navType == 'deleted' && $(this).attr('id') == 'js-speadsheetTruncateTrash')) {
                        $(this).addClass('disabled');
                    }
                });
            }
        },

        rename: function () {

            if ($(this).hasClass('disabled')) {
                return false;
            }

            var id = $(AppSpreadsheets.$spreadsheetsList).find('.selected').data('id'),
                sheet = AppSpreadsheets.getSpreadsheet(id);

            $('#currentSpreadsheetName').val(sheet.filename);

            window.Modals.instance.forceShow('popup_renameSpreadsheet');
        },

        renameUpdate: function(e) {
            e.preventDefault();

            var id = $(AppSpreadsheets.$spreadsheetsList).find('.selected').data('id'),
                postData = {
                    filename: $('#currentSpreadsheetName').val()
                };

            for (var i = 0 in AppSpreadsheets.spreadsheets) {
                if (AppSpreadsheets.spreadsheets[i].id == id) {
                    
                    // TODO Отрефакторить и убрать
                    // Потом нужна будет только postData
                    AppSpreadsheets.spreadsheets[i].filename = postData.filename;
                }
            }

            // TODO Тут (вместо Utils.store) потом должен быть ajax запрос
            // Посылать id через запятую или объект с id
            Utils.store('OasisApp-spreadsheets', AppSpreadsheets.spreadsheets);

            $('#renameSpreadsheetPopup .js-modalClose').trigger('click');
            AppSpreadsheets.render();
        },

        share: function() {
            window.Modals.instance.forceShow('popup_shareSpreadsheet');
        },

        copy: function() {

            var selected = $(AppSpreadsheets.$spreadsheetsList).find('.selected');

            if (selected.length >= 5) {

                window.Modals.instance.forceShow('popup_copySpreadsheet', function(){
                    $('#js-countCopyFiles').html(selected.length);
                });
            } else {
                AppSpreadsheets.copyMany();
            }
        },

        copyMany: function() {
            var selected = $(AppSpreadsheets.$spreadsheetsList).find('.selected'),
                copyIds = [];
            
            $.when(
                $(selected).each(function(){
                    var id = $(this).data('id'),
                        sheet = AppSpreadsheets.getSpreadsheet(id);

                    if (sheet) {
                        var max = 0,
                            copyCount = 0,
                            copyName, originalName, splitName;
                        
                        for (var i = 0 in AppSpreadsheets.spreadsheets) {
                            if (AppSpreadsheets.spreadsheets[i].filename.indexOf(sheet.filename) !== -1) {
                                splitName = AppSpreadsheets.spreadsheets[i].filename.split('- Копия ');
                                if (splitName.length >= 2) {
                                    copyCount = Math.max(parseInt(splitName[1].replace(/[()]/g,''), 10), copyCount);
                                }                                
                            }

                            max = Math.max(max, AppSpreadsheets.spreadsheets[i].id);
                        }

                        if (copyCount == 0) {
                            copyName = sheet.filename + ' - Копия (1)';
                        } else {
                            splitName = sheet.filename.split('- Копия ');
                            copyName = splitName[0] + ' - Копия (' + (copyCount + 1) + ')';
                        }
                        
                        var sheetCopy = {
                            id: max + 1,
                            filename: copyName,
                            lastEdit: new Date(),
                            owner: sheet.owner,
                            isDeleted: false
                        }

                        copyIds.push(sheetCopy.id);
                        AppSpreadsheets.spreadsheets.push(sheetCopy);

                        // TODO Тут (вместо Utils.store) потом должен быть ajax запрос
                        // Посылать id через запятую или объект с id
                        Utils.store('OasisApp-spreadsheets', AppSpreadsheets.spreadsheets);
                    } else {
                        console.warn('Не найдена таблица, которую вы хотите скопировать');
                    }
                })
            ).then(function(){
                var type = $('#js-spreadsheetsTypes .active').data('type');
                AppSpreadsheets.showList(type);

                $(AppSpreadsheets.$spreadsheetsList).find('tr').each(function(){
                    if (copyIds.indexOf($(this).data('id')) !== -1) {
                        $(this).addClass('selected');
                        $(this).find('input[type="checkbox"]').prop('checked', true);
                    }
                });
            });
        },

        showHistory: function() {
            if ($(this).hasClass('disabled')) {
                return ;
            }
            window.Modals.instance.forceShow('popup_historyVersionsSpreadsheet');
        },

        delete: function () {
            var ids = [];

            $.when(
                $(AppSpreadsheets.$spreadsheetsList).find('.selected').each(function(){
                    ids.push($(this).data('id'));
                })
            ).then(function(){

                for (var i = 0 in AppSpreadsheets.spreadsheets) {
                    if (ids.indexOf(AppSpreadsheets.spreadsheets[i].id) !== -1) {
                        AppSpreadsheets.spreadsheets[i].isDeleted = true;
                    }
                }
                
                // TODO Тут (вместо Utils.store) потом должен быть ajax запрос
                // Посылать id через запятую или объект с id

                Utils.store('OasisApp-spreadsheets', AppSpreadsheets.spreadsheets);
                
                $('#deletePopup .popup__close').trigger('click');

                var type = $('#js-spreadsheetsTypes .active').data('type');
                AppSpreadsheets.showList(type);
            });
        },

        deleteForever: function() {
            var ids = [];

            $.when(
                $(AppSpreadsheets.$spreadsheetsList).find('.selected').each(function(){
                    ids.push($(this).data('id'));
                })
            ).then(function(){

                for (var k in ids) {
                    for (var i in AppSpreadsheets.spreadsheets) {
                        if (ids.indexOf(AppSpreadsheets.spreadsheets[i].id) !== -1) {
                            (AppSpreadsheets.spreadsheets).splice(i, 1);
                        }
                    }
                }
                
                // TODO Тут (вместо Utils.store) потом должен быть ajax запрос
                // Посылать id через запятую или объект с id
                Utils.store('OasisApp-spreadsheets', AppSpreadsheets.spreadsheets);
                
                $('#deletePopup .js-modalClose').trigger('click');

                var type = $('#js-spreadsheetsTypes .active').data('type');
                AppSpreadsheets.showList(type);
            });
        },

        truncateTrash: function() {
            $('#js-spreadsheetsList tr').addClass('selected');
            AppSpreadsheets.deleteForever();
        },

        restore: function () {

            if ($(this).hasClass('disabled')) {
                return false;
            }

            var id = $(AppSpreadsheets.$spreadsheetsList).find('.selected').data('id'),
                sheet = AppSpreadsheets.getSpreadsheet(id);


            for (var i = 0 in AppSpreadsheets.spreadsheets) {
                if (AppSpreadsheets.spreadsheets[i].id == id) {
                    
                    // TODO Отрефакторить и убрать
                    // Потом нужна будет только postData
                    AppSpreadsheets.spreadsheets[i].isDeleted = false;
                }
            }

            // TODO Тут (вместо Utils.store) потом должен быть ajax запрос
            // Посылать id через запятую или объект с id
            Utils.store('OasisApp-spreadsheets', AppSpreadsheets.spreadsheets);

            var type = $('#js-spreadsheetsTypes .active').data('type');
            AppSpreadsheets.showList(type);
        },

        getList: function(type) {

            var possibleValues = ["all", "deleted"],
                list = this.spreadsheets,
                result = [];

            if (typeof type === 'undefined' || possibleValues.indexOf(type) === -1) {
                var type = 'all';
            }

            for (var key =0; key<list.length; key++) {
                var kObj =  list[key];
                if (type == 'deleted' && kObj.isDeleted === true) {
                    result.push(kObj);
                } else if (type == 'all' && kObj.isDeleted !== true) {
                    result.push(kObj);
                }
            }

            return result;
        },

        showList: function() {
            var type = $(this).data('type');
            

            $(AppSpreadsheets.$typeSpreadsheetsNav).find('.active').removeClass('active');
            $(this).addClass('active');
            AppSpreadsheets.navType = type;

            if (type == 'deleted') {
                $(AppSpreadsheets.$actionsSpreadsheetsDeletedNav).removeClass('hidden');
                $(AppSpreadsheets.$actionsSpreadsheetsAllNav).addClass('hidden');
            } else {
                $(AppSpreadsheets.$actionsSpreadsheetsDeletedNav).addClass('hidden');
                $(AppSpreadsheets.$actionsSpreadsheetsAllNav).removeClass('hidden');
            }
            AppSpreadsheets.filteredSpreadsheets = AppSpreadsheets.getList(type);
            AppSpreadsheets.render();
        },

        getSpreadsheet: function(id) {
            var spreadsheet = {};
            
            for (var i in this.spreadsheets) {
                if (this.spreadsheets[i].id == id) {
                    spreadsheet = this.spreadsheets[i];
                    break;
                }
            }

            return spreadsheet;
        },

        deleteSharedUser: function() {
            var $tr = $(this).parent().parent().remove();
        },

        toggleAll: function() {
            var isChecked = $(this).prop('checked');

            if (isChecked) {
                $(AppSpreadsheets.$spreadsheetsList).find('input[type="checkbox"]').prop('checked', true);
                $(AppSpreadsheets.$spreadsheetsList).find('tr').addClass('selected');
            } else {
                $(AppSpreadsheets.$spreadsheetsList).find('input[type="checkbox"]').prop('checked', false);
                $(AppSpreadsheets.$spreadsheetsList).find('.selected').removeClass('selected');
            }

            AppSpreadsheets.changeMenu();
        },

        selectRow: function (e) {

            var $rows = $(AppSpreadsheets.$spreadsheetsList).find('tr'),
                $active = $(AppSpreadsheets.$spreadsheetsList).find('.active');

            if (!(e.metaKey || e.ctrlKey || e.shiftKey) && !$(this).hasClass('selected')) {
                $(AppSpreadsheets.$spreadsheetsList).find('input[type="checkbox"]').prop('checked', false);
                $(AppSpreadsheets.$spreadsheetsList).find('.selected').removeClass('selected');
            } else if (e.shiftKey) {

                var indexNext = $($rows).index($(this)),
                    indexPrev = $($rows).index($($active));

                var slice = $($rows).slice(Math.min(indexPrev, indexNext), Math.max(indexPrev, indexNext));

                $($active).removeClass('active');
                $(this).addClass('selected').addClass('active');
                $(this).find('input[type="checkbox"]').prop('checked', true);

                $(slice).each(function(){
                    $(this).addClass('selected');
                    $(this).find('input[type="checkbox"]').prop('checked', true);
                });
            }
            
            if (!e.shiftKey) {
                $($active).removeClass('active');
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');    
                    $(this).find('input[type="checkbox"]').prop('checked', false);
                } else {
                    $(this).addClass('selected').addClass('active');
                    $(this).find('input[type="checkbox"]').prop('checked', true);
                }    
            }
            
            AppSpreadsheets.changeMenu();
        },

        selectRowByCheckbox: function(e){
            e.stopPropagation();

            var isChecked = $(this).prop('checked'),
                $active = $(AppSpreadsheets.$spreadsheetsList).find('.active'),
                $tr = $(this).parent().parent();
                
            $($active).removeClass('active');

            if (isChecked) {
                $($tr).addClass('selected').addClass('active');
            } else {
                $($tr).removeClass('selected');
            }

            AppSpreadsheets.changeMenu();
        }
    };

    AppSpreadsheets.init();

});
