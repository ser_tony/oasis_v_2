# -*- coding: UTF-8 -*-
import httplib
import urllib
import sys
from tornado import template
import tornado.ioloop
import tornado.httpserver
from tornado.options import define, options
import tornado.web
import os
import json
from application.controller.google_drive_store import SpreadsheetStorage, GoogleOAuth, WorksheetHandler, ShareHandler

#TODO: Привести к структуре проекта с соответсвующими подключениями скриптов при сборке
define("port", default=8081, help="run on the given port", type=int)
define("package", default=False, help="generate scripts", type=bool)
define("compress", default=False, help="compress scripts", type=bool)
define("test", default=False, help="test scripts", type=bool)

 #
 # Описание задач по ссылке
 # https://onedrive.live.com/view.aspx?resid=4BA4BED985855230!2085&ithint=onenote%2c&app=OneNote&wdo=2&authkey=!AG07sH0P1Zso_VM
 #
AppName = 'App'
BASE_DIR = os.path.dirname(__file__)

def joinScripts(scripts):
    out = ''
    utils = ''
    static_path = os.path.join(os.path.dirname(__file__), "js", "app")

    # load utils
    if scripts.get('utils'):
        with open(os.path.join(static_path, scripts.get('utils') + '.js'), 'rt') as f:
            utils += f.read()

    # load Models
    for model in scripts.get('models', []):
        with open(os.path.join(static_path, 'model', model + '.js'), 'rt') as f:
            out += f.read()

    # load Router
    with open(os.path.join(static_path, scripts.get('router') + '.js'), 'rt') as f:
        out += f.read()

    # load Routes
    for route in scripts.get('routes', []):
        with open(os.path.join(static_path, 'route', route + '.js'), 'rt') as f:
            out += f.read()

    # load Controllers
    for controller in scripts.get('controllers', []):
        with open(os.path.join(static_path, 'controller', controller + '.js'), 'rt') as f:
            out += f.read()

    # load Views
    for view in scripts.get('views', []):
        with open(os.path.join(static_path, 'view', view + '.js'), 'rt') as f:
            out += f.read()

    # load Components
    for component in scripts.get('components', []):
        with open(os.path.join(static_path, 'component', component + '.js'), 'rt') as f:
            out += f.read()

    # load Templates
    for template in scripts.get('templates', []):
        if "/" in template:
            dirs = template.split('/')
            paths = [static_path, 'tpl'] + dirs[:-1]
            path = os.path.join(*paths)
            path = os.path.join(path, dirs[-1] + '.hbs')
            with open(path, 'rt') as f:
                out += ('Ember.TEMPLATES["' + template + '"] = Ember.Handlebars.compile("%s");' %
                        f.read().replace("\r", "").replace("\n", "").replace('"', '\\"')) + "\n"
                # if component register
                if dirs[0] != 'components':
                    continue
                name = "".join([n.capitalize() for n in dirs[-1].split('-')])
                out += "App.registerComponent('"+dirs[-1]+"');"
        else:
            with open(os.path.join(static_path, 'tpl', template + '.hbs'), 'rt') as f:
                out += ('Ember.TEMPLATES["' + template + '"] = Ember.Handlebars.compile("%s");' %
                        f.read().replace("\r", "").replace("\n", "").replace('"', '\\"')) + "\n"

    # load test scripts
    if options.test and scripts.get('tests', []):
        for test in scripts.get('tests', []):
            with open(os.path.join(static_path, 'test', test + '.js'), 'rt') as f:
                out += f.read()

    if scripts.get('app'):
        with open(os.path.join(static_path, scripts.get('app') + '.js'), 'rt') as f:
            app = f.read()
            out = utils + app.replace('{{scripts}}', out)
    return out


def joinApplicationJS():
    scripts = {
        'utils': 'utils',
        'app': 'app',
        'router': 'router-application',
        'models': ['BaseModel', 'UserModel', 'SpreadsheetsModel', 'WorksheetModel'],
        'routes': ['ApplicationRoute', 'IndexRoute',
                   'AppSpreadsheetsRoute', 'AppParserRoute', 'AppTableRoute'
                   ],
        'controllers': ['ApplicationController',  'AppTableController',
                        'AppSpreadsheetsController'
                        ],
        'views': ['AppSpreadsheetsView',
                  'AppCommentPopupView',
                  'AppTableView', 'AppTableFunctionView',
                  'AppTableAccessView', 'AppTableAddUserView',
                  'AppHiddenColumnModalView',
                  'AppNumberFormattingModalView',
                  'AppValidationDataModalView',
                  'AppConditionalRuleView',
                  'AppConditionalFormattingModalView',
                  'AppChangeRowHeightModalView',
                  'AppTableSidebarView',
                  'AppConfirmModalView'],

        'templates': ['app/spreadsheets/index', 'app/spreadsheets/spreadsheets-list',
                      'app/table/index', 'app/table/sidebar',
                      'app/table/popups/function', 'app/table/popups/access',
                      'app/table/popups/comments',
                      'app/table/popups/add_user',
                      'app/table/popups/sidebar/number_formatting',
                      'app/table/popups/sidebar/hidden_columns',
                      'app/table/popups/sidebar/validation_data',
                      'app/table/popups/sidebar/conditional_formatting',
                      'app/table/popups/sidebar/conditional_rule',
                      'app/table/popups/sidebar/row_height',
                      'app/menu',
                      'app/confirm_modal'
                      ],
        'tests': options.test and ['testStartup'] or [],
        'components': []
    }
    return joinScripts(scripts)




def buildScripts():
    static_path = os.path.join(os.path.dirname(__file__), "js")
    app_path = os.path.join(static_path, 'application.js')
    app_data = joinApplicationJS()

    with open(app_path, 'wt') as f:
        f.write(app_data)
    if options.compress:
        for file_item in [[app_path, app_data]]:
            params = urllib.urlencode([
                ('js_code', file_item[1]),
                ('compilation_level', 'SIMPLE_OPTIMIZATIONS'),
                ('output_format', 'text'),
                ('output_info', 'compiled_code'),
            ])

            # Always use the following value for the Content-type header.
            headers = {"Content-type": "application/x-www-form-urlencoded"}
            conn = httplib.HTTPConnection('closure-compiler.appspot.com')
            conn.request('POST', '/compile', params, headers)
            response = conn.getresponse()
            data = response.read()
            with open(file_item[0], 'wt') as f:
                f.write(data)
            conn.close()


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        f = 'index.html'
        if options.test:
            f = 'index_test.html'
        self.write(open(os.path.join(BASE_DIR, f), 'rt').read())
        self.finish()

class ApplicationPanelHandler(tornado.web.RequestHandler):
    """Точка входа в Ember приложение
    """
    def get(self, user={}):
        if user.get('email', None) is None:
            user['email'] = self.get_secure_cookie("user")
            user['token'] = self.get_secure_cookie("token")
        html = open(os.path.join(BASE_DIR, 'js', 'index.html')).read()
        html = html.replace('{{user}}', json.dumps(user))
        self.write(html)
        self.finish()

class TestDraft(tornado.web.RequestHandler):
    def get(self):
        self.write(open(os.path.join(BASE_DIR, 'test_draft.html'), 'rt').read())
        self.finish()


class BaseJSHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/x-javascript')
        self.write(self.getScripts())


class StartupHandler(BaseJSHandler):
    def getScripts(self):
        pass
        # return joinStartupJS()


class PanelHandler(BaseJSHandler):
    def getScripts(self):
        pass


class TestJSHandler(tornado.web.RequestHandler):
    def get(self):
        self.write(open(os.path.join(BASE_DIR, 'test_js.html')).read())
        self.finish()


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),

            # Начальный скрипт с авторизацией
            (r"/compress/js/app/startup.js", StartupHandler),

            # Cкрипт после авторизации
            (r"/compress/js/app/application.js", PanelHandler),

            (r"/test_draft", TestDraft),

            # директория со статикой - Legacy без имзенений для работы сайта
            (r'/assets/(.*)', tornado.web.StaticFileHandler, {'path': os.path.join(os.path.dirname(__file__), "assets")}),

            (r'/js/(.*)', tornado.web.StaticFileHandler, {'path': os.path.join(os.path.dirname(__file__), "js")}),

            (r'/application', ApplicationPanelHandler),


            # Хендлеры работы с Google Storage
            (r'/spreadsheets/list', SpreadsheetStorage, dict(action='list')),
            (r'/spreadsheets/create', SpreadsheetStorage, dict(action='create')),
            (r'/spreadsheets/copy', SpreadsheetStorage, dict(action='copy')),
            (r'/spreadsheets/delete', SpreadsheetStorage, dict(action='delete')),
            (r'/spreadsheets/create_worksheet', WorksheetHandler),
            (r'/spreadsheets/share', ShareHandler),


            (r'/test_js', TestJSHandler),

            (r'/google_auth/?', GoogleOAuth),
            (r'/refresh_token', GoogleOAuth),


            #(r'/google_auth_callback', GoogleAuthCallback)


        ]
        settings = dict(
            debug=True,
            cookie_secret="9fjfsdofk40-3fk40fk3o4fksdfhf",
            xsrf_cookies=True,
            domain='localhost',
#            static_path=os.path.join(os.path.dirname(__file__), "js")
        )
        tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == "__main__":
    tornado.options.parse_command_line(sys.argv)
    buildScripts()
    if not options.package:
        http_server = tornado.httpserver.HTTPServer(Application())
        http_server.listen(options.port)
        tornado.ioloop.IOLoop.instance().start()
