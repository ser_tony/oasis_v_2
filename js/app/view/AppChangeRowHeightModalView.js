App.AppChangeRowHeightModalView = Em.View.extend({
    templateName: 'app/table/popups/sidebar/row_height',
    default_height: 25,
    current_height: 25,
    grid: null,

    open_modal: function(){
        var me = this,
            controller = me.get('parentView').get('parentView').get('controller');
        me.set('current_height', controller.get('tableWorksheetInstance').getSettingByKey('row_height') || me.get('default_height'));
        me.$('#changeRowHeightPopup').show();
        $('.overlay').show();
        $('body').on('keydown.closeModal', function(ev){
            if(ev.keyCode == $.ui.keyCode.ESCAPE){
                me._hideModal();
            }
        });
    },

    close_modal: function(){
        var me = this,
            controller = me.get('parentView').get('parentView').get('controller'),
            grid = me.get('grid'),
            value = me.get('current_height');
        if(!grid) return;
        if(typeof value == 'string' && value.match(/[0-9]+/)) value = parseInt(value, 10);
        if(typeof value != 'number' || _.isNaN(value)) return;
        grid.setOptions({rowHeight: value});
        grid.invalidateAllRows();
        grid.render(true);
        // для пересчета крестика
        grid.setActiveCell(0,2);
        grid.setActiveCell(0,1);

        if(value == me.get('default_height')){
            controller.get('tableWorksheetInstance').removeSettingByKey('row_height');
        } else {
            controller.get('tableWorksheetInstance').setSettingByKey('row_height', value);
        }
        me._hideModal();
    },

    _hideModal: function(){
        this.$('#changeRowHeightPopup').hide();
        $('.overlay').hide();
        $('body').off('keydown.closeModal');
    },

    actions:{
        'save': function(){
            this.close_modal();
        },
        'reset': function(){
            var me = this;
            me.set('current_height', me.get('default_height'));
            me.close_modal();
        }
    }
});