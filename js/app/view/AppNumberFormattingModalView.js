App.AppNumberFormattingModalView = Em.View.extend({
    templateName: 'app/table/popups/sidebar/number_formatting',
    selected_formatting: '',
    type_formatter: 'number',
    open_modal: function(){
        var me = this;
        me.$('#numberFormattingPopup').show();
        $('.overlay').show();
        $('body').on('keydown.closeModal', function(ev){
            if(ev.keyCode == $.ui.keyCode.ESCAPE){
                $('body').off('keydown.closeModal');
                me.$('.popup').hide();
                $('.overlay').hide();
            }
        });

        me.$('.scroller_table').baron({
            bar: '.scroller_table__track',
            barOnCls: 'possibleScroll',
            direction: 'v'
        });
    },

    actions:{
        'set_filter': function(filter){
            var me = this,
                el = me.$('.number_formatting__content_wrapper');
            me.$('.number_formatting__menu a.active, .table_popup tbody.active').removeClass('active');
            me.$('.number_formatting__menu a[data-id="' + filter + '"], .table_popup tbody[data-id="' + filter + '"]')
                .addClass('active');
            $('.scroller_table').trigger('sizeChange');
        },
        'select_formatting': function(formatting){
            var me = this,
                el = me.$('.table_popup'),
                tr = event.target.tagName == 'TR' && $(event.target) || $(event.target).parents('tr');
            el.find('tr.active').removeClass('active');
            tr.addClass('active');
            me.set('selected_formatting', formatting);
            me.set('type_formatter', 'number');
        },
        'select_formatting_date': function(formatting){
            var me = this,
                el = me.$('.table_popup'),
                tr = event.target.tagName == 'TR' && $(event.target) || $(event.target).parents('tr');
            el.find('tr.active').removeClass('active');
            tr.addClass('active');
            me.set('selected_formatting', formatting);
            me.set('type_formatter', 'date');
        },
        'apply_format': function(){
            var me = this,
                columns_with_index = [],
                grid = me.get('parentView').get('grid'),
                columns = grid.getColumns(),
                controller = me.get('parentView').get('parentView').get('controller'),
                ranges = grid.getSelectionModel().getSelectedRanges();
            for(var r=0; r<ranges.length; r++){
                var range = ranges[r];
                for (var cell_i = range.fromCell; cell_i <= range.toCell; cell_i++) {
                    var column = columns[cell_i];
                    column.format_type = me.get('type_formatter');
                    column.format = me.get('selected_formatting');
                    column.formatter = ColumnFormatterUniversal;
                    columns_with_index.push([column]);
                }
            }
            controller.get('tableWorksheetInstance').changeColumns(columns_with_index);
            grid.setColumns(columns);
            $('body').off('keydown.closeModal');
			$('#numberFormattingPopup').hide();
			$('.overlay').hide();
        }
    },

    formatting_values: {
        numbers: {
            num1: numeral(1000).format('0,0.0000'),
            num2: numeral(10000.23).format('0,0'),
            num3: numeral(10000.23).format('+0,0'),
            num4: numeral(-10000).format('0,0.0'),
            num5: numeral(10000.1234).format('0.000'),
            num6: numeral(10000.1234).format('0[.]00000'),
            num7: numeral(-10000).format('(0,0.0000)'),
            num8: numeral(-0.23).format('.00'),
            num9: numeral(-0.23).format('(.00)'),
            num10: numeral(0.23).format('0.00000'),
            num11: numeral(0.23).format('0.0[0000]'),
            num12: numeral(1230974).format('0.0a'),
            num13: numeral(1460).format('0 a'),
            num14: numeral(-104000).format('0a'),
            num15: numeral(1).format('0o'),
            num16: numeral(52).format('0o'),
            num17: numeral(23).format('0o'),
            num18: numeral(100).format('0o')
        },
        currencys: {
            curr1: numeral(1000.234).format('$0,0.00'),
            curr2: numeral(1000.2).format('0,0[.]00 $'),
            curr3: numeral(1001).format('$ 0,0[.]00'),
            curr4: numeral(-1000.234).format('($0,0)'),
            curr5: numeral(-1000.234).format('$0.00'),
            curr6: numeral(1230974).format('($ 0.00 a)')
        },
        percents: {
            perc1: numeral(1).format('0%'),
            perc2: numeral(0.974878234).format('0.000%'),
            perc3: numeral(-0.43).format('0 %'),
            perc4: numeral(0.43).format('(0.000 %)')
        },
        bytes: {
            byte1: numeral(100).format('0b'),
            byte2: numeral(2048).format('0 b'),
            byte3: numeral(7884486213).format('0.0b'),
            byte4: numeral(3467479682787).format('0.000 b')
        },
        datetime: {
            date1: moment(1399270077132).format('YYYY'),
            date2: moment(1399270077132).format('YYYY-MM-DD'),
            date3: moment(1399270077132).format('YYYY/MM/DD'),
            date4: moment(1399270077132).format('hh:mm'),
            date5: moment(1399270077132).format('hh:mm:ss'),
            date6: moment(1399270077132).format('h:mm:ss a'),
            date7: moment(1399270077132).format('YYYY-MM-DD hh:mm:ss'),
            date8: moment(1399270077132).format('dddd, MMMM Do YYYY, h:mm:ss a')
        }
    }
});