App.AppTableAccessView = Ember.View.extend({
    //layoutName: 'app/table/index',
    templateName: 'app/table/popups/access',

    roles: [
            { id: 'owner', name: 'Владелец'},
            { id: 'writer', name: 'Редактор'},
            { id: 'reader', name: 'Читатель'}
    ],

    accessUsers: [],

    editUser: false,

    actions: {
        addNewUser: function () {
            $('#addUserPopup').show();
            $('.overlay_access').show();
            $('body').off('keydown.closeModal');
            $('body').on('keydown.closeModal', function(ev){
                if(ev.keyCode == $.ui.keyCode.ESCAPE){
                    $('body').off('keydown.closeModal');
                    $('#addUserPopup').hide();
                    $('.overlay_access').hide();
                    $('body').on('keydown.closeModal', function(ev){
                        if(ev.keyCode == $.ui.keyCode.ESCAPE){
                            $('body').off('keydown.closeModal');
                            $('#accessPopup').hide();
                            $('.overlay').hide();
                        }
                    });
                }
            });
            return false;
        },
        saveAccess: function(){
            var me = this;
            me.get('controller').send('sharingTable');
            $('#accessPopup').hide();
            $('.overlay').hide();
            $('body').off('keydown.closeModal');
            return false;
        },
        show_popup_roles: function(user){
            console.log('click');
            var me = this,
                a = event.target.tagName == 'A' && $(event.target) || $(event.target).parents('a'),
                popup = me.$('#popup_roles'),
                pos = a.offset();
            me.set('editUser', user);
            $('body').off('mousedown.for_roles_popup');
            $('body').one('mousedown.for_roles_popup', function(ev){
                if($(ev.target).hasClass('unone')) return;
                popup.hide();
            });

            pos.top += 33;
            popup.show().width(a.width()).offset(pos);
        },
        change_role: function(role){
            var me = this,
                popup = me.$('#popup_roles');
            if(!me.get('editUser')){
                popup.hide();
                return;
            }
            Em.set(me.get('editUser'), 'role', role.id);
            popup.hide();
            me.set('editUser', false);
        }
    }

});