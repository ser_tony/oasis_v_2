App.AppCommentPopupView = Em.View.extend({
    templateName: 'app/table/popups/comments',
    grid: null,
    commentData: {},
    commentObj: {sequence_index: 0, comments:[]},
    comments: [],
    activeCell: null,
    currentComment: {},
    text: '',
    cell: null,
    max_last_comments_in_cell: 5,

    actions: {
        'close_popup': function(){
            this.$('#commentTooltip').hide();
        },
        'save_comment': function () {
            this.saveComment();
        },
        'edit_comment': function (comment) {
            if (!comment.isMyComment) return;
            this.set('currentComment', comment);
        },
        'delete_comment': function (comment) {
            var me = this,
                grid = me.get('grid'),
                cell = me.get('cell'),
                id_cell_comment = cell.getId(),
                commentObj = me.get('commentObj'),
                old_text = me.get('text'),
                controller = me.get('parentView').get('controller'),
                isUpdateCell = false;
            if (!commentObj) return;
            if (!comment || comment.author != App.User.get('email').split('@')[0]) return;
            var last_comments = _.clone(commentObj.comments).sortBy('date').reverse().slice(-1 * me.get('max_last_comments_in_cell'));
            if (last_comments.findBy('id', comment.id)) {
                isUpdateCell = true;
            }
            var result = controller.get('tableWorksheetInstance').removeTableComment(id_cell_comment, comment.id);
            if (result) {
                Em.set(commentObj, 'comments', _.without(commentObj.comments, _.find(commentObj.comments, function(c){
                    return c.id == comment.id;
                })));
                //me.$('#commentTooltip .comment_row[data-id="' + comment.id + '"]').remove();
                me.updateCellData(isUpdateCell);
            }
            me.reset();
            me.resetComments();
            me.set('text', old_text);
        }
    },

    saveComment: function () {
        var me = this,
            currentComment = me.get('currentComment'),
            selected = me.get('activeCell'),
            date = moment(),
            grid = me.get('grid'),
            controller = me.get('parentView').get('controller'),
            cell = me.get('cell'),
            commentObj = me.get('commentObj'),
            id_cell_comment = cell.getId(),
            val = me.get('text');
        if (!val.trim()) return;
        var id_comment = currentComment.id;
        if (Em.isNone(commentObj)) {
            if (id_comment) return;
            commentObj = {
                sequence_index: 0,
                comments: []
            }
        }
        var comment,
            isAddNewComment = false;
        if (id_comment) {
            var comment = commentObj.comments.findBy('id', id_comment);
            if (!comment || comment.author != App.User.get('email').split('@')[0]) return;
            comment.text = val;
            //me.$('#commentTooltip .comment_row[data-id="' + id_comment + '"] .comment-text').text(val);
        } else {
            isAddNewComment = true;
            comment = {
                'author': App.User.get('email').split('@')[0],
                'date': date,
                'text': val
            }
        }
        var new_comment = controller.get('tableWorksheetInstance').saveTableComment(comment, id_cell_comment);
        if(isAddNewComment){
            commentObj.comments.push(new_comment);
        }
        me.updateCellData(new_comment);
        commentData[id_cell_comment] = commentObj;
        me.$('#commentTooltip').hide();
        $('body').off('keydown.closeModal');
        $('body').off('click.closeCommentPopup');
        me.reset();
        me.resetComments();
    },

    updateCellData: function(new_comment){
        var me = this,
            cell = me.get('cell'),
            grid = me.get('grid'),
            controller = me.get('parentView').get('controller'),
            selected = me.get('activeCell'),
            commentObj = me.get('commentObj'),
            max_last_comments_in_cell = me.get('max_last_comments_in_cell'),
            str_cell = '',
            last_comments = commentObj.comments.slice(-1 * max_last_comments_in_cell);
        if((typeof new_comment == 'boolean' && !new_comment) || (typeof new_comment != 'boolean' && !last_comments.findBy('id', new_comment.id))) return;
        last_comments.reverse();
        for(var i=0; i<last_comments.length; i++){
            var comment = last_comments[i];
            str_cell += comment.text + ' &lt;' + comment.author + ' ' + moment(comment.date).format('YYYY-MM-DD hh:mm:ss') + '&gt;';
            if(i != last_comments.length-1) str_cell += ' | ';
        };
        if(commentObj.comments.length > max_last_comments_in_cell) str_cell += ' ...';
        if (typeof(str_cell) === 'string' && str_cell.length >= 1 && str_cell.indexOf('=') === 0) {
            str_cell = ' ' + str_cell;
        }
        cell.setValue(str_cell);
        grid.updateCell(selected.row, selected.cell);
        controller.get('tableWorksheetInstance').changeTableRow({
            grid: me.get('grid'),
            row: selected.row,
            cell: selected.cell
        });
    },

    show: function (selected) {
        var me = this,
            grid = me.get('grid'),
            data = grid.getData(),
            columnName = grid.getColumns()[selected.cell].name,
            cell = data.spreadsheet.getCell(columnName + (selected.row + 1)),
            commentObj = me.get('commentData')[cell.getId()],
            cord = $(grid.getCellNode(selected.row, selected.cell)).offset();
        me.set('cell', cell);
        me.set('activeCell', selected);
        me.set('commentObj', commentObj || {sequence_index: 0, comments:[]});
        cord.left -= 245;
        cord.top -= 1;
        me.$('#commentTooltip').css({
            'display': 'block'
        }).offset(cord);

        $('body').off('keydown.closeModal');
        $('body').on('keydown.closeModal', function(ev){
            if(ev.keyCode == $.ui.keyCode.ESCAPE){
                $('body').off('keydown.closeModal');
                me.$('#commentTooltip').hide();
            }
        })
            .on('click.closeCommentPopup', function(ev){
                if($(ev.target).hasClass('comment-tooltip')
                    || $(ev.target).parents('.comment-tooltip').size()
                    || $(ev.target).hasClass('comment_column__cell')) return;
                $('body').off('click.closeCommentPopup');
                me.$('#commentTooltip').hide();
            });
        me.$('#commentTooltipText').focus();
        me.$('.scroller_table').trigger('sizeChange');
    },

    reset: function () {
        this.set('currentComment', {
            text: '',
            author: App.User.get('email').split('@')[0]
        });
    },

    didInsertElement: function () {
        var me = this;
        me.$('.scroller_table').baron({
            bar: '.scroller_table__track',
            barOnCls: 'possibleScroll',
            direction: 'v'
        });
        me.$('#commentTooltipText').keydown(function (e) {
            if (e.ctrlKey && e.keyCode == 13) {
                // Ctrl-Enter pressed
                me.saveComment();
            }
        });
    },

    resetComments: function(){
        var me = this,
            cell = me.get('cell'),
            commentObj = me.get('commentObj'),
            comments = [];
        if (Em.isNone(commentObj) || !commentObj.comments) return;
        _.each(commentObj.comments, function (c) {
            comments.push({
                id: c.id,
                author: c.author,
                isMyComment: c.author == App.User.get('email').split('@')[0],
                text: c.text
            });
        });
        me.set('comments', comments.reverse());
        Em.run.later(function(){
            me.$('.scroller_table').trigger('sizeChange');
        }, 100);
    },

    currentCommentObserve: function () {
        var me = this;
        me.set('text', me.get('currentComment').text);
    }.observes('currentComment'),

    commentObjCommentsObserve: function () {
        this.resetComments();
    }.observes('commentObj.comments')
});