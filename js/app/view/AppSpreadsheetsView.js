App.AppSpreadsheetsView  = Ember.View.extend({
    layoutName: 'app/spreadsheets/index',
    templateName: 'app/spreadsheets/spreadsheets-list',
    actions: {
        'show_modal_delete': function(){
            if($('#js-spreadsheetDelete').hasClass('disabled')) return;
            $('#deletePopup').show();
        },
        'hide_modal_delete': function(){
            $('#deletePopup').hide();
        }
    }
});