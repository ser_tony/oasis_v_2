App.AppConditionalRuleView = Em.View.extend({
    templateName: 'app/table/popups/sidebar/conditional_rule',

    conditionalTypeTextTranslation: 'text_contains:Текст содержит;text_not_contains:Текст не содержит;text_equal:Текст в точности;empty:Ячейка не заполнена',
    conditionalTypeDateTranslation: 'date_equal:Дата;date_before:Дата до;date_after:Дата после;date_between:Дата между;date_not_between:Дата не между',
    conditionalTypeNumberTranslation: 'gt:Больше;lt:Меньше;equal:Равно;not_equal:Не равно;between:Между;not_between:Не между',

    model: {},
    patternInput: '^.+$',
    // inputes = [ {label: 'Минимум', type: 'date'} ]
    inputs: {
        'text_input': [{type:'text'}],
        'date_input': [{type:'text'}],
        'date_between': [{type:'text', label: 'min'}, {type: 'text', label: 'max'}],
        'number_input': [{type:'number'}],
        'number_between': [{type:'number', label: 'min'}, {type:'number', label: 'max'}],
        'empty': []
    },

    bg_color: 'black',
    font_color: 'black',
    current_inputs: [],
    current_conditional_types: [],
    current_conditional_type: null,

    init: function(){
        var me = this;
        me._super();
        // convert translated text to data
        me.set('current_conditional_types', []);
        me.get('current_conditional_types').pushObjects(me._translated_to_dict(me.get('conditionalTypeTextTranslation')));
        me.get('current_conditional_types').pushObjects(me._translated_to_dict(me.get('conditionalTypeDateTranslation')));
        me.get('current_conditional_types').pushObjects(me._translated_to_dict(me.get('conditionalTypeNumberTranslation')));
    },

    _translated_to_dict: function(translated_string){
        return _.map(translated_string.split(';'), function(term){
            var key_value = _.map(term.split(':'), function(v){return v.trim()});
            return {
                value: key_value[0],
                label: key_value[1]
            };
        });
    },

    actions: {
        'remove_rule': function(){
            this.get('parentView').remove_rule(this.get('model'));
        },
        'change_condition_value': function(){
            var me = this,
                el = $(event.target);
            me._get_value(el);
        },
        'toggle_color': function(attr){
            var me = this;
            me.set(attr, !me.get(attr));
        }
    },

    _get_value: function(el){
        var me = this,
                model = me.get('model');
        el = $(el);
        me.validate_format_data_validation(el);
        if(!me.get('model').is_form_error){
            var inputs = me.$('.input_condition_value');
            if(inputs.length == 1){
                Em.set(model, 'value', el.val());
            } else if(inputs.length == 2){
                if(!model.value || !_.isArray(model.value)) Em.set(model, 'value', []);
                model.value[inputs.index(el)] = el.val();
            }
        }
    },

    validate_format_data_validation: function(el){
        if(!el) return;
        var me = this,
            model = me.get('model');
        Em.set(model, 'is_form_error', false);
        el.each(function(){
            var el = $(this),
                val = el.val(),
                reg_pattern = new RegExp(me.get('patternInput') || (me.get('isDateType') && '^\\d{4}-\\d{2}-\\d{2}$') || '^.+$');
            el.removeClass('has_error');
            if(!val.match(reg_pattern)){
                Em.set(model, 'is_form_error', true);
                el.addClass('has_error');
            }
        });
    },

    init_datepicker: function(){
        var me = this;
        if(me.get('isDateType')){
            me.$('.input_condition_value').each(function(){
                var el = this;
                new Pikaday({
                    field: el,
                    onSelect: function(date) {
                        el.value = moment(date).format('YYYY-MM-DD');
                        me._get_value(el);
                    }
                });
            });
        }
    },

    didInsertElement: function(){
        var me = this;
        me.$(".colorpicker").spectrum({
            showPaletteOnly: true,
            showPalette: true,
            showInput: true,
            color: "black",
            chooseText: "Выбрать",
            cancelText: "Отмена",
            palette: [me.get('parentView').get('parentView').get('options').bgColorsRGB]
        });
        me.$(".select").chosen();
    },

    // Observers
    modelObserver: function(){
        var me = this,
            model = me.get('model');
        if(!model) return;
        if(typeof model.current_conditional_type == 'string'){
            Em.set(model, 'current_conditional_type', me.get('current_conditional_types').findBy('value', model.current_conditional_type));
        }
        if(!model.current_conditional_type) Em.set(model, 'current_conditional_type', me.get('current_conditional_types')[0]);

        me.set('current_conditional_type', model.current_conditional_type);
        Em.run.later(function(){
            me.beginPropertyChanges();
            me.set('isBgColor', !!model.bg_color);
            me.set('bg_color', model.bg_color);
            me.set('isFontColor', !!model.font_color);
            me.set('font_color', model.font_color);
            me.endPropertyChanges();
            Em.run.later(function(){
                var select = me.$('.select_conditional_type');
                select.trigger("liszt:updated");
                if(_.isArray(model.value)){
                    var values = model.value,
                        i = 0;
                    me.$('.input_condition_value').each(function(){
                        $(this).val(values[i]);
                        i++;
                    });
                } else {
                    me.$('.input_condition_value').val(model.value);
                }
                me.init_datepicker();
                me.validate_format_data_validation(me.$('.input_condition_value'));
            }, 200);
        }, 200);
    }.observes('model').on('init'),

    current_conditional_typeObserver: function(){
        var me = this,
            current_conditional_type = me.get('current_conditional_type');
        if(!current_conditional_type) return;
        current_conditional_type = current_conditional_type.value;
        // set inputs
        if(_.contains(['text_contains', 'text_not_contains', 'text_equal'], current_conditional_type)){
            me.set('current_inputs', me.get('inputs')['text_input']);
        } else if(_.contains(['date_equal', 'date_before', 'date_after'], current_conditional_type)){
            me.set('current_inputs', me.get('inputs')['date_input']);
        } else if(_.contains(['date_between', 'date_not_between'], current_conditional_type)){
            me.set('current_inputs', me.get('inputs')['date_between']);
        } else if(_.contains(['gt', 'lt', 'equal', 'not_equal'], current_conditional_type)){
            me.set('current_inputs', me.get('inputs')['number_input']);
        } else if(_.contains(['between', 'not_between'], current_conditional_type)){
            me.set('current_inputs', me.get('inputs')['number_between']);
        } else if(_.contains(['empty'], current_conditional_type)){
            me.set('current_inputs', me.get('inputs')['empty']);
        }
        me.set('isDateType', _.contains(['date_equal', 'date_before', 'date_after', 'date_between', 'date_not_between'],
            current_conditional_type));
        Em.set(me.get('model'), 'current_conditional_type', current_conditional_type);
    }.observes('current_conditional_type'),

    current_inputsObserver: function(){
        var me = this;
        if(!me.$()) return;
        Em.run.later(me, me.init_datepicker, 100);
    }.observes('current_inputs'),

    isBgColorObserve: function(){
        var me = this,
            color = me.get('bg_color') || 'black';
        me.set('bg_color', me.get('isBgColor') && color || false);
        me.set('model.bg_color', me.get('isBgColor') && color || false);
    }.observes('isBgColor'),

    isFontColorObserve: function(){
        var me = this,
            color = me.get('font_color') || 'black';
        me.set('font_color', me.get('isFontColor') && color || false);
        me.set('model.font_color', me.get('isBgColor') && color || false);
    }.observes('isFontColor'),

    bg_colorObserve: function(){
        var me = this,
            color = me.get('bg_color') || 'black';
        if(!me.get('isBgColor')) return;
        me.set('model.bg_color', color);
        me.$() && me.$('.colorpicker.bg_color').spectrum('set', color);
    }.observes('bg_color'),

    font_colorObserve: function(){
        var me = this,
            color = me.get('font_color');
        if(!me.get('isFontColor')) return;
        me.set('model.font_color', color);
        me.$() && me.$('.colorpicker.font_color').spectrum('set', color);
    }.observes('font_color')
});