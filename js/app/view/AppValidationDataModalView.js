App.AppValidationDataModalView = Em.View.extend({
    templateName: 'app/table/popups/sidebar/validation_data',

    validatorTypeTextTranslation: 'text_contains:Текст содержит;text_not_contains:Текст не содержит;text_equal:Текст в точности',
    validatorTypeDateTranslation: 'date_equal:Дата;date_before:Дата до;date_after:Дата после;date_between:Дата между;date_not_between:Дата не между',
    validatorTypeNumberTranslation: 'gt:Больше;lt:Меньше;equal:Равно;not_equal:Не равно;between:Между;not_between:Не между',
    translateErrorFormatDataValidationCondition: 'Введите допустимое значение',

    grid: null,
    activeCell: null,

    patternInput: '^.+$',
    validator_types: {
        'text': [],
        'date': [],
        'number': []
    },
    // inputes = [ {label: 'Минимум', type: 'date'} ]
    inputs: {
        'text_input': [{type:'text'}],
        'date_input': [{type:'text'}],
        'date_between': [{type:'text', label: 'min'}, {type: 'text', label: 'max'}],
        'number_input': [{type:'number'}],
        'number_between': [{type:'number', label: 'min'}, {type:'number', label: 'max'}]
    },
    current_inputs: [],

    current_type: '',
    current_validator_types: [],
    current_validator_type: null,

    form_error_msg: '',
    hint_msg: '',
    validate_error_msg: '',

    isAllowOnlyValidValue: false,
    isShowHint: false,
    isInteger: true,
    isFloat: false,
    isTextType: function(){return this.get('current_type') == 'text'}.property('current_type'),
    isDateType: function(){return this.get('current_type') == 'date'}.property('current_type'),
    isNumberType: function(){return this.get('current_type') == 'number'}.property('current_type'),

    init: function(){
        var me = this;
        me._super();
        // convert translated text to data
        me.set('validator_types.text', me._translated_to_dict(me.get('validatorTypeTextTranslation')));
        me.set('validator_types.date', me._translated_to_dict(me.get('validatorTypeDateTranslation')));
        me.set('validator_types.number', me._translated_to_dict(me.get('validatorTypeNumberTranslation')));
        // set current type by default
        me.set('current_type', 'text');
    },

    _translated_to_dict: function(translated_string){
        return _.map(translated_string.split(';'), function(term){
            var key_value = _.map(term.split(':'), function(v){return v.trim()});
            return {
                value: key_value[0],
                label: key_value[1]
            };
        });
    },

    open_modal: function(){
        var me = this,
            validator_types = me.get('validator_types'),
            current_type = 'text',
            is_found = false,
            grid = me.get('grid'),
            activeCell = grid.getActiveCell(),
            column_validation = grid.getColumns()[activeCell.cell].validation;
        if(column_validation){
            if(me.get('validator_types.text').findBy('value', column_validation.current_validator_type)){
                current_type = 'text';
            } else if(me.get('validator_types.date').findBy('value', column_validation.current_validator_type)) {
                current_type = 'date';
            } else {
                current_type = 'number';
            }
            me.set('current_type', current_type);
            me.set('isShowHint', !!column_validation.hint_msg);
            me.set('hint_msg', column_validation.hint_msg);
            me.set('isAllowOnlyValidValue', !!column_validation.validate_error_msg);
            me.set('validate_error_msg', column_validation.validate_error_msg);
            Em.run.later(function(){
                me.set('current_validator_type', me.get('validator_types.' + current_type).findBy('value', column_validation.current_validator_type));
                var select = me.$('.select_validator_type');
                Ember.run.later(select, select.trigger, "liszt:updated", 200);
                Em.run.later(function(){
                    if(_.isArray(column_validation.value)){
                        var values = column_validation.value,
                            i = 0;
                        me.$('.validation_condition_data input').each(function(){
                            $(this).val(values[i]);
                            i++;
                        });
                    } else {
                        me.$('.validation_condition_data input').val(column_validation.value);
                    }
                    me.validate_format_data_validation(me.$('.validation_condition_data input'));
                }, 200);
            }, 200);
        } else {
            me.reset_form();
        }
        // show modal
        me.$('#validatorDataModal').show();
        $('.overlay').show();
        $('body').on('keydown.closeConfirm', function(ev){
            if(ev.keyCode == $.ui.keyCode.ESCAPE){
                $('body').off('keydown.closeModal');
                $('#validatorDataModal').hide();
                $('.overlay').hide();
            }
        });
    },

    actions: {
        'set_type': function(type){
            var me = this,
                el = event.target.tagName == 'A' && $(event.target) || $(event.target).parents('a');
            me.set('current_type', type);
        },
        'toggle_is_show_hint': function(){
            var me = this,
                is_show_hint = me.get('isShowHint');
            me.set('isShowHint', !is_show_hint);
        },
        'toggle_is_only_valid_data': function(){
            var me = this,
                isAllowOnlyValidValue = me.get('isAllowOnlyValidValue');
            me.set('isAllowOnlyValidValue', !isAllowOnlyValidValue);
        },
        'change_is_integer': function(is_integer){
            var me = this;
            me.set('isInteger', is_integer);
            me.set('isFloat', !is_integer);
        },
        'change_validation_value': function(){
            var me = this,
                el = $(event.target);
            me.validate_format_data_validation(el);
        },
        'apply_validation': function(){
            var me = this,
                grid = me.get('grid'),
                columns = grid.getColumns(),
                validation,
                controller = me.get('parentView').get('parentView').get('controller'),
                changed_columns = [],
                ranges = grid.getSelectionModel().getSelectedRanges();
            me.validate_format_data_validation(me.$('.validation_condition_data input'));
            if(me.get('form_error_msg')) return;
            validation = {
                current_validator_type: me.get('current_validator_type').value,
                current_type: me.get('current_type'),
                hint_msg: me.get('isShowHint') && me.get('hint_msg') || false,
                validate_error_msg: me.get('isAllowOnlyValidValue') && me.get('validate_error_msg') || false,
                value: _.map(me.$('.validation_condition_data input'), function(el){
                    return $(el).val();
                })
            }
            if(validation.value.length == 1){
                validation.value = validation.value[0];
            }
            _.each(ranges, function(range){
                for(var cell_i = range.fromCell; cell_i <= range.toCell; cell_i++){
                    columns[cell_i].validation = validation;
                    changed_columns.push([columns[cell_i]]);
                }
            });
            grid.setColumns(columns);
            controller.get('tableWorksheetInstance').changeColumns(changed_columns);
            // hde modal
            me.$('#validatorDataModal').hide();
            $('.overlay').hide();
            $('body').off('keydown.closeModal');
        },
        'remove_validation': function(){
            var me = this,
                grid = me.get('grid'),
                columns = grid.getColumns(),
                controller = me.get('parentView').get('parentView').get('controller'),
                changed_columns = [],
                ranges = grid.getSelectionModel().getSelectedRanges();
            _.each(ranges, function(range){
                for(var cell_i = range.fromCell; cell_i <= range.toCell; cell_i++){
                    columns[cell_i].validation = false;
                    changed_columns.push([columns[cell_i]]);
                }
            });
            grid.setColumns(columns);
            controller.get('tableWorksheetInstance').changeColumns(changed_columns);
            $('#js__validator_hint_message').hide();
            // hde modal
            me.$('#validatorDataModal').hide();
            $('.overlay').hide();
            $('body').off('keydown.closeModal');
        }
    },

    validate_format_data_validation: function(el){
        if(!el) return;
        var me = this;
        me.set('form_error_msg', '');
        el.each(function(){
            var el = $(this),
                val = el.val(),
                reg_pattern = new RegExp(me.get('patternInput') || (me.get('current_type') == 'date' && '^\\d{4}-\\d{2}-\\d{2}$') || '^.+$');
            el.removeClass('has_error');
            if(!val.match(reg_pattern)){
                me.set('form_error_msg', me.get('translateErrorFormatDataValidationCondition'));
                el.addClass('has_error');
            }
        });
    },

    reset_form: function(){
        var me = this;
        me.set('current_type', 'text');
        me.set('isShowHint', false);
        me.set('isAllowOnlyValidValue', false);
    },

    init_datepicker: function(){
        var me = this;
        if(me.get('current_type') != 'date') return;
        me.$('.validation_condition_data input').each(function(){
            var el = this;
            new Pikaday({
                field: el
            });
        });
    },

    // Observes
    current_typeObserver: function(){
        var me = this,
            current_type = me.get('current_type'),
            current_validator_types = me.get('validator_types')[current_type],
            current_validator_type = current_validator_types[0],
            select = me.$('.select_validator_type');
        me.set('current_validator_types', current_validator_types);
        //if(!_.contains(current_validator_types, current_validator_type)){
        me.set('current_validator_type', current_validator_type);
        //}
        if(select) {
            Ember.run.later(select, select.trigger, "liszt:updated", 100);
        }
        if(!me.$()) return;
        me.$('.number_formatting__menu a.active').removeClass('active');
        me.$('.number_formatting__menu a[data-id="' + current_type + '"]').addClass('active');
    }.observes('current_type').on('init'),

    current_validator_typeObserver: function(){
        var me = this,
            current_validator_type = me.get('current_validator_type');
        if(!current_validator_type) return;
        current_validator_type = current_validator_type.value;
        // set inputs
        if(_.contains(['text_contains', 'text_not_contains', 'text_equal'], current_validator_type)){
            me.set('current_inputs', me.get('inputs')['text_input']);
        } else if(_.contains(['date_equal', 'date_before', 'date_after'], current_validator_type)){
            me.set('current_inputs', me.get('inputs')['date_input']);
        } else if(_.contains(['date_between', 'date_not_between'], current_validator_type)){
            me.set('current_inputs', me.get('inputs')['date_between']);
        } else if(_.contains(['gt', 'lt', 'equal', 'not_equal'], current_validator_type)){
            me.set('current_inputs', me.get('inputs')['number_input']);
        } else if(_.contains(['between', 'not_between'], current_validator_type)){
            me.set('current_inputs', me.get('inputs')['number_between']);
        }
    }.observes('current_validator_type'),

    current_inputsObserver: function(){
        var me = this;
        if(!me.$()) return;
        Em.run.later(me, me.init_datepicker, 100);
    }.observes('current_inputs'),

    isIntegerObserver: function(){
        var me = this,
            current_type = me.get('current_type'),
            isInteger = me.get('isInteger');
        if(current_type != 'number'){
            me.set('patternInput', '^.+$');
        } else {
            if(isInteger){
                me.set('patternInput', '^[0-9]+$');
            } else {
                me.set('patternInput', '^\\d+(\\.\\d+)?$');
            }
        }
        me.validate_format_data_validation(me.$('.validation_condition_data input'));
    }.observes('isInteger', 'current_type'),

    activeCellObserver: function(){
        var me = this,
            hint_block = $('#js__validator_hint_message'),
            activeCell = me.get('activeCell'),
            activeCellNode = me.get('grid').getActiveCellNode(),
            column = me.get('grid').getColumns()[activeCell.cell];
        hint_block.hide();
        if(!column.validation || !column.validation.hint_msg) return;
        hint_block.removeClass('invalid').text(column.validation.hint_msg)
            .show().position({
                my: 'left bottom',
                at: 'right top',
                of: activeCellNode
            });
    }.observes('activeCell'),

    isShowHintObserve: function(){
        var me = this,
            is_show_hint = me.get('isShowHint'),
            el = me.$('.input_hint_msg');
        if(is_show_hint){
            el.removeAttr('disabled');
        } else {
            el.attr('disabled', 'disabled');
        }
    }.observes('isShowHint'),

    isAllowOnlyValidValueObserve: function(){
        var me = this,
            isAllowOnlyValidValue = me.get('isAllowOnlyValidValue'),
            el = me.$('.input_validate_error_msg');
        if(isAllowOnlyValidValue){
            el.removeAttr('disabled');
        } else {
            el.attr('disabled', 'disabled');
        }
    }.observes('isAllowOnlyValidValue')
});