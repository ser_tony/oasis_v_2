App.AppTableView = Ember.View.extend({
    //layoutName: 'app/table/index',
    templateName: 'app/table/index',

    cellCssStyleKey: 'cell_css_style_key',

    isFrozenColumns: false,
    isFrozenRows: false,

    //child Views
    popupCommentView: App.AppCommentPopupView,


    init: function () {
        //this.initColumnSettings();
        this._super();
    },

    preloaderOn: function(){
        var me = this,
            preloaderOpt = {
                width: 200,
                height: 200,
                backgroundColor: '#ffffff',
                stepsPerFrame: 4,
                trailLength: 1,
                pointDistance: 0.01,
                fps: 25,

                setup: function() {
                    this._.lineWidth = 10;
                },

                step: function(point, i, f) {

                    var progress = point.progress,
                        degAngle = 360 * progress,
                        angle = Math.PI/180 * degAngle,
                        angleB = Math.PI/180 * (degAngle - 180),
                        size = i*5;

                    this._.fillStyle = '#FF7B24';

                    this._.fillRect(
                        Math.cos(angle) * 25 + (50-size/2),
                        Math.sin(angle) * 15 + (50-size/2),
                        size,
                        size
                    );

                    this._.fillStyle = '#63D3FF';

                    this._.fillRect(
                        Math.cos(angleB) * 15 + (50-size/2),
                        Math.sin(angleB) * 25 + (50-size/2),
                        size,
                        size
                    );

                    if (point.progress == 1) {

                        this._.globalAlpha = f < .5 ? 1-f : f;

                        this._.fillStyle = '#c8c8c8';

                        this._.beginPath();
                        this._.arc(50, 50, 5, 0, 360, 0);
                        this._.closePath();
                        this._.fill();
                    }
                },

                path: [
                    ['line', 0, 0, 1, 1] // stub -- not actually rendered
                ]
        };
        if (!me.get('preloader')){
            var a = new Sonic(preloaderOpt);
            var table= $('#tableBody'),
                tOffset = table.offset(),
                sonicDiv = $('<div class="sonic_div" />')
                            .height(table.height())
                            .width(table.width())
                            .offset({'left':tOffset.left, 'top': tOffset.top});
            $('.table_body_wrapper').prepend(sonicDiv.append(a.canvas));
            var twrap = $('.table_body_wrapper');

            a.canvas.style.marginTop = (twrap.height()/2 - a.fullHeight/2)  + 'px';
            a.canvas.style.marginLeft = (twrap.width()/2 - a.fullWidth/2)   + 'px';
            a.play();
            me.set('preloader', sonicDiv);
        }
        else {
            me.get('preloader').show();
        }

    },
    loading: false,
    preloader: null,
    loadingChange: function(){
      var me = this;
        if(me.get('loading')){
            me.preloaderOn();
        }
        else{
            me.get('preloader').hide();
        }
    }.observes('loading'),

    grid: null,

    didInsertElement: function () {
        var me = this;
        me.get('controller').set('view', me);
        me.set('loading', true);
    },



    //TODO: copying range of cells

    actions:{
        'setFunction': function(){
            $('body').on('keydown.closeModal', function(ev){
                if(ev.keyCode == $.ui.keyCode.ESCAPE){
                    $('body').off('keydown.closeModal');
                    $('#functionPopup').hide();
                    $('.overlay').hide();
                }
            });
            $('#functionPopup').show();
            $('.overlay').show();
            $('.scroller_function').baron({
                bar: '.scroller_function__track',
                barOnCls: 'possibleScroll',
                direction: 'v'
            });
            return false;
        },

        //undo/rendo
        'undo': function(){
            var me = this;
            me.undo();
        },

        'redo': function(){
            var me = this;
            me.redo();
        },

        'frozen_rows_columns': function(){
            var me = this,
                grid = me.get('grid'),
                columns = grid.getColumns(),
                modal = $('#lockedRegionPopup'),
                rows = parseInt(modal.find('select.rows').val(), 10),
                columns = parseInt(modal.find('select.columns').val(), 10);
            if(!me.get('isFrozenColumns') || _.isNaN(columns)){
                columns = -1;
            }
            if(!me.get('isFrozenRows') || _.isNaN(rows)){
                rows = -1;
            }
            grid.scrollRowToTop(0);
            setTimeout(function(){
                grid.setOptions({
                    frozenColumn: columns,
                    frozenRow: rows
                });
            }, 400);
            modal.hide();
            $('.overlay').hide();
        }

    },

    active_string_val: '',

    changeActiveStr: function(){
        /**
         * пользователь изменил занчение ячейки через командную строку формулы
         */
        var me = this,
            activeCell = me.get('grid').getActiveCell();
        if (activeCell){
            var columnName = me.get('grid').getColumns()[activeCell.cell].name;
            activeCell.position = columnName + (activeCell.row + 1);
            activeCell.value = me.get('active_string_val');

            me.updateGridItem(activeCell, true);
        }

    }.observes('active_string_val'),


    //настройка столбцов
    isColumnsInit: false,
    columns: [
        {   id: "sel",
            name: "num",
            field: "num",
            width: 100,
            resizable: false,
            selectable: false,
            focusable: false
        }
        //	{id: "", name: "", field: "", width: 100, editor: Slick.Editors.Text, formatter: formatter},
    ],

//    initColumnSettings: function () {
//        var me = this;
//        if (me.columns.length > 1) {
//            return;
//        }
//        me.set('isColumnsInit', true);
//        for (var i = 1; i < en_symbs.length; i++) {
//            me.columns.push({
//                id: i,
//                name: en_symbs[i],
//                field: en_symbs[i],
//                width: 60,
//                editor: FormulaEditor
//                // selectable: true
//            });
//            //me.columns[i].id = i;
//            //me.columns[i].name = columns[i].field = me.en_symbs[i];
//        }
//    },

    columnFilters: {},
    activeCell: null,

    dataView: null,
    data: [],
    dataGrid: null,
    tableData: null,
    tableInitOptions: {
        showHeaderRow: true,
        headerRowHeight: 25,
        rowHeight: 25,
        explicitInitialization: true,
        enableColumnReorder: true,
        //enableAsyncPostRender:	true,
        enableAddRow: true
    },

    // GLOBAL vars изменяя эти переменные, все остальное будет перерисовываться
    globalCommentData: null,
    globalCellCssStyles: {},

    //очередь команд плагина //////////////////////////////////
    //fixing undo/redo
    existUndo: function(){
        var me = this;
        return me.get('commandCtr') != 0;
    }.property('commandCtr'),

    existRedo: function(){
        return !(this.get('commandCtr') >= this.commandQueue.length);
    }.property('commandCtr'),


      commandQueue : [],
      commandCtr : 0,

      queueAndExecuteCommand : function(editCommand) {
        var me = this;
          /** 2 -варианта коммады:
           *  -применить к ячейке
           *  -применить к диапазону
           */
        if (editCommand instanceof Array){
            //команда для ячейки
            editCommand = arguments[2];
        }
        me.commandQueue[me.get('commandCtr')] = editCommand;
        me.set('commandCtr', me.get('commandCtr') + 1);
        editCommand.execute();
      },

      undo : function() {
        var me = this;
        if (me.get('commandCtr') == 0)
          return;

        me.set('commandCtr', me.get('commandCtr') - 1);
        var command = me.commandQueue[this.get('commandCtr')];

        if (command && Slick.GlobalEditorLock.cancelCurrentEdit()) {
          command.undo();
        }
        me.get('grid').gotoCell(command.row, command.cell, false);
      },
      redo : function() {
        var me = this;
        if (me.get('commandCtr') >= me.commandQueue.length)
          return;
        var command = me.commandQueue[me.get('commandCtr')];
        me.set('commandCtr', me.get('commandCtr') + 1);
        if (command && Slick.GlobalEditorLock.cancelCurrentEdit()) {
          command.execute();
        }
        me.get('grid').gotoCell(command.row, command.cell, false);
      },

/////////////////////////////////



//    filter: function (item) {
//        var me = this;
//        //фильтр для столбцов
//        for (var columnId in me.columnFilters) {
//            if (columnId !== undefined && me.columnFilters[columnId] !== "") {
//                var c = grid.getColumns()[grid.getColumnIndex(columnId)];
//                if (item[c.field] != me.columnFilters[columnId]) {
//                    return false;
//                }
//            }
//        }
//        return true;
//    },


    createGrid: function (eventInit) {
        /** создается грид только после полной загрузки модели */
        var me = this,
            eventInit = eventInit || true,
            controller = me.get('controller'),
            tableData = controller.get('tableWorksheetInstance').getTableDataRows();
        Em.set(me.get('tableInitOptions'), 'rowHeight', controller.get('tableWorksheetInstance').getSettingByKey('row_height') || 25);
        me.set('tableData', tableData);
        commentData = me.cloneDataObject(controller.get('tableWorksheetInstance').getTableCommentData());
        me.set('globalCommentData', commentData);


        var dataView = me.get('dataView'),
            data = me.get('data'),
            grid = me.get('grid');


//        var groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider();
//        dataView = new Slick.Data.DataView({
//            groupItemMetadataProvider: groupItemMetadataProvider,
//            inlineFilters: true
//        });


        // prepare the data for initialization

        //data = me.cloneDataObject(tableData);
        data = tableData;
        me.set('dataGrid', data);

        /******************* INIT TABLE PLUGIN *********************/
        dataView = new Slick.Data.DataView();

        //realizing undo operation
        $.extend(me.tableInitOptions, {editCommandHandler: function(p1,p2,p3){
            me.queueAndExecuteCommand(p1,p2,p3);
        }});

        spreadsheetSlick = new Spreadsheet.Slick('#tableBody',data, me.tableInitOptions, controller);
        //grid = new Slick.Grid("#tableBody", dataView, me.columns, me.tableInitOptions);
        //ссылка на экземпляр
        me.set('spreadsheetSlick', spreadsheetSlick);

        grid = spreadsheetSlick.getGrid();
        // register the group item metadata provider to add expand/collapse group handlers
        //grid.registerPlugin(groupItemMetadataProvider);

        // Подсветка выделенных ячеек слева и сверху таблицы
        grid.getSelectionModel().onSelectedRangesChanged.subscribe(function(event, ranges){
            var range = ranges[0],
                cellNode;
            $('.slick-row.active').removeClass('active');
            $('.slick-header-column.active').removeClass('active');
            for(var col_i = range.fromCell; col_i<=range.toCell; col_i++){
                for(var row_i = range.fromRow; row_i<=range.toRow; row_i++){
                    cellNode = grid.getCellNode(row_i, col_i);
                    if($(cellNode).parents('.slick-pane-top').hasClass('slick-pane-left')){
                        $(cellNode).parents('.slick-row').addClass('active');
                    } else if($(cellNode).parents('.slick-pane-bottom').hasClass('slick-pane-left')){
                        $(cellNode).parents('.slick-row').addClass('active');
                    } else {
                        var slick_row =  $(cellNode).parents('.slick-row'),
                            index = $(cellNode).parents('.grid-canvas').find('.slick-row').index(slick_row);
                        var cls = '.slick-pane-bottom';
                        if($(cellNode).parents('.slick-pane-top').size()){
                            cls = '.slick-pane-top';
                        }
                        $($('#tableBody ' + cls + '.slick-pane-left .slick-row')[index]).addClass('active');
                    }
                }
                $($('.slick-header-column')[col_i]).addClass('active');
            }
        });

        //установить фокус на таблице
        grid.getCanvasNode().focus();


        //excel-copying plugin
        var pluginOptions = {
            clipboardCommandHandler: function(editCommand){
                me.queueAndExecuteCommand(editCommand);
            },
            includeHeaderWhenCopying : false,
            dataItemColumnValueSetter: function(cell, column, newValue ){}
        };
        var excelCopyManager = new Slick.CellExternalCopyManager(pluginOptions);
        grid.registerPlugin(excelCopyManager);
        excelCopyManager.onPasteCells.subscribe(function (e, args) {
            if (args.ranges && args.ranges[0].fromExcel){
                    me.get('controller').get('tableWorksheetInstance').changeTableRows(args.ranges[0].changedCells);
            }
        });


        //плагин для копипаста по отдельным ячейкам
        var copyManager = new Slick.CellCopyManager({
            'commandHandler': function(editCommand){
                me.queueAndExecuteCommand(editCommand);
            }
        });
        grid.registerPlugin(copyManager);

        //эвент кописпастинга
        copyManager.onPasteCells.subscribe(function (e, args) {
            me.copySelectedRange(e, args);
        });


        var overlayPlugin = new Ext.Plugins.Overlays({});
        // Event fires when a range is selected
        overlayPlugin.onFillUpDown.subscribe(function (e, args) {
            me.copySelectedRange(e, args, true);
        });
        grid.registerPlugin(overlayPlugin);

        grid.onValidationError.subscribe(function (e, args) {
            var hint_block = $('#js__validator_hint_message'),
                activeCellNode = grid.getActiveCellNode();
            hint_block.hide();
            hint_block.removeClass('invalid').addClass('invalid').text(args.validationResults.msg)
                .show().position({
                    my: 'left bottom',
                    at: 'right top',
                    of: activeCellNode
                });
        });

        grid.onSetColumns.subscribe(function(e, columns){
            controller.get('tableWorksheetInstance').setColumns(columns);
        });


        grid.onAddNewRow.subscribe(function (e, args) {
            var item = args.item;
            var column = args.column;
            grid.invalidateRow(data.length);
            data.push(item);
            grid.updateRowCount();
            grid.render();
        });

        grid.onScroll.subscribe(function(e){
            $("#contextMenu").hide();
        });

        grid.onContextMenu.subscribe(function (e) {
            e.preventDefault();
            var cell = grid.getCellFromEvent(e);
            grid.setActiveCell(cell.row, cell.cell);
            var tableDiv = $('#tableBody'),
                tableOffset= tableDiv.offset(),
                tableH = tableDiv.height(),
                tableW = tableDiv.width(),
                menu = $("#contextMenu"),
                menuH = menu.height(),
                menuW = menu.width();

            var pageY = e.pageY,
                pageX = e.pageX;

            if ( (pageY + menuH) >= (tableOffset.top + tableH)){
                pageY = pageY - menuH;
            }
            if ( (pageX + menuW) >= (tableOffset.left + tableW) ){
                pageX = pageX - menuW;
            }

            menu
                .data("row", cell.row)
                .css("top", pageY)
                .css("left",pageX)
                .show();

            $("body").one("click", function () {
                menu.hide();
            });
        });

         grid.onSort.subscribe(function (e, args) {
            currentSortCol = args.sortCol;
            me.get('controller').set('isAsc', args.sortAsc);
            grid.invalidateAllRows();
            grid.render();
         });

        grid.onKeyDown.subscribe(function(e, obj){
            var code = e.which;
            //delete
            if (code == $.ui.keyCode.DELETE ){
                var delRange = grid.getSelectionModel().getSelectedRanges()[0],
                    cell = grid.getActiveCell();
                var deleteCommand = {
                    delRange: delRange,
                    row: cell.row,
                    cell:cell.cell,
                    oldValues: [],
                    onlyOne: false,
                    execute: function() {
                        e.stopPropagation();
                        if (delRange.fromCell == delRange.toCell && delRange.fromRow == delRange.toRow) {
                            //выбрана только одна ячейка
                           this.onlyOne = true;
                           var columnName = grid.getColumns()[obj.cell].name,
                               cell = {
                                 row: this.row,
                                 cell: this.cell,
                                 position: columnName + (obj.row + 1),
                                 value: ''
                               };
                            var cellObject = me.get('grid').getData().spreadsheet.getCell(cell.position);
                            this.oldValues = _.clone(cell);
                            this.oldValues.value = cellObject.isFormula()? cellObject.formula: cellObject.value;

                            me.updateGridItem(cell, true);
                        }
                        else{
                            var changedCells = [];
                             for (var row_i = delRange.fromRow; row_i <= delRange.toRow; row_i++) {
                                this.oldValues[row_i] = [];
                                for (var cell_i = delRange.fromCell; cell_i <= delRange.toCell; cell_i++) {
                                    var from_column_name = grid.getColumns()[cell_i].name,
                                        from_cell_position = from_column_name + '' + (row_i + 1),
                                        cell = grid.getData().spreadsheet.getCell(from_cell_position);
                                    var oldValue = cell.isFormula()? cell.formula: cell.value;
                                    this.oldValues[row_i][cell_i] = {
                                         'value': oldValue,
                                         'formula': cell.formula || oldValue,
                                         'id': cell.getId()
                                    }
                                    cell.setValue('');
                                    changedCells.push(cell);
                                    grid.invalidateRow(delRange.fromRow + (row_i-delRange.fromRow));
                                }
                            }
                            //синхронизировать с realtime doc
                            grid.render();
                            controller.get('tableWorksheetInstance').changeTableRows(changedCells);
                        }
                    },

                    undo: function() {
                        if (this.onlyOne) {
                            me.updateGridItem(this.oldValues, true);
                        }
                        else {
                            var changedCells = [];
                             for (var row_i = this.delRange.fromRow; row_i <= this.delRange.toRow; row_i++) {
                                for (var cell_i = this.delRange.fromCell; cell_i <= this.delRange.toCell; cell_i++) {
                                    var cellObj = this.oldValues[row_i][cell_i];
                                    var cell = grid.getData().spreadsheet.getCell(cellObj.id);
                                    cell.setValue(cellObj.formula);
                                    changedCells.push(cell);
                                    grid.invalidateRow(this.delRange.fromRow + (row_i-this.delRange.fromRow));
                                }
                            }
                            //синхронизировать с realtime doc
                            grid.render();
                            controller.get('tableWorksheetInstance').changeTableRows(changedCells);
                        }
                    }

                };

                me.queueAndExecuteCommand(deleteCommand);

            }
            else if (code == $.ui.keyCode.ENTER) {
                e.stopImmediatePropagation();
                grid.navigateDown();
            }

            // undo shortcut
            else if (e.which == 90 && (e.ctrlKey || e.metaKey)) {    // CTRL + (shift) + Z
              if (e.shiftKey){
                me.redo();
              } else {
                me.undo();
              }
            }
        });

        grid.onClick.subscribe(function (e, args) {
            var selectNode = grid.getCellNode(args.row, args.cell);
            if (/comment_column_/.test($(selectNode).attr('class'))) {
                var view;
                view =_.find(me._childViews, function(v){return v instanceof me.popupCommentView;});
                if(!view) return true;
                view.show(args);
            } else {
                $('body').off('keydown.closeModal');
                $('body').off('click.closeCommentPopup');
                $('#commentTooltip').hide();
            }
            return true;
        });
//
//
//        //Фильтр столбцов
//        $(grid.getHeaderRow()).delegate(":input", "change keyup", function (e) {
//            var columnId = $(this).data("columnId");
//            if (columnId != null) {
//                columnFilters[columnId] = $.trim($(this).val());
//                dataView.refresh();
//            }
//        });
//
//        grid.onHeaderRowCellRendered.subscribe(function(e, args) {
//            $(args.node).empty();
//            $("<input type='text'>")
//                .data("columnId", args.column.id)
//                .val(columnFilters[args.column.id])
//                .appendTo(args.node);
//        });

        grid.onCellChange.subscribe(function (e, args) {
            controller.get('tableWorksheetInstance').changeTableRow(args);
        });

        var timer_active_cell_chanched = null;
        grid.onActiveCellChanged.subscribe(function (e, obj) {

            var grid = obj.grid,
                column = grid.getColumns()[obj.cell],
                columnName = column.name,
                cell =grid.getData().spreadsheet.getCell(columnName + (obj.row+1)),
                activeCellNode = $(grid.getActiveCellNode()),
                toInput = '';
            me.set('activeCell', obj);
            // change active class for column
            console.log('activeCellCahnge');
            if(activeCellNode.hasClass('comment_column__cell') || activeCellNode.hasClass('lock_cell')){
                $('#active_string_input').attr('disabled', 'disabled');
                console.log('disable');
            } else {
                console.log('enable');
                $('#active_string_input').removeAttr('disabled');
            }

            me.updateActiveStringVal(cell);
            //TODO: change event of toInput in interface
            if(timer_active_cell_chanched) clearTimeout(timer_active_cell_chanched);
            timer_active_cell_chanched = setTimeout(function(){
                controller.get('tableMasterInstance').changeCollaborativeActiveCell({cell: obj.cell, row: obj.row});
                timer_active_cell_chanched = null;
            }, 300);

            return true;
        });

        grid.init();
        me.set('grid', grid);
        grid.setActiveCell(0,1);

        arrayHiddenColumn = grid.getColumns();

        if (eventInit) {
            me.initInterfaceEvents();
        }

        me.renderActiveCollaboratorCell();

        // set cellCssStyles
        var cellCssStyles = controller.get('tableWorksheetInstance').getCellCssStyles();
        me.set('globalCellCssStyles', cellCssStyles);


        me.set('loading', false);
    },

    copySelectedRange: function(e, args, justOne){
        var me = this,
            grid = me.get('grid'),
            controller = me.get('controller'),
            activeCell = grid.getActiveCell();

        var execCommand = {
            activeRow: activeCell.row,
            activeCell: activeCell.cell,
            row: activeCell.row,
            cell: activeCell.cell,
            oldValues: [],


            execute: function() {
                var changedCells = [];
                if (justOne){
                    var to_range = {
                            fromCell: this.cell,
                            fromRow: this.row
                    };

                    //сохранить диапазон для истории
                    this.to_range = to_range;

                    var from_column_name = grid.getColumns()[to_range.fromCell].name,
                        from_cell_position = from_column_name + '' + (to_range.fromRow + 1);
                        //originCell = grid.getData().spreadsheet.getCell(from_cell_position);

                    for (var i = args.range.fromRow + 2; i <= (args.range.toRow + 1); i++) {
                        var to_column_name = grid.getColumns()[to_range.fromCell].name,
                            to_cell_position = to_column_name + '' + i;
                        if($(grid.getCellNode(i-1, to_range.fromCell)).hasClass('lock_cell')) continue;

                        //сохранить версию ячейки
                        var cellObj = {},
                            cellNow = grid.getData().spreadsheet.getCell(to_cell_position);
                        cellObj.formula = cellNow.isFormula()? cellNow.formula: cellNow.value;
                        cellObj.value = cellNow.getCalculatedValue();
                        cellObj.id = cellNow.getId();

                        this.oldValues[i] = cellObj;

                        grid.getData().copyCell(from_cell_position, to_cell_position);
                        changedCells.push(grid.getData().spreadsheet.getCell(to_cell_position));
                        grid.invalidateRow(i-1);
                    }
                }
                else {

                    if (args.from.length !== 1 || args.to.length !== 1) {
                        throw "This implementation only supports single range copy and paste operations";
                    }
                    // copy ranges
                    var ranges = args.from;
                    for (var i = 0; i < ranges.length; i++) {
                        var range = ranges[i],
                            to_range = args.to.length == args.from.length ? args.to[i] : {
                                fromCell: this.cell + range.fromCell - ranges[0].fromCell,
                                fromRow: this.row + range.fromRow - ranges[0].fromRow
                            };
                        //сохранить диапазон для истории
                        this.to_range = to_range;
                        this.count_rows = Math.abs(range.toRow - range.fromRow);
                        this.count_cells = Math.abs(range.toCell - range.fromCell);

                        for (var row_i = range.fromRow; row_i <= range.toRow; row_i++) {
                            this.oldValues[(to_range.fromRow + (row_i-range.fromRow))] = [];

                            for (var cell_i = range.fromCell; cell_i <= range.toCell; cell_i++) {
                                var from_column_name = grid.getColumns()[cell_i].name,
                                    from_cell_position = from_column_name + '' + (row_i + 1),
                                    to_column_name = grid.getColumns()[to_range.fromCell + (cell_i-range.fromCell)].name,
                                    to_cell_position = to_column_name + '' + (to_range.fromRow + 1 + (row_i-range.fromRow));
                                if($(grid.getCellNode(to_range.fromRow + (row_i-range.fromRow), to_range.fromCell + (cell_i-range.fromCell))).hasClass('lock_cell')) continue;

                                //сохранить старое значение в ячейке
                                var cellObj = {},
                                    cellNow = grid.getData().spreadsheet.getCell(to_cell_position);
                                cellObj.formula = cellNow.isFormula()? cellNow.formula: cellNow.value;
                                cellObj.value = cellNow.getCalculatedValue();
                                cellObj.id = cellNow.getId();

                                this.oldValues[(to_range.fromRow + (row_i-range.fromRow))][to_range.fromCell + (cell_i-range.fromCell)] = cellObj;

                                grid.getData().copyCell(from_cell_position, to_cell_position);
                                changedCells.push(grid.getData().spreadsheet.getCell(to_cell_position));
                                grid.invalidateRow(to_range.fromRow + (row_i-range.fromRow));
                            }
                        }
                    }
                }

                //синхронизировать с realtime doc
                grid.render();
                controller.get('tableWorksheetInstance').changeTableRows(changedCells);
            },


            undo: function() {
                var changedCells = [];
                if (justOne){
                    var to_range = this.to_range;
                    for (var i = args.range.fromRow + 2; i <= (args.range.toRow + 1); i++) {
                        if($(grid.getCellNode(i-1, to_range.fromCell)).hasClass('lock_cell')) continue;
                        //старая версия ячейки
                        var cellObj = this.oldValues[i];
                            cellNow = grid.getData().setCellData(cellObj.id, cellObj.formula);
                        changedCells.push(cellNow);
                        grid.invalidateRow(i-1);
                    }
                }
                else {

                    if (args.from.length !== 1 || args.to.length !== 1) {
                        throw "This implementation only supports single range copy and paste operations";
                    }
                    // copy ranges
                    var ranges = args.from;
                    for (var i = 0; i < ranges.length; i++) {

                        var to_range = this.to_range,
                            rows = to_range.fromRow + this.count_rows,
                            cells = to_range.fromCell + this.count_cells;

                        for (var row_i = to_range.fromRow; row_i <= rows; row_i++) {
                            for (var cell_i = to_range.fromCell; cell_i <= cells; cell_i++) {
                                if($(grid.getCellNode(to_range.fromRow + (row_i-to_range.fromRow), to_range.fromCell + (cell_i-to_range.fromCell))).hasClass('lock_cell')) continue;
                                //восстановить по старому значению
                                var cellObj = this.oldValues[row_i][cell_i],
                                    cellNow = grid.getData().setCellData(cellObj.id, cellObj.formula);

                                changedCells.push(cellNow);
                                grid.invalidateRow(to_range.fromRow + (row_i-to_range.fromRow));
                            }
                        }
                    }
                }

                //синхронизировать с realtime doc
                grid.render();
                controller.get('tableWorksheetInstance').changeTableRows(changedCells);
            }
        }

        me.queueAndExecuteCommand(execCommand);

    },

    updateActiveStringVal: function(cell){
      //обновляет знавчение текущей  строки функций
      var me = this,
          grid = me.get('grid'),
          toInput ='';
      if (cell.isFormula()) {
          toInput = typeof cell.formula != 'undefined'?cell.formula:'';
      }
      else if (typeof cell.value != 'undefined') {
          toInput = cell.value;
      }
      me.set('active_string_val', toInput);
    },

    getActiveCellObject: function(){
        /**
         * вернет объект активной ячейки
         * @type {*}
         */
        var me = this,
            grid = me.get('grid'),
            actCell = grid.getActiveCell(),
            column = grid.getColumns()[actCell.cell],
            columnName = column.name;
            return grid.getData().spreadsheet.getCell(columnName + (actCell.row+1));
    },


    renderActiveCollaboratorCell: function () {
        var me = this,
            grid = me.get('grid'),
            collaboratorUsers = me.get('controller').get('tableMasterInstance').getOutsideCollaborative();

        //grid.removeCellCssStyles("collaborative");
        for(var j=1; j<=5; j++) $('.slick-cell').removeClass('collaborative_'+j);
        //border: 2px solid #5AB7E3;
        for (var i = 0; i < collaboratorUsers.length; i++) {
            var u = collaboratorUsers[i];
            if (u.activeCell) {
                var obj = {},
                    cell = u.activeCell.cell;
                obj[u.activeCell.row] = {cell: "collaborative"};
                var cellEl = grid.getCellNode(u.activeCell.row, u.activeCell.cell);
                $(cellEl).addClass('collaborative_' + (i+1));
            }
        }
    },

    extendObject: function (item, itemView) {
        for (var key in item) {
            if (item.hasOwnProperty(key)) {
                itemView[key] = item[key];
            }
        }
        return itemView;
    },

    updateGridItems: function(changingList){
        /**@param - changingList - список измененных ячеек
         *
         */
        var me = this,
            grid = me.get('grid'),
            changeObject = {},
            invalidRows = [];
        for (var i =0; i<changingList.length; i++ ){
            var item = changingList[i];
            changeObject[item.position] = {
                formula: item.formula,
                value: item.value
            }
            var pos = Spreadsheet.parsePosition(item.position);
            if(!_.contains(invalidRows, (pos.row-1))){
                invalidRows.push((pos.row-1));
            }
        }
        me.get('spreadsheetSlick').setData(changeObject);
        grid.invalidateRows(invalidRows);
        grid.render();
    },

    updateGridItem: function (cellChanged, needSinc) {
        /**
         * @param needSinc - если передан и true, нужно сохранить в Drive синхронизировать
         * @type {*}
         */
        var me = this,
            grid = me.get('grid'),
            gridData = grid.getData();

            var cell = gridData.spreadsheet.getCell(cellChanged.position);
            if (cell.value == cellChanged.value || (cell.isFormula() && cell.isCalculated()
                 && cell.formula == cellChanged.value)) {
                //TODO: check - избавиться от пересчета при активации ячейки и в результате чего измененяется active_str
                return;
            }

            var force = false;
            if (needSinc){
                force = true;
            }
            cell.setValue(cellChanged.value, force);
            grid.updateCell(cellChanged.row, cellChanged.cell);


        if (needSinc){
            me.updateActiveStringVal(cell);
            me.get('controller').get('tableWorksheetInstance').changeTableRow({
                grid: grid,
                row: cellChanged.row,
                cell: cellChanged.cell
            });
        }
    },

    cloneDataObject: function (data) {
        return _.cloneDeep(data);
    },

     validateKeyPressInput: function(e){
         var code = e.which;
         var char = String.fromCharCode(code);
         var t = /[\W\w]+/.test(char);
         if (t)
             return 'insert';
         if (code==113) { //f2
             return 'open';
         }
         if (code==46){
             return "delete";
         }

         //
         // проверяем, а не нажата ли функциональная клавиша
         //
         if(code == 8 || code == 37
            || code == 39 || code == 9 || code == 46)
           return true;
         return false;
        },

        keypress: function(e, obj){
            var me = this,
                grid = me.get('grid');
            //печатные символы или F2
            var validCode = me.validateKeyPressInput(e),
                activeCell = grid.getActiveCell();
            if (validCode) {
                //достать ячейку
                var cellName = grid.getColumns()[activeCell.cell].name + (activeCell.row + 1),
                    cell = grid.getData().spreadsheet.getCell(cellName);
                if (validCode=='insert'){
                    if(!grid.getEditorLock().isActive()){
                        grid.editActiveCell();
                    }
                    //var symb = String.fromCharCode(e.which);
                    //$('input.editor-formula').val(symb);
                }
                else if (validCode == 'open'){
                    if(!grid.getEditorLock().isActive()){
                        grid.editActiveCell();
                    }
                }
                else if( validCode == 'delete'){
                    var columnName = grid.getColumns()[activeCell.cell].name;
                    cell.position = columnName + (activeCell.row + 1);
                    cell.value = me.get('');
                    me.updateGridItem(cell, true);
                }
            }
        },

    initInterfaceEvents: function () {
        var me = this;
        me.$(".select").chosen();
        conditionalFormattingArray = [],
            grid = me.get('grid'),
            dataView = me.get('dataView'),
            tempSelectedCell = [];

        $('#tableBody').bind('keypress', function(e){
            me.keypress(e);
        });

        (function () {

            var allTableArray = [];
            var arrayHideShowColumn = [];
            var arrayHiddenColumn = [];
            var cellCopy = '';
            var cellCut = '';
            var colorCellArray = {};
            //var commentsArray = {};
            var noteArray = [];
            var columnFormatter = '';

            function arrayToArray(from) {
                var to = [];
                for (var i = 0; i < from.length; i++) {
                    to.push(from[i]);
                }
                return to
            }

            $('#showFilter').on('click', function () {
                $('#tableBody').toggleClass('show_filter');
                if ($('#tableBody').hasClass('show_filter')) {
                    $('.slick-viewport').height($('.slick-viewport').height() - 53)
                } else {
                    $('.slick-viewport').height($('.slick-viewport').height() + 53)
                }
            });

            /********************************
             locked region popup window
             *********************************/

            $('body').on('click', '.slick-header-column:eq(0)', function () {
                $('#lockedRegionPopup').show();
                $('.overlay').show();
            });

            /********************************
             history popup window
             *********************************/

            $('#showHistory').on('click', function () {
                $('#historyPopup').show();
                $('.overlay').show();

                return false;
            });

            $('#histiryButton').on('click', function () {
                $('#historyPopup').hide();
                $('.overlay').hide();

                return false;
            });


            /********************************
             close popup window
             *********************************/
            $('.authForm__btn_cancel, .popup__close, .overlay').on('click', function () {
                $('.popup').hide();
                $('.overlay').hide();
                //fix issue #10
                $('.sp-container:not(.sp-hidden)').addClass('sp-hidden');
                $('body').off('keydown.closeModal');
                return false;
            });

            $('#addUserPopup .authForm__btn_cancel,#addUserPopup .popup__close').unbind('click').on('click', function () {
                $('#addUserPopup').hide();
                $('.overlay_access').hide();
                //fix issue #10
                $('.sp-container:not(.sp-hidden)').addClass('sp-hidden');

                $('body').off('keydown.closeModal');
                return false;
            });

        }).call(this);

    }
});