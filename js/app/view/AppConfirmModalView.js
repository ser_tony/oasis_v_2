App.AppConfirmModalView = Em.View.extend({
    templateName: 'app/confirm_modal',
    tagName: 'div',
    classNames: ['popup'],

    // required
    title: '',
    message: '',
    // optional
    fnOk: null,
    fnCancel: null,
    fnContext: null,

    show: function(){
        var me = this;
        me.$().show();
        $('.overlay').show();
        $('body').on('keydown.closeConfirm', function(ev){
            if(ev.keyCode == $.ui.keyCode.ESCAPE){
                me._executeCancel();
            } else if(ev.keyCode == $.ui.keyCode.ENTER){
                me._executeOk();
            }
        });
    },

    actions: {
        'ok': function(){
            this._executeOk();
        },
        'cancel': function(){
            this._executeCancel();
        }
    },

    _hide: function(){
        this.$().hide();
        $('.overlay').hide();
        this.remove();
    },

    _executeOk: function(){
        var me = this;
        $('body').off('keydown.closeConfirm');
        me.get('fnOk') && me.get('fnOk').call(me.get('fnContext')||me);
        me._hide();
    },

    _executeCancel: function(){
        var me = this;
        $('body').off('keydown.closeConfirm');
        me.get('fnCancel') && me.get('fnCancel').call(me.get('fnContext')||me);
        me._hide();
    }

});