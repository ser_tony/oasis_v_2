App.AppTableSidebarView = Em.View.extend({
    templateName: 'app/table/sidebar',

    grid: null,
    activeCell: null,
    cellCssStyles: Em.A(),
    cellCssStyleKey: 'cell_css_style_key',
    buffer: {
        type: 'copy'
    },
    is_trigger_change_css_off: false,
    is_bg_color: false,
    is_text_color: false,
    is_trigger_observers: true,
    formatting_by_stamp_active: false,
    selected_range: false,
    notes: false,
    timer_for_note_box: null,
    columns: [],
    timer_init_controlls: null,

    // children views
    modalNumberFormattingView: App.AppNumberFormattingModalView,
    modalHiddenColumnsView: App.AppHiddenColumnModalView,
    modalValidationDataModalView: App.AppValidationDataModalView,
    modalConditionalFormattingView: App.AppConditionalFormattingModalView,
    modalChangeRowHeightModalView: App.AppChangeRowHeightModalView,

    set: function(key, val){
        if(key == 'cellCssStyles'){
            var rows_changed = [],
                old_cell_css = this.get(key);
            _.each(_.keys(val), function(row){
                if(!_.isEqual(old_cell_css[row], val[row])){
                    rows_changed.push(parseInt(row, 10));
                }
            });
            this.setCellCssStyles(rows_changed);
        }
        this._super(key, val);
    },

    get: function(key){
        var result = this._super(key);
        if(key == 'cellCssStyles'){
            return _.cloneDeep(result);
        }
        return result;
    },

    processStyle: function (selected, styleClassCellArray, newClassName, removeClassName) {
        var me = this,
            grid = me.get('grid'),
            columnId = grid.getColumns()[parseInt(selected.cell, 10)].id;
        styleClassCellArray[selected.row] = styleClassCellArray[selected.row] ? styleClassCellArray[selected.row] : {};
        var csses = styleClassCellArray[selected.row][columnId];
        if (csses) {
            var rem_classes = removeClassName.split(' ');
            for (var i = 0; i < rem_classes.length; i++) {
                csses = csses.replace(new RegExp('\\b' + rem_classes[i] + '\\b', 'g'), '');
            }
            if (newClassName) {
                csses = csses.replace(new RegExp('\\b' + newClassName + '\\b', 'g'), '').replace(/\s+/g, ' ').trim();
                csses += ' ' + newClassName;
            }
        } else {
            csses = newClassName;
        }
        styleClassCellArray[selected.row][columnId] = csses;
    },

    addNewStyleClass: function (newClassName, removeClassName, styleClassCellArray) {
        removeClassName = removeClassName.trim().replace(/\s+/g, ' ');
        var me = this,
            grid = this.get('grid'),
            trigger = false;
        if (!styleClassCellArray) {
            trigger = true;
            styleClassCellArray = me.get('cellCssStyles');
        }

        me.applyForSelectedCells(me.processStyle, [styleClassCellArray, newClassName, removeClassName], me, false);
        //$('.slick-cell.selected').removeClass(removeClassName);
        if (trigger) {
            me.set('cellCssStyles', styleClassCellArray);
            me.get('controller').get('tableWorksheetInstance').changeCellCssStyles(styleClassCellArray);
        }
    },

    switch_leftside_bar: function(){
        var me = this;
        $('#tableLeftColumn, .layoutWithColumn__wrapper').toggleClass('active');
        if (me.$('#tableLeftColumn').hasClass('active')) {
            $('.slick-viewport').width($('.slick-viewport').width() + 255);
        } else {
            $('.slick-viewport').width($('.slick-viewport').width() - 255);
        }
    },

    actions: {
        'change_tab': function (addClass) {
            var me = this,
                sidebar = me.$('#tableLeftColumn'),
                active_el = event.target.tagName.toUpperCase() == 'A' && $(event.target) || $(event.target).parents('a');

            if(active_el.parents('.layoutWithColumn__leftColumn').hasClass('active')) {
                me.switch_leftside_bar();
            };
            me.$('#tableTabs a.active').removeClass('active');
            active_el.addClass('active');
            if (addClass) {
                sidebar.addClass('view_actions');
            } else {
                sidebar.removeClass('view_actions');
            }
            if (active_el.hasClass('tableTabs__actions active')) {
                me.$('.scroller_actions').baron({
                    bar: '.scroller_active__track',
                    barOnCls: 'possibleScroll',
                    direction: 'v'
                });
            }
        },

        'switch_sidebar': function () {
            var me = this;
            me.switch_leftside_bar();
        },

        // cut, copy, past
        'copy_cut': function (is_cut) {
            var me = this,
                grid = me.get('grid'),
                buffer = me.get('buffer') || me.set('buffer', {}) || {},
                ranges = grid.getSelectionModel().getSelectedRanges();
            buffer.type = is_cut ? 'cut' : 'copy';
            buffer.cell_from = [];
            for (var i = 0; i < ranges.length; i++) {
                var range = ranges[i];
                var j = 0;
                for (var row_i = range.fromRow; row_i <= range.toRow; row_i++) {
                    buffer.cell_from.push([]);
                    for (var cell_i = range.fromCell; cell_i <= range.toCell; cell_i++) {
                        var column_name = grid.getColumns()[cell_i].id,
                            cell_position = column_name + '' + (row_i + 1);
                        buffer.cell_from[j].push([cell_position, grid.getData().spreadsheet.getCell(cell_position).formula]);
                    }
                    j++;
                }
            }
        },

        'paste': function () {
            var me = this,
                controller = this.get('controller'),
                grid = me.get('grid'),
                data = grid.getData(),
                buffer = me.get('buffer'),
                activeCell = grid.getActiveCell(),
                changedCells = [];
            if (!buffer || !buffer.cell_from) return;
            var range = buffer.cell_from;
            for (var row_i = 0; row_i < range.length; row_i++) {
                for (var cell_i = 0; cell_i < range[row_i].length; cell_i++) {
                    var from_cell = range[row_i][cell_i][0],
                        formula = range[row_i][cell_i][1],
                        to_cell = grid.getColumns()[activeCell.cell + cell_i].id + '' + (activeCell.row + 1 + row_i);
                    if (buffer.type == 'copy') {
                        data.copyCell(from_cell, to_cell);
                    } else {
                        var cut_from_cell = data.spreadsheet.getCell(from_cell);
                        data.setCellData(to_cell, formula);
                        for (var ii = 0; ii < range.length; ii++) {
                            for (var jj = 0; jj < range[ii].length; jj++) {
                                if (range[ii][jj][0] == to_cell) {
                                    range[ii][jj].push(false);
                                }
                            }
                        }
                        if (range[row_i][cell_i].length == 2) {
                            data.setCellData(from_cell, '');
                            changedCells.push(data.spreadsheet.getCell(from_cell));
                        }
                    }
                    grid.updateCell(activeCell.row + row_i, activeCell.cell + cell_i);
                    changedCells.push(data.spreadsheet.getCell(to_cell));
                }
            }
            // sync change data
            controller.get('tableWorksheetInstance').changeTableRows(changedCells);
        },

        'change_font_family': function () {
            var val = this.$('#setFontFamily option:selected').val(),
                addStyle = !val ? 'font_family__Segoe_UI' : val;
            if(this.get('is_trigger_change_css_off')){
                this.set('is_trigger_change_css_off', false);
                return;
            }
            this.addNewStyleClass(addStyle, this.get('options').fontFamilies);
        },
        'change_font_size': function () {
            this.addNewStyleClass(this.$('#setFontSize option:selected').val(), this.get('options').fontSizes);
        },
        'set_font_style': function (cls) {
            var me = this,
                el = event.target.tagName.toUpperCase() == 'A' ? $(event.target) : $(event.target).parents('a');
            if (el.hasClass('active')) {
                me.addNewStyleClass('', cls);
            } else {
                me.addNewStyleClass(cls, '');
            }
            el.toggleClass('active');
        },
        'set_align_style': function (cls) {
            var me = this,
                el = event.target.tagName.toUpperCase() == 'A' ? $(event.target) : $(event.target).parents('a');
            me.$('#setAlignStyle a').removeClass('active');
            me.addNewStyleClass(cls, this.get('options').aligns);
            el.addClass('active');
        },
        'change_bg_color': function () {
            this.set('is_bg_color', true);
            this.set_bg_color();
        },
        'label_change_bg_color': function () {
            this.set('is_trigger_observers', true);
            this.set('is_bg_color', !this.get('is_bg_color'));
        },
        'change_text_color': function () {
            this.set('is_text_color', true);
            this.set_text_color();
        },
        'label_change_text_color': function () {
            this.set('is_trigger_observers', true);
            this.set('is_text_color', !this.get('is_text_color'));
        },
        'set_bg_color_wrapper': function (bg_color) {
            this.addNewStyleClass(bg_color, this.options.colors);
            this.get_active_bg_color();
        },
        'set_border_cell': function (border_cls) {
            var me = this,
                grid = me.get('grid'),
                ranges = grid.getSelectionModel().getSelectedRanges(),
                styleClassCellArray = me.get('cellCssStyles');
            if (!border_cls || border_cls == 'borderFull') {
                me.addNewStyleClass(border_cls, me.options.borders, styleClassCellArray);
            } else {
                for (var r = 0; r < ranges.length; r++) {
                    var range = ranges[r];
                    // delete all border classes
                    me.addNewStyleClass('', me.options.borders, styleClassCellArray);
                    if (_.contains(['borderTop', 'borderBottom'], border_cls)) {
                        for (var cell_i = range.fromCell; cell_i <= range.toCell; cell_i++) {
                            me.processStyle({
                                    row: border_cls == 'borderTop' && range.fromRow || range.toRow,
                                    cell: cell_i
                                }, styleClassCellArray, border_cls, me.options.borders
                            );
                        }
                    } else if (_.contains(['borderLeft', 'borderRight'], border_cls)) {
                        for (var row_i = range.fromRow; row_i <= range.toRow; row_i++) {
                            me.processStyle({
                                    row: row_i,
                                    cell: border_cls == 'borderLeft' && range.fromCell || range.toCell
                                }, styleClassCellArray, border_cls, me.options.borders
                            );
                        }
                    } else if (border_cls == 'borderSides') {
                        // add border sides
                        if (range.fromRow == range.fromTo && range.fromCell == range.toCell) {
                            me.addNewStyleClass('borderFull', me.options.borders, styleClassCellArray);
                        } else {
                            for (var row_i = range.fromRow; row_i <= range.toRow; row_i++) {
                                for (var cell_i = range.fromCell; cell_i <= range.toCell; cell_i++) {
                                    if (cell_i == range.fromCell) {
                                        me.processStyle({
                                                row: row_i,
                                                cell: cell_i
                                            }, styleClassCellArray, 'borderLeft', ''
                                        );
                                    }
                                    if (cell_i == range.toCell) {
                                        me.processStyle({
                                                row: row_i,
                                                cell: cell_i
                                            }, styleClassCellArray, 'borderRight', ''
                                        );
                                    }
                                    if (row_i == range.fromRow) {
                                        me.processStyle({
                                                row: row_i,
                                                cell: cell_i
                                            }, styleClassCellArray, 'borderTop', ''
                                        );
                                    }
                                    if (row_i == range.toRow) {
                                        me.processStyle({
                                                row: row_i,
                                                cell: cell_i
                                            }, styleClassCellArray, 'borderBottom', ''
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //apply css styles
            me.set('cellCssStyles', styleClassCellArray);
            me.get('controller').get('tableWorksheetInstance').changeCellCssStyles(styleClassCellArray);
        },
        'formatting_by_stamp': function () {
            var me = this,
                grid = me.get('grid'),
                styleClassCellArray = me.get('cellCssStyles'),
                ranges = grid.getSelectionModel().getSelectedRanges(),
                styles = [],
                range = ranges[0],
                i = 0, j = 0, column_id,
                a = event.target.tagName == 'A' && $(event.target) || $(event.target).parents('a');
            if (_.isEqual(me.get('selected_range'), range)) {
                $('#tableBody').removeClass('formatting_by_stamp');
                a.find('.actionLinkText').removeClass('active');
                me.set('formatting_by_stamp_active', false);
                return;
            } else {
                $('#tableBody').addClass('formatting_by_stamp');
                a.find('.actionLinkText').addClass('active');
            }
            if (!me.get('formatting_by_stamp_active')) {
                for (var row_i = range.fromRow; row_i <= range.toRow; row_i++) {
                    var row_styles = [];
                    for (var cell_i = range.fromCell; cell_i <= range.toCell; cell_i++) {
                        column_id = grid.getColumns()[cell_i].id;
                        if (!styleClassCellArray[row_i] || !styleClassCellArray[row_i][column_id]) {
                            row_styles[j] = null;
                        } else {
                            row_styles[j] = styleClassCellArray[row_i][column_id];
                        }
                        j++;
                    }
                    styles.push(row_styles);
                    i++;
                    j = 0;
                }
                me.set('formatting_by_stamp_active', styles);
            } else {
                styles = me.get('formatting_by_stamp_active');
                for (i = 0; i < styles.length; i++) {
                    row_styles = styles[i];
                    for (j = 0; j < row_styles.length; j++) {
                        column_id = grid.getColumns()[range.fromCell + j].id;
                        if (row_styles[j]) {
                            if (!styleClassCellArray[range.fromRow + i]) styleClassCellArray[range.fromRow + i] = {};
                            styleClassCellArray[range.fromRow + i][column_id] = row_styles[j];
                        }
                    }
                }
                //apply css styles
                me.set('cellCssStyles', styleClassCellArray);
                me.get('controller').get('tableWorksheetInstance').changeCellCssStyles(styleClassCellArray);
                me.set('formatting_by_stamp_active', false);
                $('#tableBody').removeClass('formatting_by_stamp');
                a.find('.actionLinkText').removeClass('active');
            }
        },

        'change_lock_cells': function (state) {
            var me = this;
            if(me.get('is_trigger_change_css_off')){
                me.set('is_trigger_change_css_off', false);
                return;
            }
            me.addNewStyleClass(state && 'lock_cell' || '', state && '' || 'lock_cell');
        },

        'add_notes': function () {
            var me = this,
                grid = me.get('grid'),
                activeCell = grid.getActiveCell(),
                activeCellPos = grid.getActiveCellPosition(),
                doc_width = $(document).width(),
                column_id = grid.getColumns()[activeCell.cell].id,
                pos = column_id + '' + (activeCell.row + 1),
                cell = grid.getData().spreadsheet.getCell(pos),
                notes = cell.notes,
                cellNode = grid.getActiveCellNode(),
                textarea_div = $('#js_cell_note_textarea');
            textarea_div.data('active-cell', activeCell);
            if (notes) {
                notes = '\n\n' + notes;
            }
            me.set('notes', notes);
            textarea_div.show().position({
                my: doc_width - activeCellPos.right > 200 ? 'left top' : 'right top',
                at: doc_width - activeCellPos.right > 200 ? 'right top' : 'left top',
                of: cellNode
            });

            var input = textarea_div.find('textarea').val(notes && notes || '').focus().get(0);
            input.selectionStart = 0;
            input.selectionEnd = 0;
        },

        'remove_notes': function () {
            var me = this,
                grid = me.get('grid'),
                styleClassCellArray = me.get('cellCssStyles'),
                ranges = grid.getSelectionModel().getSelectedRanges(),
                changedCells = [];
            for (var r = 0; r < ranges.length; r++) {
                for (var row_i = ranges[r].fromRow; row_i <= ranges[r].toRow; row_i++) {
                    for (var cell_i = ranges[r].fromCell; cell_i <= ranges[r].toCell; cell_i++) {
                        var column_id = grid.getColumns()[cell_i].id,
                            cell = grid.getData().spreadsheet.getCell(column_id + '' + (row_i + 1));
                        if (!cell.notes) continue;
                        cell.notes = false;
                        changedCells.push(cell);
                        me.processStyle({row: row_i, cell: cell_i}, styleClassCellArray, '', 'cell_note');
                    }
                }
            }
            if (!changedCells.length) return;
            me.get('controller').get('tableWorksheetInstance').changeTableRows(changedCells, true);
            me.set('cellCssStyles', styleClassCellArray);
            me.get('controller').get('tableWorksheetInstance').changeCellCssStyles(styleClassCellArray);
        },

        'cell_note_focus': function () {
            clearTimeout(this.get('timer_for_note_box'));
        },
        'cell_note_focus_live': function () {
            if ($('#js_cell_note_textarea textarea').is(':focus')) return;
            this.hideNoteBox();
        },
        'cell_note_blur': function () {
            var me = this,
                grid = me.get('grid'),
                textarea_div = $('#js_cell_note_textarea'),
                activeCell = textarea_div.data('active-cell'),
                columnt_id = grid.getColumns()[activeCell.cell].id,
                pos = columnt_id + '' + (activeCell.row + 1),
                cell = grid.getData().spreadsheet.getCell(pos),
                styleClassCellArray = me.get('cellCssStyles'),
                val = textarea_div.find('textarea').val();
            if (val != me.get('notes')) {
                if (!val) val = false;
                cell.notes = val;
                me.get('controller').get('tableWorksheetInstance').changeTableRow({
                    cell: activeCell.cell,
                    row: activeCell.row,
                    grid: grid
                }, true);
                // change css styles
                if (val) {
                    me.processStyle(activeCell, styleClassCellArray, 'cell_note', '');
                } else {
                    me.processStyle(activeCell, styleClassCellArray, '', 'cell_note');
                }
                me.set('cellCssStyles', styleClassCellArray);
                me.get('controller').get('tableWorksheetInstance').changeCellCssStyles(styleClassCellArray);
            }
            me.set('notes', false);
            textarea_div.hide();
        },

        'add_table_row': function () {
            var me = this,
                grid = me.get('grid'),
                data = grid.getData(),
                activeCell = grid.getActiveCell();
            data.addItem(activeCell.row);
            me.get('controller').get('tableWorksheetInstance').addRow(activeCell.row);
            grid.invalidateAllRows();
            grid.render();
        },
        'remove_table_row': function () {
            var me = this,
                grid = me.get('grid'),
                data = grid.getData(),
                ranges = grid.getSelectionModel().getSelectedRanges(),
                from_index, count,
                OkFunction = function(){
                    for (var r = 0; r < ranges.length; r++) {
                        from_index = ranges[r].fromRow;
                        count = 0;
                        for (var row_i = ranges[r].toRow; row_i >= ranges[r].fromRow; row_i--) {
                            var cells = data.removeItem(row_i);
                            count++;
                        }
                        if (!count) continue;
                        me.get('controller').get('tableWorksheetInstance').removeRows(from_index, count);
                    }
                    grid.invalidateAllRows();
                    grid.render();
                };
            var confirmModal= me.createChildView(App.AppConfirmModalView, {
                title: 'Удаление строк',
                message: 'Вы действиетельно хотите удалить выделенные строки?',
                fnOk: OkFunction,
                fnContext: me
            });
            confirmModal.appendTo('body');
            Em.run.later(function(){
                confirmModal.show();
            }, 200);
        },

        'add_column': function (is_comment) {
            var me = this,
                grid = me.get('grid'),
                activeCell = grid.getActiveCell(),
                columns = grid.getColumns(),
                column_name = grid.getData().spreadsheet.getNextColumnName(grid),
                column_field = grid.getData().spreadsheet.getFieldByColumnId(column_name),
                arrayHiddenColumn = me.get('arrayHiddenColumn'),
                index;

            var newColumn = {
                'defaultSortAsc': true,
                'field': column_field,
                'focusable': true,
                'headerCssClass': null,
                'id': column_name,
                'minWidth': 30,
                'name': column_name,
                'rerenderOnResize': false,
                'resizable': true,
                'selectable': true,
                'sortable': false,
                'width': 100
            };
            if(is_comment){
                newColumn = $.extend(newColumn, {
                    'cssClass': 'comment_column__cell',
                    'headerCssClass': 'comment_column',
                    'width': 300
                });
            } else {
                newColumn = $.extend(newColumn, {
                    'editor': FormulaEditor,
                        'asyncPostRender': AsyncRenderer,
                        'formatter': ColumnFormatterUniversal
                });
            }
            if (activeCell) {
                index = activeCell.cell+1;
                columns.insertAt(index, newColumn);
            } else {
                //TODO: проверить правильный ли индекс тут
                index = columns.length;
                columns.push(newColumn);
            }
            grid.setColumns(columns);
            me.get('controller').get('tableWorksheetInstance').setColumn(index, newColumn);
            me.setColumns();
        },

        'remove_columns': function(){
            var me = this,
                grid = me.get('grid'),
                columns = grid.getColumns(),
                ranges = grid.getSelectionModel().getSelectedRanges(),
                from_index, count,
                OkFunction = function(){
                    for (var r = 0; r < ranges.length; r++) {
                        from_index = ranges[r].fromCell;
                        count = 0;
                        var column_names = [];
                        for (var cell_i = ranges[r].toCell; cell_i >= ranges[r].fromCell; cell_i--) {
                            column_names.push(columns[cell_i].id);
                            columns.removeAt(cell_i);
                            count++;
                        }
                        if (!count) continue;
                        var cells = grid.getData().spreadsheet.cells.byPosition;
                        var keys_cells = _.keys(cells);
                        keys_cells = _.filter(keys_cells, function(key){
                            var matches = key.match(/[A-Z]+/);
                            if(!matches) return false;
                            matches = matches[0];
                            return _.contains(column_names, matches);
                        });
                        var changedCells = [];
                        _.each(keys_cells, function(cell_id){
                            var cell = grid.getData().spreadsheet.getCell(cell_id);
                            cell.setValue('');
                            changedCells.push(cell);
                        });
                        grid.setColumns(columns);
                        me.get('controller').get('tableWorksheetInstance').changeTableRows(changedCells);
                        me.get('controller').get('tableWorksheetInstance').removeColumns(from_index, count);
                        me.setColumns();
                    }
                };
            var confirmModal= me.createChildView(App.AppConfirmModalView, {
                title: 'Удаление столюцов',
                message: 'Вы действиетельно хотите удалить выделенные столбцы?',
                fnOk: OkFunction,
                fnContext: me
            });
            confirmModal.appendTo('body');
            Em.run.later(function(){
                confirmModal.show();
            }, 200);

        },

        'open_modal_change_row_height': function(){
            var me = this, view;
            view =_.find(me._childViews, function(v){return v instanceof me.modalChangeRowHeightModalView;});
            if(!view) return;
            view.open_modal();
        },

        'open_modal_hidden_column': function(){
            var me = this, view;
            view =_.find(me._childViews, function(v){return v instanceof me.modalHiddenColumnsView;});
            if(!view) return;
            view.open_modal();
        },
        'open_modal_validation': function(){
            var me = this, view;
            view =_.find(me._childViews, function(v){return v instanceof me.modalValidationDataModalView;});
            if(!view) return;
            view.open_modal();
        },
        'open_modal_formatter': function(){
            var me = this, view;
            view =_.find(me._childViews, function(v){return v instanceof me.modalNumberFormattingView;});
            if(!view) return;
            view.open_modal();
        },
        'open_modal_conditional': function(){
            var me = this, view;
            view =_.find(me._childViews, function(v){return v instanceof me.modalConditionalFormattingView;});
            if(!view) return;
            view.open_modal();
        },
        'setAccess': function(){
            var popup = $('#accessPopup');
            popup.show();
            $('.overlay').show();
            $('body').on('keydown.closeConfirm', function(ev){
                if(ev.keyCode == $.ui.keyCode.ESCAPE){
                    $('body').off('keydown.closeModal');
                    popup.hide();
                    $('.overlay').hide();
                }
            });
            var inp = $('#linkTableInput'),
                inpVal = inp.val();
            inp[0].setSelectionRange(0, inpVal.length);
            popup.find('.access_table_scroll').baron({
                bar: '.scroller_table__track',
                barOnCls: 'possibleScroll',
                direction: 'v'
            });
            return false;
        }
    },
    // end actions

    showNoteBox: function (obj, isEvent) {
        var me = this,
            grid = me.get('grid'),
            doc_width = $(document).width(),
            activeCell = isEvent && grid.getCellFromEvent(obj) || grid.getActiveCell(),
            activeCellNode = isEvent && grid.getCellNode(activeCell.row, activeCell.cell) || grid.getActiveCellNode(),
            activeCellPos = grid.getCellNodeBox(activeCell.row, activeCell.cell),
            column_id = grid.getColumns()[activeCell.cell].id,
            pos = column_id + '' + (activeCell.row + 1),
            cell = grid.getData().spreadsheet.getCell(pos),
            textarea_div = $('#js_cell_note_textarea');
        if (textarea_div.is(':visible') && _.isEqual(textarea_div.data('activeCell'), activeCell)) {
            return;
        }
        me.set('notes', cell.notes);
        textarea_div.data('active-cell', activeCell);
        textarea_div.find('textarea').val(cell.notes || '');
        textarea_div.show().position({
            my: doc_width - activeCellPos.right > 200 ? 'left top' : 'right top',
            at: doc_width - activeCellPos.right > 200 ? 'right top' : 'left top',
            of: activeCellNode
        });
    },

    hideNoteBox: function () {
        var me = this;
        var timer = setTimeout(function () {
            var timer = me.get('timer_for_note_box');
            timer && clearTimeout(timer);
            me.set('timer_for_note_box', null);
            $('#js_cell_note_textarea').data('activeCell', null).hide();
        }, 300);
        me.set('timer_for_note_box', timer);
    },

    applyForSelectedCells: function (fn, args, context, trigger) {
        var me = this,
            grid = me.get('grid'),
            changedCells = [],
            ranges = grid.getSelectionModel().getSelectedRanges();
        if (!context) context = me;
        for (var r = 0; r < ranges.length; r++) {
            var range = ranges[r];
            for (var row_i = range.fromRow; row_i <= range.toRow; row_i++) {
                for (var cell_i = range.fromCell; cell_i <= range.toCell; cell_i++) {
                    var column_name = grid.getColumns()[cell_i].id,
                        cell_position = column_name + '' + (row_i + 1);
                    if (!_.isArray(args)) {
                        args = [args];
                    }
                    var arg = _.clone(args);
                    arg.unshift({
                        row: row_i,
                        cell: cell_i
                    });
                    fn.apply(context, arg);
                    changedCells.push(grid.getData().spreadsheet.getCell(cell_position));
                }
            }
        }
        if (trigger)  me.get('controller').get('tableWorksheetInstance').changeTableRows(changedCells);
    },

    set_bg_color: function () {
        var me = this,
            is_bg_color = me.get('is_bg_color'),
            selected_color = $("#fontColorPanel").spectrum('get').toRgbString(),
            color;
        color = me.options.bgColorsRGB.indexOf(selected_color)+1;
        if (is_bg_color) {
            me.addNewStyleClass('bgcolor' + color, me.options.colors);
        } else {
            me.addNewStyleClass('', me.options.colors);
        }
    },

    set_text_color: function () {
        var me = this,
            is_text_color = me.get('is_text_color'),
            selected_color = $("#textColorPanel").spectrum('get').toRgbString(),
            color;
        color = me.options.bgColorsRGB.indexOf(selected_color)+1;
        if (is_text_color) {
            me.addNewStyleClass('color' + color, me.options.fontColors);
        } else {
            me.addNewStyleClass('', me.options.fontColors);
        }
    },

    resetInputs: function () {
        var me = this;
        me._reset_font_family();
        me._reset_font_size();
        me._reset_text_align();
        //me._reset_bg_color();
        //me._reset_text_color();
        me.reset_colors_checkbox();
        me.$('.actionLink.active').removeClass('active');
    },
    _reset_font_family: function () {
        var me = this, el;
        me.set('is_trigger_change_css_off', true);
        // reset font famaly select element
        el = me.$('#setFontFamily');
        el.find('option:selected').removeAttr('selected');
        el.find('option[value=""]').attr('selected', 'selected');
        el.chosen().change();
        el.trigger("liszt:updated");
    },
    _reset_font_size: function () {
        var me = this, el;
        me.set('is_trigger_change_css_off', true);
        // reset font size select element
        el = me.$('#setFontSize');
        el.find('option:selected').removeAttr('selected');
        el.find('option[value="font_size14"]').attr('selected', 'selected');
        //el.chosen().change();
        el.trigger("liszt:updated");
    },
    _reset_text_align: function (is_all) {
        var me = this, el;
        me.set('is_trigger_change_css_off', true);
        // reset font size select element
        el = me.$('#setAlignStyle');
        el.find('.actionLink.active').removeClass('active');
        if (!is_all) el.find('.actionLink[data-id="text_right"]').addClass('active');
    },
    _reset_bg_color: function (color) {
        var me = this;
        me.set('is_trigger_change_css_off', true);
        if (color) {
            color = color.match(/[0-9]+/);
            color = color && me.get('options').bgColorsRGB[parseInt(color[0], 10) - 1] || 'black';
        }
        me.$('#fontColorPanel').spectrum('set', color || 'black');
        me.set('is_trigger_observers', false);
        me.set('is_bg_color', !!color);
    },
    _reset_text_color: function (color) {
        var me = this;
        me.set('is_trigger_change_css_off', true);
        if (color) {
            color = color.match(/[0-9]+/);
            color = color && me.get('options').bgColorsRGB[parseInt(color[0], 10) - 1] || 'black';
        }
        me.$('#textColorPanel').spectrum('set', color || 'black');
        me.set('is_trigger_observers', false);
        me.set('is_text_color', !!color);
    },

    options: {
        colors: 'bgcolor1 bgcolor2 bgcolor3 bgcolor4 bgcolor5 '
            + 'bgcolor6 bgcolor7 bgcolor8 bgcolor9 bgcolor10 '
            + 'bgcolor11 bgcolor12 bgcolor13 bgcolor14 bgcolor15 '
            + 'bgcolor16 bgcolor17 bgcolor18 bgcolor19 bgcolor20 '
            + 'bgcolor21 bgcolor22 bgcolor23 bgcolor24 bgcolor25 '
            + 'bgcolor26 bgcolor27 bgcolor28 bgcolor29 bgcolor30 '
            + 'bgcolor31 bgcolor32 bgcolor33 bgcolor34 bgcolor35 '
            + 'bgcolor36 bgcolor37 bgcolor38 bgcolor39 bgcolor40 '
            + 'bgcolor41 bgcolor42 bgcolor43 bgcolor44 bgcolor45 '
            + 'bgcolor46 bgcolor47 bgcolor48 bgcolor49 bgcolor50 '
            + 'bgcolor51 bgcolor52 bgcolor53 bgcolor54 bgcolor55 '
            + 'bgcolor56 bgcolor57 bgcolor58 bgcolor59 bgcolor60 '
            + 'bgcolor61 bgcolor62 bgcolor63 bgcolor64 bgcolor65 '
            + 'white_bg green_bg pink_bg brawn_bg yellow_bg blue_bg ',
        bgColorsRGB: [
            "rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
            "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)",
            "rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
            "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
            "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
            "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
            "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
            "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
            "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
            "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
            "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
            "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)",
            "rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
        fontColors: 'color1 color2 color3 color4 color5 '
            + 'color6 color7 color8 color9 color10 '
            + 'color11 color12 color13 color14 color15 '
            + 'color16 color17 color18 color19 color20 '
            + 'color21 color22 color23 color24 color25 '
            + 'color26 color27 color28 color29 color30 '
            + 'color31 color32 color33 color34 color35 '
            + 'color36 color37 color38 color39 color40 '
            + 'color41 color42 color43 color44 color45 '
            + 'color46 color47 color48 color49 color50 '
            + 'color51 color52 color53 color54 color55 '
            + 'color56 color57 color58 color59 color60 '
            + 'color61 color62 color63 color64 color65 '
            + 'color66',
        fontSizes: 'font_size8 font_size9 font_size10 font_size11 ' +
            'font_size12 font_size14 font_size16 font_size18 ' +
            'font_size20 font_size22 font_size24 font_size26 ' +
            'font_size28 font_size36 font_size48 font_size72',
        fontFamilies: 'font_family__Segoe_UI font_family__Arial ' +
            'font_family__ComicSansMS font_family__CourierNew ' +
            'font_family__Georgia font_family__Impact font_family__Tahoma ' +
            'font_family__TrebuchetMS font_family__TimesNewRoman font_family__Verdana',
        fontStyles: 'font_bold font_italic font_underline',
        aligns: 'text_left text_center text_right',
        borders: 'borderTop borderRight borderBottom borderLeft borderFull'
    },

    initControllsByActiveCell: function(){
        var me = this, el,
            activeCell = me.get('activeCell'),
            grid = me.get('grid'),
            columnId = grid.getColumns()[parseInt(activeCell.cell, 10)].id,
            styleClassCellArray = me.get('cellCssStyles');
        if (!styleClassCellArray[activeCell.row]) {
            me.resetInputs();
            return;
        }
        var active_style = styleClassCellArray[activeCell.row][columnId];
        if (!active_style) {
            me.resetInputs();
            return;
        }
        active_style = active_style.replace(/\s+/, ' ');
        var styles = active_style.split(' '),
            fontFamilies = me.get('options').fontFamilies.replace(/\s+/, ' ').split(' ');
        // check font family
        var is_font_family = _.intersection(fontFamilies, styles);
        if (is_font_family.length > 0) {
            me.set('is_trigger_change_css_off', true);
            el = me.$('#setFontFamily');
            el.find('option:selected').removeAttr('selected');
            el.find('option[value="' + is_font_family[0] + '"]').attr('selected', 'selected');
            el.chosen().change();
            el.trigger("liszt:updated");

        } else {
            me._reset_font_family();
        }
        // check and set font size
        var is_font_size = _.intersection(me.get('options').fontSizes.replace(/\s+/, ' ').split(' '), styles);
        if (is_font_size.length > 0) {
            me.set('is_trigger_change_css_off', true);
            el = me.$('#setFontSize');
            el.find('option:selected').removeAttr('selected');
            el.find('option[value="' + is_font_size[0] + '"]').attr('selected', 'selected');
            el.chosen().change();
            el.trigger("liszt:updated");

        } else {
            me._reset_font_size();
        }
        // check font style
        var is_font_style = _.intersection(me.get('options').fontStyles.replace(/\s+/, ' ').split(' '), styles);
        el = me.$('#setFontStyle');
        el.find('.actionLink.active').removeClass('active');
        _.each(is_font_style, function (font_style) {
            el.find('.actionLink[data-id="' + font_style + '"]').addClass('active');
        });
        // check text align
        var is_text_align = _.intersection(me.get('options').aligns.replace(/\s+/, ' ').split(' '), styles);
        el = me.$('#setAlignStyle');
        me._reset_text_align(true);
        if (is_text_align.length > 0) {
            el.find('.actionLink[data-id="' + is_text_align[0] + '"]').addClass('active');
        }
        me.reset_colors_checkbox();
        // check bg color
        //me.get_active_bg_color();
        // check text color
        //var is_text_align = _.intersection(me.get('options').fontColors.replace(/\s+/, ' ').split(' '), styles);
        //if (is_text_align) {
        //    me._reset_text_color(is_text_align[0]);
        //} else {
        //    me._reset_text_color();
        //}
    },

    // Observers
    activeCellObserve: function () {
        var me = this,
        timer_init_controll = me.get('timer_init_controll');
        if(timer_init_controll) clearTimeout(timer_init_controll);
        timer_init_controll = setTimeout(function(){
            me.initControllsByActiveCell();
            me.set('timer_init_controll', null);
        }, 400);
        me.set('timer_init_controll', timer_init_controll);
    }.observes('activeCell'),

    get_active_bg_color: function () {
        var me = this,
            activeCell = me.get('activeCell'),
            grid = me.get('grid'),
            columnId = grid.getColumns()[parseInt(activeCell.cell, 10)].id,
            styleClassCellArray = me.get('cellCssStyles');
        if (!styleClassCellArray[activeCell.row]) {
            me.resetInputs();
            return;
        }
        var active_style = styleClassCellArray[activeCell.row][columnId];
        if (!active_style) {
            me.resetInputs();
            return;
        }
        active_style = active_style.replace(/\s+/, ' ');
        var styles = active_style.split(' '),
            is_bg_align = _.intersection(me.get('options').colors.replace(/\s+/, ' ').split(' '), styles);
        if (is_bg_align) {
            me._reset_bg_color(is_bg_align[0]);
        } else {
            me._reset_bg_color();
        }
    },

    didInsertElement: function () {
        var me = this;
        me.$('#js-tableTabs__home .scroller').baron({
            bar: '.scroller__bar',
            barOnCls: 'possibleScroll',
            direction: 'v'
        });
        me.$("#fontColorPanel, #textColorPanel, .colorpicker").spectrum({
            showPaletteOnly: true,
            showPalette: true,
            showInput: true,
            chooseText: "Выбрать",
            cancelText: "Отмена",
            palette: [me.get('options').bgColorsRGB]
        });
        me.$("#fontColorPanel").spectrum('set', {r: 252, g: 229, b: 205, a: 1});
        me.$("#textColorPanel").spectrum('set', {r: 32, g: 18, b: 77, a: 1});
        $('#tableBody').on('mouseenter', '.slick-cell.cell_note',function (ev) {
            var timer = me.get('timer_for_note_box');
            timer && clearTimeout(timer);
            me.showNoteBox(ev, true);
        }).on('mouseleave', '.slick-cell.cell_note', function () {
            if ($('#js_cell_note_textarea textarea').is(':focus')) return;
            me.hideNoteBox();
        });
    },

    reset_colors_checkbox: function(){
        var me = this;
        me.set('is_trigger_observers', false);
        me.set('is_bg_color', false);
        me.set('is_trigger_observers', false);
        me.set('is_text_color', false);
    },

    isBgColorObserve: function () {
        if (!this.get('is_trigger_observers')) {
            this.set('is_trigger_observers', true);
            return;
        }
        this.set_bg_color();
    }.observes('is_bg_color'),

    setColumns: function(){
        var me = this,
            columns = me.get('controller').get('tableWorksheetInstance').getColumns();
        _.map(columns, function(c){
            if(c.field == 'rowNum') return c;
            _.extend(c, {
                editor: FormulaEditor,
                formatter: ColumnFormatterUniversal,
                asyncPostRender: AsyncRenderer
            });
            return c;
        });
        me.set('columns', columns);
    },

    gridObserve: function(){
        this.setColumns();
    }.observes('grid'),

    isTextColorObserve: function () {
        if (!this.get('is_trigger_observers')) {
            this.set('is_trigger_observers', true);
            return;
        }
        this.set_text_color();
    }.observes('is_text_color'),

    cellCssStylesObserve: function(){
        var me = this;
        me.setCellCssStyles();
    }.observes('cellCssStyles'),

    setCellCssStyles: function(rows){
        var me = this,
            grid = me.get('grid');
        grid.removeCellCssStyles(me.get('cellCssStyleKey'));
        grid.setCellCssStyles(me.get('cellCssStyleKey'), me.get('cellCssStyles'));
        grid.invalidateRows(rows);
        grid.render();
    },

    confirmModal: function(title, message, fnOk, fnCancel){

    }
});