App.AppHiddenColumnModalView = Em.View.extend({
    templateName: 'app/table/popups/sidebar/hidden_columns',
    grid: null,
    columns: [],
    columnsClone: [],

    open_modal: function(){
        var me = this;
        me.$('#hideShowColumnPopup').show();
        $('.overlay').show();
        me.$('.hideShow_scroller').trigger('sizeChange');
        $('body').on('keydown.closeModal', function(ev){
            if(ev.keyCode == $.ui.keyCode.ESCAPE){
                $('body').off('keydown.closeModal');
                me.$('.popup').hide();
                $('.overlay').hide();
            }
        });
    },

    actions: {
        'save': function(){
            var me = this,
                controller = me.get('parentView').get('parentView').get('controller'),
                grid = me.get('grid'),
                columns = me.get('columns'),
                first_column = columns[0],
                columns_clone = me.get('columnsClone'),
                changed_columns = [],
                hidden_columns_id = _.filter(columns_clone, function(c){ return !!c.hidden;}).mapBy('id'),
                new_columns = _.map(_.filter(columns, function(c){
                    return c.field == 'rowNum' || !hidden_columns_id.contains(c.id);
                }), function(c, index){
                    if(c.field == 'rowNum') return c;
                    //c.field = index - 1;
                    return c;
                });
            _.each(columns_clone, function(c){
                var column = columns.findBy('id', c.id);
                if(!column) return;
                if(!!column.hidden == !!c.hidden) return;
                changed_columns.push([c]);
            })
            grid.setColumns(new_columns);
            controller.get('tableWorksheetInstance').changeColumns(changed_columns);
            var re_clone_columns = _.cloneDeep(columns_clone);
            re_clone_columns.unshift(first_column);
            me.set('columns', re_clone_columns);
            $('body').off('keydown.closeModal');
            me.$('#hideShowColumnPopup').hide();
            $('.overlay').hide();
        },
        'cancel': function(){
            this.reset();
        },
        'toggleState': function(column){
            var el = $(event.target).hasClass('scroll_button__wrapper') && $(event.target) || $(event.target).parents('.scroll_button__wrapper');
            Em.set(column, 'hidden', !column.hidden);

            el.toggleClass('active');
        }
    },

    reset: function(){
        var me = this,
            columns = me.get('columns');
        columns = _.filter(columns, function(c){
            return c.field != 'rowNum';
        });
        me.set('columnsClone', _.cloneDeep(columns));
    },

    didInsertElement: function(){
        var me = this;
        me.$('.hideShow_scroller').baron({
            bar: '.scroller_hideShow__track',
            barOnCls: 'possibleScroll',
            direction: 'v'
        });
    },

    columnsObserve: function(){
        this.reset();
    }.observes('columns')
});