App.AppConditionalFormattingModalView = Em.View.extend({
    templateName: 'app/table/popups/sidebar/conditional_formatting',

    translateErrorFormatDataValidationCondition: 'Введите допустимое значение',

    grid: null,

    conditional_rules: [],

    form_error_msg: '',

    ruleView: App.AppConditionalRuleView,

    open_modal: function(){
        var me = this,
            is_found = false,
            grid = me.get('grid'),
            activeCell = grid.getActiveCell(),
            conditional_rules = grid.getColumns()[activeCell.cell].conditional_rules;
        if(conditional_rules){
            me.set('conditional_rules', _.cloneDeep(conditional_rules));
        } else {
            me.reset_form();
        }
        // show modal
        me.$('#conditionalFormattingModal').show();
        $('.overlay').show();
        $('body').on('keydown.closeModal', function(ev){
            if(ev.keyCode == $.ui.keyCode.ESCAPE){
                $('body').off('keydown.closeModal');
                me.$('.popup').hide();
                $('.overlay').hide();
            }
        });
    },

    actions: {
        'add_conditional_rule': function(){
            this.get('conditional_rules').pushObject({});
        },
        'apply_conditional_formatting': function(){
            var me = this,
                grid = me.get('grid'),
                columns = grid.getColumns(),
                validation,
                controller = me.get('parentView').get('parentView').get('controller'),
                changed_columns = [],
                ranges = grid.getSelectionModel().getSelectedRanges();
            me.validate_all_rules();
            me.set('form_error_msg', false);
            if(me.check_error_form()) {
                me.set('form_error_msg', me.get('translateErrorFormatDataValidationCondition'));
                return;
            }
            conditional_rules = _.map(me.get('conditional_rules'), function(rule){
                return {
                    current_conditional_type: rule.current_conditional_type,
                    bg_color: rule.bg_color,
                    font_color: rule.font_color,
                    value: rule.value
                }
            });
            _.each(ranges, function(range){
                for(var cell_i = range.fromCell; cell_i <= range.toCell; cell_i++){
                    columns[cell_i].conditional_rules = conditional_rules;
                    changed_columns.push([columns[cell_i]]);
                }
            });
            grid.setColumns(columns);
            controller.get('tableWorksheetInstance').changeColumns(changed_columns);
            // hde modal
            $('body').off('keydown.closeModal');
            me.$('#conditionalFormattingModal').hide();
            $('.overlay').hide();
        }
    },

    remove_rule: function(model){
        var me = this;
        me.set('conditional_rules', _.without(me.get('conditional_rules'), model));
    },

    reset_form: function(){
        var me = this;
        me.set('conditional_rules', [
            {}
        ]);
    },

    validate_all_rules: function(){
        var me = this,
            child_views = _.filter(me.viewHierarchyCollection().views, function(v){return v instanceof me.get('ruleView');});
        _.each(child_views, function(child_view){
            child_view.validate_format_data_validation(child_view.$('.input_condition_value'));
        });
    },

    check_error_form: function(){
        var me = this,
            conditional_rules = me.get('conditional_rules');
        return !!conditional_rules.findBy('is_form_error', true);
    }
});