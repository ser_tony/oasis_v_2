App.AppTableAddUserView = Ember.View.extend({
    //layoutName: 'app/table/index',
    templateName: 'app/table/popups/add_user',

    addShareUser: '',

    actions: {
        addUserButton: function() {
            var me =this;
            me.get('controller').get('model').users.pushObject({
                value: me.get('addShareUser'),
                role: 'writer',
                added: false
            });
            $('#addUserPopup').hide();
            $('.overlay_access').hide();
            $('body').off('keydown.closeModal');
            $('body').on('keydown.closeModal', function(ev){
                if(ev.keyCode == $.ui.keyCode.ESCAPE){
                    $('body').off('keydown.closeModal');
                    $('#accessPopup').hide();
                    $('.overlay').hide();
                }
            });
            Em.run.later(function(){
                $('.access_table_scroll').trigger('sizeChange');
            }, 200);
            return false;
        }
    }

});