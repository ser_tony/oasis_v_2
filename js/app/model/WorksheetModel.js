App.WorksheetModel = App.BaseModel.extend({
    test: false,
    //instance - link to initialized Realtime model  - creating after loading
    fields: ['title', 'drive_id', 'id', 'isLoaded', 'isCurrent', 'instance']

});