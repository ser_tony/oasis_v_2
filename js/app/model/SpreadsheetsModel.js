App.SpreadsheetsModel = App.BaseModel.extend({

    test: false,

    fields: ['owner', 'lastEdit', 'id', 'isDeleted', 'filename'],


    get: function(filter){
        var me = this;
        if (!me.test){
            return Ember.$.getJSON('/spreadsheets/list');
//            return new Ember.RSVP.Promise(function (resolve, reject) {
//                me._request('/spreadsheets/list', {}, 'GET').then(function (result) {
//
//                    if (result.status) {
//                        reject(result)
//                    } else {
//                        var data;
//                        result = JSON.parse(result);
//                        debugger;
//                        if (Array.isArray(result)) {
//                            data = []
//                            debugger;
//                            result.forEach(function (el) {
//                                data.push(el);
//                            });
//                        } else {
//                            data = result;
//                        }
//                        resolve(data);
//                    }
//                }, function (error_response) {
//                    reject(error_response);
//                });
//        });
//


            //this._request('/spreadsheets/list', {}, 'get');

            //return $.getJSON('/spreadsheets/list');
        }

        var list = this.testData();
        if (filter && filter=='delete') {
            list = list.filterBy('isDeleted', true);
        }
        return this.decorate(list);
    },

    decorate: function(list){
        var me = this;
        list.forEach(function(item){
            item.lastEdit_text = me.timeToReadable(item.lastEdit);
        });
        return list;
    },

    timeToReadable: function(time){
        return moment(time).format("DD.MM.YYYY в HH:mm")
    },

    testData: function(namespace) {
            var testSpreadsheets = [{
                id: 1,
                filename: "Максимальное количество пользователей",
                lastEdit: new Date('2013-09-01 12:10:11'),
                owner: "Константин Константинопольский",
                isDeleted: false,
            },{
                id: 2,
                filename: "Размер файлового хранилища",
                lastEdit: new Date('2013-09-02 18:13:12'),
                owner: "Константин Константинопольский",
                isDeleted: false
            },{
                id: 3,
                filename: "Цена подписки на пользователей",
                lastEdit: new Date('2013-08-01 22:10:11'),
                owner: "Артем",
                isDeleted: false
            },{
                id: 4,
                filename: "Цена подписки (Удалено)",
                lastEdit: new Date('2013-08-01 20:00:01'),
                owner: "Филлип Филлиппович",
                isDeleted: true
            },{
                id: 5,
                filename: "Магазин постеров",
                lastEdit: new Date('2013-09-03 12:10:11'),
                owner: "Артем",
                isDeleted: true
            },{
                id: 6,
                filename: "Гадкий Я",
                lastEdit: new Date('2013-09-02 18:13:12'),
                owner: "Грю Иванович",
                isDeleted: false
            },{
                id: 7,
                filename: "Тачки",
                lastEdit: new Date('2013-08-01 22:10:11'),
                owner: "Дисней",
                isDeleted: true
            },{
                id: 8,
                filename: "Апрельские тезисы",
                lastEdit: new Date('2013-04-21 09:00:01'),
                owner: "Владимир Ильич",
                isDeleted: false
            }];

            //this.store(namespace, testSpreadsheets);
            return testSpreadsheets;
        }

});