//if (location.hash == '')    location.hash = '/app';

// load language
$.ajax({method: 'GET', url: './js/app/i18n/' + Ember.util.getLanguage() + '.js', async: false }).done(function (response) {
    try{
        eval(response);
    } catch(e) {
        console.log('Error parse language file: ', './js/app/i18n/' + Ember.util.getLanguage() + '.js');
        console.exception(e);
    }
    Ember.util.initDB();

    window.App = Ember.Application.create({
        currentPath: '',
        log: true,
        LOG_TRANSITIONS: true,
        LOG_TRANSITIONS_INTERNAL: true,
        LOG_VIEW_LOOKUPS: true,
        LOG_ACTIVE_GENERATION: true
    });

    App.log = window.console.log;
    // for register components (it is need because Ember register automatically if only template in DOM before init App)
    App.registerComponent = function (name) {
        var fullName = 'component:' + name,
            templateFullName = 'template:components/' + name;
        App.__container__.injection(fullName, 'layout', templateFullName);
        var Component = App.__container__.lookupFactory(fullName);
        if (!Component) {
            App.__container__.register(fullName, Ember.Component);
            Component = App.__container__.lookupFactory(fullName);
        }
        Ember.Handlebars.helper(name, Component);
    }
    window.ColumnFormatterUniversal = function(row, cell, value, columnDef, dataContext){
        var result_css = {};
        function setColors(is, rule){
            var bgColorsRGB = [
                "rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)",
                "rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)",
                "rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                bg_index = rule.bg_color && bgColorsRGB.indexOf(rule.bg_color),
                font_index = rule.font_color && bgColorsRGB.indexOf(rule.font_color),
                color_bg_class = bg_index !== false && bg_index >= 0 && rule.current_conditional_type + '__bgcolor' + (bg_index+1) || false,
                color_font_class = font_index !== false && font_index >= 0 && rule.current_conditional_type + '__fontcolor' + (font_index+1) || false;
            if(is && color_bg_class){
                result_css.color_bg = color_bg_class;
            }
            if(is && color_font_class){
                result_css.color_font = color_font_class;
            }
        }
        if(columnDef.conditional_rules){
            _.each(columnDef.conditional_rules, function(rule){
                var cond = rule.current_conditional_type;
                switch(cond){
                    case 'empty':
                        setColors(!value && !value.trim(), rule);
                        break;
                    case 'text_contains':
                        setColors(value && _.contains(value, rule.value) || false, rule);
                        break;
                    case 'text_not_contains':
                        setColors(value && !_.contains(value, rule.value) || false, rule);
                        break;
                    case 'text_equal':
                        setColors(value && value.toLowerCase() == rule.value.toLowerCase() || false, rule);
                        break;
                    case 'date_equal':
                        setColors(value && value == rule.value || false, rule);
                        break;
                    case 'date_before':
                        setColors(value && moment(value, 'YYYY-MM-DD') < moment(rule.value, 'YYYY-MM-DD') || false, rule);
                        break;
                    case 'date_after':
                        setColors(value && moment(value, 'YYYY-MM-DD') > moment(rule.value, 'YYYY-MM-DD') || false, rule);
                        break;
                    case 'date_between':
                        setColors(value && moment(value, 'YYYY-MM-DD') >= moment(rule.value[0], 'YYYY-MM-DD') && moment(value, 'YYYY-MM-DD') <= moment(rule.value[1], 'YYYY-MM-DD') || false, rule);
                        break;
                    case 'date_not_between':
                        setColors(value && (moment(value, 'YYYY-MM-DD') < moment(rule.value[0], 'YYYY-MM-DD') || moment(value, 'YYYY-MM-DD') > moment(rule.value[1], 'YYYY-MM-DD')) || false, rule);
                        break;
                    case 'gt':
                        var val = parseFloat(value),
                            val2 = parseFloat(rule.value);
                        setColors(!_.isNaN(val) && !_.isNaN(val2) && val > val2 || false, rule);
                        break;
                    case 'lt':
                        var val = parseFloat(value),
                            val2 = parseFloat(rule.value);
                        setColors(!_.isNaN(val) && !_.isNaN(val2) && val < val2 || false, rule);
                        break;
                    case 'equal':
                        var val = parseFloat(value),
                            val2 = parseFloat(rule.value);
                        setColors(!_.isNaN(val) && !_.isNaN(val2) && val == val2 || false, rule);
                        break;
                    case 'not_equal':
                        var val = parseFloat(value),
                            val2 = parseFloat(rule.value);
                        setColors(!_.isNaN(val) && !_.isNaN(val2) && val != val2 || false, rule);
                        break;
                    case 'between':
                        var val = parseFloat(value),
                            val2 = parseFloat(rule.value[0]),
                            val3 = parseFloat(rule.value[1]);
                        setColors(!_.isNaN(val) && !_.isNaN(val2) && !_.isNaN(val3) && val >= val2 && val <= val3 || false, rule);
                        break;
                    case 'not_between':
                        var val = parseFloat(value),
                            val2 = parseFloat(rule.value[0]),
                            val3 = parseFloat(rule.value[1]);
                        setColors(!_.isNaN(val) && !_.isNaN(val2) && !_.isNaN(val3) && (val < val2 || val > val3) || false, rule);
                        break;
                }
            });
        }
        //end conditional formatting
        if(typeof value == 'undefined') value =  '';
        if(!columnDef.format || !columnDef.format_type || !value) return [value, result_css];
        if(columnDef.format_type == 'number'){
            return [numeral(value).format(columnDef.format), result_css];
        } else if (columnDef.format_type == 'date'){
            if(!('' + value).match(/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/)) return value;
            return [moment(value, 'YYYY-MM-DD hh:mm:ss').format(columnDef.format), result_css];
        } else {
            return [value, result_css];
        }
        return [value, result_css];
    };

    // Ember helpers
    Ember.Handlebars.helper('findAndGetValueByKeyFromObjectArray', function (obj_array, key_name, key, value_name, options) {
        if(!obj_array) return options.default || '';
        var result  = obj_array.findBy(key_name, key);
        if(!result) return options.default || '';
        var escaped = Handlebars.Utils.escapeExpression(result[value_name]);
        return new Ember.Handlebars.SafeString(escaped);
    });
    Ember.Handlebars.helper('momentCalendar', function (date, options) {
        return moment(date).calendar();
    });
    // Вставляются скрипты после сборки
    {{scripts}}
    // проверка авторизован ли пользователь
//    var from = location.hash.slice(1);
//    if(from == '/auth') return;
//    location.hash = '/auth';
//    var hasUser = Ember.util.getCookie('user');
//    if(!hasUser) return;
//    try{
//       App.User =  Ember.Object.create({
//           'id': 99,
//           'login': 'user',
//           'email': 'email@email.com',
//           'password': '1'
//       }); //JSON.parse(CryptoJS.enc.Base64.parse(hasUser).toString(CryptoJS.enc.Utf8));
//    } catch(e){
//        return;
//    }
//    App.User = App.UserModel.create({
//           'id': 99,
//           'login': 'user',
//           'email': 'email@email.com',
//           'password': '1'
//       }
//    );
//    App.User.updateSecret().then(function(){
//       // $('head').append('<script type="application/javascript" src="/static/js/application.js"></script>');
//        if(from.indexOf('app')>=0){
//            location.hash = from;
//        } else location.hash = '#/app';
//    });
});

