App.Router.map(function(){
//    this.route('auth');
//    this.route('register');

    this.resource('app', { path: '/app' }, function(){

        /** Ресурс управления "Таблицы" */


        //this.resource('app.spreadsheets', {path: '/spreadsheets/'});

        this.resource('app.spreadsheets', {path: '/spreadsheets/:filter'}, function(){
            this.route('share');
            this.route('rename');
            this.route('history');
            this.route('create_copy');
            this.route('delete');
        });

        /** Ресурс работы с экземпляром таблицы */
        this.resource('app.table', {path: '/table/:ref'}, function(){
            this.route('main');
            this.route('actions');
        });

        this.resource('app.parser', {path: '/parser/'});
        this.resource('app.accounts', {path: '/accounts/'});
        this.resource('app.logs', {path: '/logs/'});
        this.resource('app.profile', {path: '/profile/'});



        /** Profile and etc routes defines here*/

    });
});