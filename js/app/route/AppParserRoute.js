App.AppParserRoute = Ember.Route.extend({

    title_class: 'title_blue',
    title: 'Правила обработки почты',

    activate: function(){
        this.controllerFor('application').set('title', this.title);
        this.controllerFor('application').set('title_class', this.title_class);
    }
});