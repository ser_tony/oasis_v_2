
App.SpreadsheetsStore = [];

App.AppSpreadsheetsRoute = Ember.Route.extend({

    //по дефолту переход на список всех загруженных таблиц
    defaultFilter: 'all',
    title_class: 'title_yellow',
    title: 'Электронные таблицы',

    activate: function(){
        this.controllerFor('application').set('title', this.title);
        this.controllerFor('application').set('title_class', this.title_class);
    },

    model: function(params){
        var me = this,
            filter = this.defaultFilter;
        if (params && params.filter) {
            filter = params.filter;
        }

        Ember.Logger.debug('Spreadsheets Filter :', filter);
        //загрузить реальные данные по таблицам на основании фильтра запроса
        //var spreadsheetsList =  App.SpreadsheetsModel.create({}).get(filter);

        return new Ember.RSVP.Promise(function(resolve) {
            Ember.$.getJSON('/spreadsheets/list', {}, function(r){
                App.SpreadsheetsStore = r;
                resolve({'spreadsheetsList':r});
            });
        });

        //return Ember.$.getJSON('/spreadsheets/list');
        //return {'spreadsheetsList': spreadsheetsList};
    },

    store: [],


    setupController: function(controller, model){
        controller.set('model', model);
        this._super(controller, model);

        //this.controllerFor('groups').set('model', this.store.find('group'));
    },

    serialize: function(model){
        debugger;
    }

});