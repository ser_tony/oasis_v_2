App.AppTableRoute = Ember.Route.extend({

    title_class: 'title_green',
    title: 'Таблица Магазин Постеров',

//    renderTemplate: function() {
//        debugger;
//        this.render('app/table/index');
//    },

    init: function () {
        this._super();
        //загрузка Realtime libs
        gapi.load("auth:client,drive-realtime,drive-share", function () {
        });
    },


    activate: function () {
        //TODO этот участок повторяется во всех роутах - можно пронаследовать  метод active
        this.controllerFor('application').set('title', this.title);
        this.controllerFor('application').set('title_class', this.title_class);
    },
    model: function (params) {
        /** загрузка модели таблицы со строками и столбцами
         * соответсвующие объекту данных SlickGrid
         * */
        //асинхронная подгрузка скриптов библиотеки SlickGrid (зависимости)
        var model = App.SpreadsheetsStore.findBy('id', params.ref);
        if(model) return model;
        return $.ajax({
            url:'/spreadsheets/list',
            dataType: "json",
            data: {id: params.ref}
        });
    },

    setupController: function (controller, model) {
        controller.set('masterDriveId', model.id);
        controller.set('worksheetDriveId', model.defaultWorksheetId);
        controller.set('model', model);
        controller.loadTableModel();
        // this._super(controller, model);
    },

    serialize: function (model) {
        return model;
    }
});