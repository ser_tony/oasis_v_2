App.AppTableController = Ember.Controller.extend({

    init: function () {
        this._super();
        this.set('access_token', App.User.token);
        //регистрация кастомной объектной модели
        this.registerTableType();
    },

    //токен доступа к документу в Google Drive - обновлять нужно на сервере через refresh_token
    //каждые 3600 sec . Первоначальный access_key лучше передавать с сессионными данными пользователя
    access_token: 'ya29.1.AADtN_VHKaEwr0oPs4Crc3Ggz2d8E7-TWVahmuzWtRZCJwsQ0mrjMogNC0C6SCEPmhkGztk',

    //id Master-таблицы документа на диске Drive пользователя. Передается в параметре URL и обновляется
    masterDriveId: '0B_dYv0Q_Y9SLdWp4S2FiZlgxN1E',

    linkToTable: function(){
        return window.location.href;
    }.property('masterDriveId'),

    //id Worksheet-таблицы документа
    worksheetDriveId: null,

    //пользователь загрузивший док
    collaborateUser: null,

    //пользователю использующие документ on-line
    activeCollaborateUsers: [],

    //порядок сортировки
    isAsc: true,

    /** user actions associated with funcs*/
    actions: {
        switchWorksheet: function (wsheetId) {
            //переключить на лист
            var me = this;
            me.get('view').set('loading', true);
            me.apiLoadWorksheet(wsheetId);
        },
        createWorksheet: function () {
            //создать новый лист
            var me = this;
            me.get('view').set('loading', true);

            return new Ember.RSVP.Promise(function (resolve) {
                Ember.$.post('/spreadsheets/create_worksheet', {'masterDriveId': me.get('masterDriveId')}, function (r) {
                        if (r.workSheetId) {
                            //лист создан -> переход клиента на новый лист
                            me.apiLoadWorksheet(r.workSheetId, true);

                        }
                        resolve();
                    }
                    , 'json');
            });
        }
    },

    //список листов таблицы
    worksheetList: [],

    //текущий рабочий лист
    currentWorksheetId: null,

    apiLoadWorksheet: function (workSheetId, isCreation) {
        /**workSheetId - drive_id документа
         * isCreation - при обычном выборе (isCreation==false) не следует прикреплять лист к таблице
         *  загрузка модели листа сразу после создания или выбора пользователем существующего
         * */
        var me = this,
            wsObject = me.worksheetList.findBy('id', workSheetId);
        me.set('worksheetDriveId', workSheetId);

        if (!wsObject || !wsObject.get('isLoaded') ) {
            //данный лист еще не загружен
            gapi.drive.realtime.load(me.get('worksheetDriveId'),
                function (model) {
                    me.onWorksheetFileLoaded(model, false);
                    me.setCurrentWorksheet({'id': workSheetId});
                },
                function (model) {
                    me.initializeTableWorksheetModel(model)
                },
                function (err) {
                    console.error(arguments);
                    location.href='/google_auth/'
                }
            );
        }
        else {
            me.setCurrentWorksheet({'id': wsObject.get('id')});
            //переключить лист таблицы
            me.onWorksheetFileLoaded({}, true);
        }

    },

    loadTableModel: function () {
        /** Загрузка файла документа с Google Drive с последующей инициализацией
         * полей Realtime API
         * */
        var me = this;
        gapi.auth.setToken({'expires_in': 3599,
            'access_token': me.get('access_token')});

        //первым загружается TableMaster
        gapi.drive.realtime.load(me.get('masterDriveId'),
            function (model) {
                me.onMasterFileLoaded(model);

                ////Master-table создана - можно инициализировать Worksheet
                gapi.drive.realtime.load(me.get('worksheetDriveId'),
                    function (model) {
                        me.onWorksheetFileLoaded(model)
                    },
                    function (model) {
                        me.initializeTableWorksheetModel(model)
                    },
                    function (err) {
                        console.error(arguments);
                        location.href='/google_auth/';
                    }
                );

            },
            function (model) {
                me.initializeTableMasterModel(model)
            },
            function (err) {
                var errorTyper = gapi.drive.realtime.ErrorType;
                //handle errors
                if (err.type == errorTyper.CLIENT_ERROR || err.type == errorTyper.TOKEN_REFRESH_REQUIRED) {
                    //you need refresh access_token
                    return new Ember.RSVP.Promise(function (resolve) {
                        Ember.$.post('/refresh_token', {}, function (r) {
                                me.set('access_token', r.access_token);
                                me.loadTableModel();
                                resolve();
                            }
                            , 'json');
                    });
                }
            });
    },

    initializeTableMasterModel: function (model) {
        /** Once creation method would be call to  instance of realtime-table */
        var me = this;
        me.tableMasterInstance = model.create(App.TableMaster);
        model.getRoot().set('tableMaster', me.tableMasterInstance);
    },

    initializeTableWorksheetModel: function (model, title) {
        /** Will be called once to create structure of Worksheet document in Drive */
        var me = this;
        me.tableWorksheetInstance = model.create(App.TableWorksheet);
        model.getRoot().set('tableWorksheet', me.tableWorksheetInstance);

        var title = title || (me.tableWorksheetInstance.title || 'Лист ' + (me.tableMasterInstance.worksheetList.length + 1)),
            shortWorksheet = {
                'title': title,
                'drive_id': me.worksheetDriveId,
                'id': me.worksheetDriveId
            };
        me.tableMasterInstance.worksheetList.push(shortWorksheet);
        me.initWorksheet(shortWorksheet);
    },

    initWorksheet: function (shortWorksheet) {
        //инициализация воркщита в контроллере
        var me = this,
            wModel = App.WorksheetModel.create(shortWorksheet);
        wModel.isLoaded = true;
        wModel.isCurrent = true;
        me.worksheetList.pushObject(wModel);
        me.setCurrentWorksheet(wModel);
    },

    setCurrentWorksheet: function (wModel) {
        var me = this;
        me.worksheetList.forEach(function (item) {
            item.set('isCurrent', (wModel.id == item.id) ? true : false);
            if (wModel.id == item.id) {
                item.set('isLoaded', true);
            }
        });
        me.set('currentWorksheetId', wModel.id);
    },


    onMasterFileLoaded: function (model) {
        /** This method calls every time when realtime-api is load */
        var me = this;
        if (!me.tableMasterInstance) {
            //таблица была проинициализирована и привязана к файлу на диске Drive
            me.tableMasterInstance = model.getModel().getRoot().get('tableMaster');
        }

        me.tableMasterInstance.addEventListener(gapi.drive.realtime.EventType.VALUE_CHANGED, function () {

            me.doValueChanged();
        });

        me.tableMasterInstance.worksheetList.addEventListener(gapi.drive.realtime.EventType.VALUE_CHANGED, function () {

        });
        //список прикрепеленных листов загружается вместе с мастер-таблицей
        me.set('worksheetList', (me.tableMasterInstance.worksheetList.asArray()).map(function (item) {
            item.isLoaded = false;
            item.isCurrent = false;
            return App.WorksheetModel.create(item);
        }));
        me.setCurrentWorksheet({'id': me.get('worksheetDriveId')});


        if (model.getCollaborators().length==1){
            //в случае, когда первый пользователь открыл документ - очистить список активных ячеек - сессионное хранилище
            //me.tableMasterInstance.collaboratorUsers.clear();
        }

//
        me.set('activeCollaborateUsers', _.clone(model.getCollaborators()));
        var activeCollaborateUsers = me.get('activeCollaborateUsers');
        _.each(activeCollaborateUsers, function(item){
            item.userColor = me.get_random_color()
        });
        var myUser = me.get('activeCollaborateUsers').filterBy('isMe', true)[0];
        myUser.activeCell = {cell:1 , row:1};
        me.set('collaborateUser', myUser);
        me.tableMasterInstance.addNewCollaborative(myUser);


        model.addEventListener(gapi.drive.realtime.EventType.COLLABORATOR_JOINED, function (m) {
            //добавлять в activeUsers me.activeCollaborateUsers
            me.collaboratorJoin(m, model);
        });

        model.addEventListener(gapi.drive.realtime.EventType.COLLABORATOR_LEFT, function (m) {
            //удалять из activeUsers
            me.collaboratorLeft(m);
        });
        //try init second realtime document model
    },

    //initializeTableWorksheetModel

    onWorksheetFileLoaded: function (model, isReinit) {
        //this method will be calling every time when have been loaded Worksheet model document from Drive
        /** @param isReinit - нужно переинициализировать таблицу*/

        var me = this,
            isReinit = isReinit || false;

        //таблица была проинициализирована и привязана к файлу на диске Drive
        var wSheetObject = me.worksheetList.findBy('id', me.get('worksheetDriveId'));

        if (wSheetObject.get('isLoaded') && isReinit) {
            //сохранить ссылку на модель как instance в списке загруженных realtime моделей
            me.tableWorksheetInstance = wSheetObject.get('instance');
        }
        else {
            me.tableWorksheetInstance = model.getModel().getRoot().get('tableWorksheet');
            wSheetObject.set('instance', me.tableWorksheetInstance);
            wSheetObject.set('isLoaded', true);
        }

//        me.tableWorksheetInstance.addEventListener(gapi.drive.realtime.EventType.VALUE_CHANGED, function(){
//            debugger;
//        });
//
//        me.tableWorksheetInstance.addEventListener(gapi.drive.realtime.EventType.OBJECT_CHANGED, function(){
//            debugger;
//        });
//
        me.tableWorksheetInstance.tableDataRows.addEventListener(gapi.drive.realtime.EventType.VALUE_CHANGED, function () {

        });


        if (!isReinit) {
            //первичная загрузка Worksheet и создание Grid во view
            //контроллер провел все необходимые загрузки и инициализации моделей из Realtime API\
            var view = me.get('view');
            view.createGrid(true);
        }
        else {
            //переключение листа - Grid уже существует, нужно заменить данные во view
            var view = me.get('view');
            view.createGrid(false);
        }
    },


    changeRow: function (row) {
        var me = this;
        //TODO: что то не дописано ниже, Тоха что это?
        //me.realtimeTableInstance
    },

    registerTableType: function () {
        /** Регистрация типа модели мастер-таблицы - по правилам взаимодействия с Realtime API*/
        var me = this;

        App.TableMaster = function () {
        };
        gapi.drive.realtime.custom.registerType(App.TableMaster, 'TableMaster');

        App.TableMaster.prototype.title = gapi.drive.realtime.custom.collaborativeField('title');
        App.TableMaster.prototype.lastModify = gapi.drive.realtime.custom.collaborativeField('lastModify');
        //список листов таблицы с названиями
        App.TableMaster.prototype.worksheetList = gapi.drive.realtime.custom.collaborativeField('worksheetList');

          //пользователю пользующиеся листом
        App.TableMaster.prototype.collaboratorUsers = gapi.drive.realtime.custom.collaborativeField('collaboratorUsers');




        App.TableWorksheet = function () {
        };
        gapi.drive.realtime.custom.registerType(App.TableWorksheet, 'TableWorksheet');

        //титул листа Worksheet
        App.TableWorksheet.prototype.title = gapi.drive.realtime.custom.collaborativeField('title');

        // настройки листа, сюда перенести title!
        App.TableWorksheet.prototype.settings = gapi.drive.realtime.custom.collaborativeField('settings');

        //список описания столбцов на листе Worksheet
        App.TableWorksheet.prototype.columns = gapi.drive.realtime.custom.collaborativeField('columns');

        //список комментариев на листе Worksheet
        App.TableWorksheet.prototype.tableCommentData = gapi.drive.realtime.custom.collaborativeField('comments');

        //список стилей ячейки для grid.setCellCssStyles на листе Worksheet
        App.TableWorksheet.prototype.cellCssStylesData = gapi.drive.realtime.custom.collaborativeField('cellCssStyles');

        //представление данных таблицы на листе - Worksheet
        App.TableWorksheet.prototype.tableDataRows = gapi.drive.realtime.custom.collaborativeField('tableDataRows');


//        App.TableWorksheet.prototype.addTableDataRow = function(row){
//            this.tableDataRows.push(row);
//            //console.log('Push row');
//        };

        App.TableWorksheet.prototype.saveDriveModel = function(model){
            this.model = model;
        };

        App.TableWorksheet.prototype.cloneList = function(origin){
            var cloneList = this.model.createList();
            for (var i = 0; i< origin.length; i++){
                cloneList.push(origin.get(i));
            }
            return cloneList;
        };

        App.TableWorksheet.prototype.getColumns = function(){
          //список столбцов листа таблицы
          return this.columns.asArray();
        };

        App.TableWorksheet.prototype.setColumns = function(clms){
            //инициализация столбцов
            var model = gapi.drive.realtime.custom.getModel(this);
            model.beginCompoundOperation();
            this.columns.clear();
            this.columns.pushAll(clms);
            model.endCompoundOperation();
        };

        App.TableWorksheet.prototype.setColumn = function(index, colValue){
            //вставка нового столбца
            //var clone = this.cloneList(this.columns);
            this.columns.insert(index, colValue);
        };

        App.TableWorksheet.prototype.removeColumns = function(from_index, count){
            this.columns.removeRange(from_index, from_index + count);
        };

        App.TableWorksheet.prototype.changeColumns = function(columns_with_index){
            /*
            Сохраняет изменения колонок, например
            columns_with_index = [ [{id: "A", format: "YYYY"}, 3], [{id: "B", format:"YYYY"}], [{id: "ZZ"}, 0] ]
            Данный масив разбирается так:
                - Колонка "A" уже есть, значит она заменит значение format и так как передан индекс 3, переместит
                эту колонки под этот индекс
                - Колонка "В" так же существует, заменится значение format но останется под тем же индексом
                так как новый индекс не передан
                - Колонка "ZZ", не существует, создаст и впишет ее под индекс, если ендекс не передан то в конец списка
            */

            //сохраняем изменения колонок
            var model = gapi.drive.realtime.custom.getModel(this);
            model.beginCompoundOperation();
            for(var i=0; i<columns_with_index.length; i++){
                var current = columns_with_index[i],
                    finded = false,
                    j;
                for(j=0; this.columns.length; j++){
                    if(this.columns.get(j).id == current[0].id){
                        finded = this.columns.get(j);
                        break;
                    }
                }
                if(finded){
                    finded = $.extend(finded, {
                        name: current[0].name,
                        field: current[0].field,
                        focusable: current[0].focusable,
                        format: current[0].format,
                        format_type: current[0].format_type,
                        headerCssClass: current[0].headerCssClass,
                        minWidth: current[0].minWidth,
                        width: current[0].width,
                        hidden: current[0].hidden,
                        validation: current[0].validation,
                        conditional_rules: current[0].conditional_rules
                    });
                }
                if(current.length == 2 && _.isNumber(current[1]) || finded){
                    var indx = j;
                    if(current.length == 2 && _.isNumber(current[1])){
                        indx = current[1];
                    }
                    if(finded) {
                        this.columns.removeRange(j, j+1);
                    } else {
                        finded = {
                            id: current[0].id,
                            name: current[0].name,
                            field: current[0].field,
                            focusable: current[0].focusable,
                            format: current[0].format,
                            format_type: current[0].format_type,
                            headerCssClass: current[0].headerCssClass,
                            minWidth: current[0].minWidth,
                            width: current[0].width,
                            hidden: current[0].hidden,
                            validation: current[0].validation,
                            conditional_rules: current[0].conditional_rules
                        };
                    }
                    this.columns.insert(indx, finded);
                } else {
                    this.columns.push(finded);
                }
            }
            model.endCompoundOperation();
        };

        App.TableWorksheet.prototype.getCellCssStyles = function () {
            return me.mapToObject(this.cellCssStylesData);
        };

        App.TableWorksheet.prototype.changeCellCssStyles = function (styleClassCellArray) {
            var me = this,
                model = gapi.drive.realtime.custom.getModel(this);
            model.beginCompoundOperation();
            _.each(_.keys(styleClassCellArray), function(key){
                me.cellCssStylesData.set(key, styleClassCellArray[key]);
            });
            model.endCompoundOperation();
        };

        App.TableWorksheet.prototype.getTableDataRows = function () {
            return  me.mapToObject(this.tableDataRows);
        };

        App.TableWorksheet.prototype.changeTableRows = function(changedRows, isSaveNote){
            /**
             * Синхронизация - передан массив новых значений. Обновление документа с помощью
             * CompoundOperation
             */
            //TODO: realizing
            var grid = me.get('view').get('grid'),
                model = gapi.drive.realtime.custom.getModel(this),
                tableDataRows = this.tableDataRows;

            model.beginCompoundOperation();
            _.each(changedRows, function(cell, key){
                var val = cell.getCalculatedValue(),
                    formula = cell.isFormula() ? cell.formula : val,
                    notes = cell.notes;

                if(!isSaveNote){
                    tableDataRows.set(cell.getId(), {
                        metadata: cell.metadata,
                        formula: formula,
                        value: val
                    });
                } else {
                    tableDataRows.set(cell.getId(), {
                        notes: notes
                    });
                }
            });
            model.endCompoundOperation();

        }

        App.TableWorksheet.prototype.removeRows = function(from_index, count){
            from_index++;
            var tableDataRows = this.tableDataRows,
                sortedDataAttrs = {},
                sortedDataNums = [],
                i = 0,
                model = gapi.drive.realtime.custom.getModel(this),
                keys = tableDataRows.keys();

            for(var i = 0; i < keys.length; i++){
                var match = keys[i].match(/([A-Z]+)([0-9]+)/);
                if(!match) continue;
                sortedDataNums.push(parseInt(match[2], 10));
                if(!sortedDataAttrs[match[2]]) sortedDataAttrs[match[2]] = [];
                sortedDataAttrs[match[2]].push(match[1]);
                sortedDataAttrs[match[2]] = _.sortBy(sortedDataAttrs[match[2]]);
            }
            sortedDataNums = _.uniq(sortedDataNums);
            sortedDataNums = _.sortBy(sortedDataNums);

            model.beginCompoundOperation();
            for(var i=0; i<sortedDataNums.length; i++){
                var num = parseInt(sortedDataNums[i] + '', 10);
                if(num < from_index){
                    continue;
                }
                if(num <= from_index + count){
                    _.each(sortedDataAttrs[num], function(attr){
                        tableDataRows.delete(attr + num);
                    });
                    continue
                }
                _.each(sortedDataAttrs[num], function(attr){
                    var dataCell = tableDataRows.get(attr + num);
                    tableDataRows.set(attr + (num - count), dataCell);
                    tableDataRows.delete(attr + num);
                });
            }
            model.endCompoundOperation();
        }

        App.TableWorksheet.prototype.addRow = function(to_index, row){
            /*
            Add row to table by index "to_index".
            row = [Cell, Cell, ...]
             */
            to_index++;
            var tableDataRows = this.tableDataRows,
                model = gapi.drive.realtime.custom.getModel(this),
                sortedDataAttrs = {},
                sortedDataNums = [],
                keys = tableDataRows.keys();
            for(var i = 0; i < keys.length; i++){
                var match = keys[i].match(/([A-Z]+)([0-9]+)/);
                if(!match) continue;
                sortedDataNums.push(parseInt(match[2], 10));
                if(!sortedDataAttrs[match[2]]) sortedDataAttrs[match[2]] = [];
                sortedDataAttrs[match[2]].push(match[1]);
                sortedDataAttrs[match[2]] = _.sortBy(sortedDataAttrs[match[2]]);
            }
            sortedDataNums = _.sortBy(_.uniq(sortedDataNums));
            sortedDataNums.reverse();

            model.beginCompoundOperation();
            for(var i=0; i<sortedDataNums.length; i++){
                var num = parseInt(sortedDataNums[i], 10);
                if(num < to_index) continue;
                _.each(sortedDataAttrs[num], function(attr){
                    var dataCell = tableDataRows.get(attr + num);
                    tableDataRows.set(attr + (num + 1), dataCell);
                    tableDataRows.delete(attr + num);
                });
                if(num == to_index && row){
                    _.each(row, function(cell){
                        tableDataRows.set(cell.getId(), {
                            metadata: cell.metadata,
                            formula: cell.value,
                            notes: cell.notes
                        });
                    });
                }
            }
            model.endCompoundOperation();

        }

        App.TableWorksheet.prototype.changeTableRow = function (rowArgs, isSaveNote) {
            /**
             * Синхронизация с документом Drive
             * Одно значение
             * @type {*}
             */
            var grid = rowArgs.grid,
                columnName = grid.getColumns()[rowArgs.cell].name,
                cell = grid.getData().spreadsheet.getCell(columnName + (rowArgs.row+1)),
                pos = cell.position,
                fieldPos = pos.col + pos.row,
                old_data = this.tableDataRows.get(fieldPos);
            if(!isSaveNote){
                this.tableDataRows.set(fieldPos, $.extend(old_data, {
                    metadata: cell.metadata,
                    formula: cell.value
                }));
            } else {
                this.tableDataRows.set(fieldPos, $.extend(old_data, {
                    notes: cell.notes
                }));
            }
        };
        App.TableWorksheet.prototype.getTableCommentData = function () {
            return me.mapToObject(this.tableCommentData);
        };

        App.TableWorksheet.prototype.saveTableComment = function (comment, id_cell_comment) {
            /**
             * Add or Update comment in tableCommentData Model.
             * Structure tableCommentData:
             *      Map({
             *          "AA1": {
             *              "sequence_index": 0, // unique sequance index for comment id
             *              "comments": [{comment},{comment},...]
             *          }
             *      })
             * @type Object
             */
            var commentsObject = this.tableCommentData.get(id_cell_comment);
            if (Em.isNone(commentsObject)) {
                commentsObject = {
                    "sequence_index": 0,
                    "comments": []
                }
            }
            if (comment.id) {
                var comment_old = commentsObject['comments'].findBy('id', comment.id);
                if (!comment_old || comment_old.author != App.User.get('email').split('@')[0]) return;
                comment = $.extend(comment_old, comment);
            } else {
                // generate unique ID, row_id+cell_name+sequence_index
                commentsObject.sequence_index++;
                comment.id = commentsObject.sequence_index;
                commentsObject.comments.push(comment);

            }
            this.tableCommentData.set(id_cell_comment, commentsObject);
            return comment;
        };
        App.TableWorksheet.prototype.removeTableComment = function (id_cell_comment, id_comment) {
            var commentsObject = this.tableCommentData.get(id_cell_comment);
            if (Em.isNone(commentsObject)) return false;
            var comment = commentsObject.comments.findBy('id', id_comment);
            if(!comment || comment.author != App.User.get('email').split('@')[0]) return;
            commentsObject.comments = _.without(commentsObject.comments, comment);
            this.tableCommentData.set(id_cell_comment, commentsObject);
            return true;
        };
        // достаем значение из settings по ключу
         App.TableWorksheet.prototype.getSettingByKey = function (key) {
             return this.settings.get(key);
         }
        // сохраняем значение в settings по ключу
         App.TableWorksheet.prototype.setSettingByKey = function (key, value) {
             return this.settings.set(key, value);
         }
        // удаляем значение из settings по ключу
         App.TableWorksheet.prototype.removeSettingByKey = function (key) {
             return this.settings.delete(key);
         }


        App.TableMaster.prototype.addNewCollaborative = function(newUser){
            var users = this.collaboratorUsers.asArray(),
                founds = users.findBy('sessionId', newUser.sessionId);
            if (!founds){
                this.collaboratorUsers.push(newUser);
            }
            else {
                //заменить activeCell
                this.collaboratorUsers
            }
        };

        App.TableMaster.prototype.getOutsideCollaborative = function () {
            var collUsers = me.get('activeCollaborateUsers'), //this.collaboratorUsers.asArray();
                currentSessId = me.get('collaborateUser').sessionId;
            return _.reject(collUsers, function(item){return item.sessionId == currentSessId});
        };

        App.TableMaster.prototype.changeCollaborativeActiveCell = function (obj) {
            var sessId = me.get('collaborateUser').sessionId,
                userList = this.collaboratorUsers.asArray();
            var myUserIndex =  me.customIndexOf(userList, 'sessionId', sessId)[0];
            var myUser = this.collaboratorUsers.get(myUserIndex);
            if (myUser) {
                myUser.activeCell.cell = obj.cell;
                myUser.activeCell.row = obj.row;
            }
            var updatedUser = {};
            for (var key in myUser) {
                if (myUser.hasOwnProperty(key)) {
                    updatedUser[key] = myUser[key];
                }
            }
            //активная ячейка пользователя изменилась - изменить значение в realtime списке
            this.collaboratorUsers.set(myUserIndex, updatedUser);
        };


        gapi.drive.realtime.custom.setInitializer(App.TableMaster, me.tableMasterModelInitializer);
        gapi.drive.realtime.custom.setOnLoaded(App.TableMaster, function () {
            me.onMasterLoaded(this)
        });

        gapi.drive.realtime.custom.setInitializer(App.TableWorksheet, me.tableWorksheetModelInitializer);
        gapi.drive.realtime.custom.setOnLoaded(App.TableWorksheet, function () {
            me.onWorksheetLoaded(this)
        });

    },

    customIndexOf: function (array, field,  val) {
        //object index in array by field value
        var result = [-1, undefined];

        _.some(array, function (element, index) {
            if (val === element[field]) {
                result[0] = index;
                result[1] = element;
                return true;
            }

            return false;
        });

        return result;
    },


    arrayToArray: function (from) {
        var to = [];
        for (var i = 0; i < from.length; i++) {
            to.push(from[i]);
        }
        return to
    },

    mapToObject: function (mapObj) {
        var o = {},
            items = mapObj.items();
        for(var i=0; i<items.length; i++){
            o[items[i][0]] = items[i][1];
        }
        return o;
    },

    tableMasterModelInitializer: function () {
        //инициализатор Мастер таблицы
        var me = this,
            model = gapi.drive.realtime.custom.getModel(this);

        //список из рабочих листов
        this.worksheetList = model.createList();

        //collaborative users list initialization
        this.collaboratorUsers = model.createList();

    },

    tableWorksheetModelInitializer: function () {
        /** инициализатор модели таблицы внутренних полей */

        var me = this,
            model = gapi.drive.realtime.custom.getModel(this);
        this.settings = model.createMap();
        this.tableCommentData = model.createMap();
        this.tableDataRows = model.createMap();
        this.cellCssStylesData = model.createMap();
        this.columns = model.createList();

        //default table values:
        // prepare the data
        model.beginCompoundOperation();
        var initDataObject = {},
            initRowCount = 100;
        for (var x = 'A'.charCodeAt(0); x <= 'Z'.charCodeAt(0); x++) {
            for (var y = 1; y <= initRowCount; y++) {
                if (y == 1 || y == initRowCount) {
                    this.tableDataRows.set(String.fromCharCode(x) + y, {formula: ''});
                }

                //if (y == 1 || y == initRowCount) {
//                if(x<'E'.charCodeAt(0)){
//                    this.tableDataRows.set(String.fromCharCode(x) + y, {formula: ''+y});
//                }
//                else if (y == 1 || y == initRowCount){
//                    this.tableDataRows.set(String.fromCharCode(x) + y, {formula: ''});
//                }
            }
        }
        model.endCompoundOperation();
        this.saveDriveModel(model);
    },

    doValueChanged: function () {
        //экземпляр таблицы был изменен по Collaborative event
    },

    onMasterLoaded: function (model) {
        var me = this;
        //collaboratorUsers
        model.addEventListener(gapi.drive.realtime.EventType.OBJECT_CHANGED, function (model) {
           if(model.events){
               var eventArr = model.events,
                   len = eventArr.length;
               for (var ind = 0; ind < len; ind++){
                   var event = eventArr[ind];
                   if(event.isLocal){
                       continue;
                   }
                   var recievedValues = event.newValues;
                   for (var j=0; j< recievedValues.length; j++){
                       var val = recievedValues[j];
                       if (val.hasOwnProperty('userId')){
                           var userObjectIndex = me.customIndexOf(me.activeCollaborateUsers, 'sessionId', val.sessionId);
                           userObjectIndex = userObjectIndex[0];
                           me.activeCollaborateUsers[userObjectIndex] = val;
                       }
                   }
                   me.get('view').renderActiveCollaboratorCell();
               }
           }
        });

    },

    onWorksheetLoaded: function (model) {
        var me = this;

        model.addEventListener(gapi.drive.realtime.EventType.OBJECT_CHANGED, function (model) {

            if (_.isArray( model.events)) {

                var view = me.get('view'),
                    changingLen = model.events.length,
                    multiplyChange = changingLen>1 ? true : false,
                    changingList = [];

                for (var index = 0; index < changingLen; index++){
                    var event = model.events[index];
                    if (event.isLocal) {
                        if (!multiplyChange) {
                            return;
                        }
                        else {
                            continue;
                        }
                    }

                    var value = event.newValue,
                    formula= '',
                    cellPos = event.property,
                    cellChanged = {};
                    if (_.isObject(value)){
                        formula = value.formula;
                        value = value.value? value.value: value.formula;
                    }
                    else {
                        formula = value;
                    }
                    //изменилось значение ячейки таблицы
                    cellChanged['position'] = cellPos;
                    cellChanged['value'] = value;
                    cellChanged['formula'] = formula;

                    if (multiplyChange){
                        changingList.push(cellChanged);
                    }
                    else {
                        view.updateGridItem(cellChanged);
                        return;
                    }
                }
                view.updateGridItems(changingList);
            }
        });
    },


    get_random_color: function() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.round(Math.random() * 15)];
        }
        return color;
    },


    collaboratorJoin: function (onemodel) {
        //добавить в список его активную ячейку
        var me = this;

        var ind = me.customIndexOf(me.get('activeCollaborateUsers'), 'sessionId', onemodel.collaborator.sessionId);
        if(ind[0]==-1){
            var activeCell = {cell:1 , row:1};
            onemodel.collaborator.activeCell = activeCell;
            onemodel.collaborator.userColor = me.get_random_color();
            me.get('activeCollaborateUsers').push(onemodel.collaborator);
           me.get('view').renderActiveCollaboratorCell();
        }
    },

    collaboratorLeft: function (onemodel) {
        var me = this;
        var ind = me.customIndexOf(me.get('activeCollaborateUsers'), 'sessionId', onemodel.collaborator.sessionId);
        if(ind){
           me.get('activeCollaborateUsers').removeAt(ind[0]);
           me.get('view').renderActiveCollaboratorCell();
        }
        //убрать из списка юзеров документа, убрать ячейку из таблицы
    },


    tableContentChanged: function () {

    },


    applyFunToCell: function(funcItem){
        /** Применить функцию к ячейке (поведение аналогичное Google Spreadsheets):
         * 1. если пустая -  вставка =
         * 2. если не пустая и не является формулой - тоже что в 1.
         * 3. вставка функции с whitespace
         *
         * @param funcItem - { id:1,
                                title: 'COUNT',
                                params: '(значение 1; Значение 2; ...)',
                                description: 'Подсчитывает количество ячеек в диапазоне, который содержит числа',
                                localize_name: 'Количество',
                                type: 1,
                                isActive:true
                              }
         */
        var me = this,
            view = me.get('view'),
            funcCalling = funcItem.title + '()';
        var activeCell = view.getActiveCellObject(),
            cellValue = activeCell.calculateValue() || '';
        if (cellValue == '' || !activeCell.isFormula()){
            cellValue = '=' + funcCalling;
        }
        else {
            cellValue = activeCell.formula;
            cellValue += ' '  + funcCalling;
        }
        //TODO: после updateGridItem - происходит пересчет формулы и теяется форма с Editor
        view.get('grid').editActiveCell();
        var formulaInput = $(view.get('grid').getActiveCellNode()).find('.editor-text');
        formulaInput.val(cellValue);
        //formulaInput[0].setSelectionRange(cellValue.length-1, cellValue.length-1);
//        view.updateGridItem({
//            value: cellValue,
//            position: activeCell.getId()
//        }, true);
    },

    sharingTable: function(){
        var me = this;
        return new Ember.RSVP.Promise(function(resolve) {
            Ember.$.ajax({url:'/spreadsheets/share',
                         type: 'POST',
                         dataType: "json",
                         data: {
                            table_master_id:me.get('masterDriveId'),
                            link_to_doc: me.get('linkToTable'),
                            users: JSON.stringify(me.get('model').users)
                         }
                },
                function(r){
                    console.log(r);
                });
        });

    }

});