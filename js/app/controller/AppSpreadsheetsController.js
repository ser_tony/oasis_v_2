App.AppSpreadsheetsController = Ember.Controller.extend({
    GOOGLE_CLIENT_ID: '314413490003.apps.googleusercontent.com',
    GOOGLE_SCOPES: 'https://www.googleapis.com/auth/drive',

    isSelectedAllSpreadsheets: false,

    isSelectedAllSpreadsheetsObserve: function(){
        var spreadsheets = this.get('content').spreadsheetsList,
            len_selected = spreadsheets.filterBy('selected', true).length,
            is_all_selected = this.get('isSelectedAllSpreadsheets');
        if(!is_all_selected && len_selected < spreadsheets.length) return;
        for(var i=0; i<spreadsheets.length; i++){
            Em.set(spreadsheets[i], 'selected', is_all_selected);
        }
    }.observes('isSelectedAllSpreadsheets'),

    isDisabledActions: function(){
        var spreadsheets = this.get('content').spreadsheetsList,
            len_selected = spreadsheets.filterBy('selected', true).length,
            is_all_selected = len_selected == spreadsheets.length;
        if(this.get('isSelectedAllSpreadsheets') != is_all_selected){
            this.set('isSelectedAllSpreadsheets', is_all_selected);
        }
        return len_selected == 0;
    }.property('content.spreadsheetsList.@each.selected'),

    actions: {
        createSpreadsheet: function(){
            var me = this;
            return new Ember.RSVP.Promise(function(resolve) {
            Ember.$.post('/spreadsheets/create', {}, function(r){
                    me.get('model')['spreadsheetsList'].push(r)
                    resolve();
                    location.href='/application';
                }, 'json');
            });
        },

        copySpreadsheets: function(){
            var me = this,
                spreadsheets = me.get('content').spreadsheetsList,
                selected_spreadsheets = spreadsheets.filterBy('selected', true);
            $.ajax({
                url: '/spreadsheets/copy',
                type: 'POST',
                dataType: 'json',
                data: {
                    'ids': _.pluck(selected_spreadsheets, 'id').join(',')
                }
            }).done(function(documents){
                var spreadsheets = _.union(documents, me.get('content').spreadsheetsList);
                Em.set(me.get('content'), 'spreadsheetsList', spreadsheets);
            });
        },

        deleteSpreadsheets: function(){
            var me = this,
                spreadsheets = me.get('content').spreadsheetsList,
                selected_spreadsheets = spreadsheets.filterBy('selected', true);
            $('#deletePopup').hide();
            $('#js-spreadsheetDelete').addClass('disabled');
            $.ajax({
                url: '/spreadsheets/delete',
                type: 'POST',
                dataType: 'json',
                data: {
                    'ids': _.pluck(selected_spreadsheets, 'id').join(',')
                }
            }).done(function(ids){
                Em.set(me.get('content'), 'spreadsheetsList', _.filter(
                        me.get('content').spreadsheetsList,
                        function(d){
                            return _.indexOf(ids, d.id) < 0;
                        }
                    )
                );
                $('#js-spreadsheetDelete').removeClass('disabled');
            });
        }
    }
});