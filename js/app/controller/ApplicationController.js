App.ApplicationController = Ember.Controller.extend({
    authenticated: false,
    lang: 'en_RU',
    locale: 'RU',

    init: function () {
        var me = this;
        me._super();
        me.set('lang', Ember.util.getLanguage());
        if (this.get('lang') == 'en_US') {
            me.set('locale', 'EN');
        } else {
            me.set('locale', 'RU');
        }
    },
    actions: {
        changeLocale: function (lang) {
            var me = this;
            if (!Ember.util.languages.find(function (item) {
                return lang == item;
            })) lang = 'en_US';
            me.set('lang', lang);
            if (lang == 'en_US') {
                me.set('locale', 'EN');
            } else {
                me.set('locale', 'RU');
            }
            Ember.util.changeLaguage(lang);
            $('#selectedLocale').text(me.get('locale'));
            $.getScript('/static/js/app/i18n/' + lang + '.js')
                .done(function (script) {
                    eval(script);
                    App.reset();
            });
        }
    },
    currentPathDidChange: function() {
        App.set('currentPath', this.get('currentPath'));
    }.observes('currentPath'),

    setCommon: function(o){
      for (var i in o){
          if (o.hasOwnProperty(i)){
            this.set(i, o[i]);
          }
      }
    },

    //поля описывающие общие состояния приложения - заголовки, поцветки
    title: '',
    title_class: ''

});


//TODO переопределение checkbox
Ember.CheckboxDress = Ember.Checkbox.extend({

});