// ####################################################################################
// #######                                                                      #######
// ####### Plugin:      jScroll                                                 #######
// ####### Author:      William Duffy                                           #######
// ####### Website:     http://www.wduffy.co.uk/jScroll                         #######
// ####### Version:     1.1	                                                    #######
// #######                                                                      #######
// ####### Copyright (c) 2011, William Duffy - www.wduffy.co.uk                 #######
// #######                                                                      #######
// ####### Permission is hereby granted, free of charge, to any person          #######
// ####### obtaining a copy of this software and associated documentation       #######
// ####### files (the "Software"), to deal in the Software without              #######
// ####### restriction, including without limitation the rights to use,         #######
// ####### copy, modify, merge, publish, distribute, sublicense, and/or sell    #######
// ####### copies of the Software, and to permit persons to whom the            #######
// ####### Software is furnished to do so, subject to the following             #######
// ####### conditions:                                                          #######
// #######                                                                      #######
// ####### The above copyright notice and this permission notice shall be       #######
// ####### included in all copies or substantial portions of the Software.      #######
// #######                                                                      #######
// ####### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      #######
// ####### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES      #######
// ####### OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND             #######
// ####### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT          #######
// ####### HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,         #######
// ####### WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING         #######
// ####### FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR        #######
// ####### OTHER DEALINGS IN THE SOFTWARE.                                      #######
// #######                                                                      #######
// ####################################################################################
(function($) {
    
    // Public: jScroll Plugin
    $.fn.jScroll = function(options) {

        var opts = $.extend({}, $.fn.jScroll.defaults, options);

        return this.each(function() {
			var $element = $(this);
			var $window = $(window);
			var locator = new location($element);

			$window.scroll(function() {

                //костылек под адаптивный интерфейс
                var windowWidth = $(window).width();
                if ( windowWidth <=992) {
                    //возврат panel в исходное положение
                    $element.css('top', 0);
                    $element.css('margin-top', 0);
                    return
                }
				$element
					.stop()
					.animate(locator.getMargin($window), opts.speed);
			});
        });
		
		// Private 
		function location($element)
		{

			this.min = $element.offset().top;
			this.originalMargin = parseInt($element.css("margin-top"), 10) || 0;
			this.prevTop = $(window).scrollTop();
            this.startDown = null;
            this.startUp = null;
            //this.currentTop = $window.scrollTop();


			this.getMargin = function ($window)
			{

				var max = $element.parent().height() - $element.outerHeight();
				var margin = this.originalMargin,
                    windowScroll = $window.scrollTop(),
                    currentTop = $window.scrollTop(),
                     currentMarginTop = parseInt($element.css("margin-top"), 10) || 0;



                    if (windowScroll >= this.min) {

                        //в какую сторону движение скролла?
                        if (currentTop > this.prevTop) {

                            if (this.startDown==null){ //движение вниз только что началось
                                this.startDown = currentTop;
                                this.startUp = null;
                            }

                            //скролл вниз
                            var elemHeight = parseFloat($element.outerHeight());

                            if(elemHeight> $window.height()){
                                if ((this.startDown + elemHeight) <= (windowScroll + $window.height())) {
                                    margin = margin + opts.top + windowScroll - (elemHeight - $window.height()) -this.min;
                                }
                                else {
                                    margin = currentMarginTop;
                                }
                            }
                            else {
                                margin = margin + opts.top + windowScroll  - this.min;
                            }

                        }
                        else {
                            if (this.startUp==null){ //движение вверх только что началось
                                this.startUp = currentTop;
                                this.startDown = null;
                            }

                            //скролл вверх
                            var elemHeight = parseFloat($element.outerHeight());
                            if(elemHeight> $window.height()){
                                if ((this.startUp - elemHeight)>= (windowScroll - $window.height())) {
                                    margin = margin + opts.top + windowScroll - this.min;
                                }
                                else {
                                    margin = currentMarginTop;
                                }
                            }
                            else {
                                margin = margin + opts.top + windowScroll - this.min;
                            }
                        }
                    }


                if (margin > max)
                     margin = max;

                this.prevTop = currentTop;
                return ({"marginTop" : margin + 'px'});
			}
		}	   
		
    };

    // Public: Default values
    $.fn.jScroll.defaults = {
        speed	:	"slow",
		top		:	10
    };

})(jQuery);