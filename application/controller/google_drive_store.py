# -*- coding: UTF-8 -*-
from apiclient.http import MediaFileUpload, MediaInMemoryUpload
import httplib2
import logging
import json
import re
from tornado.web import RequestHandler

from apiclient.discovery import build
from apiclient import errors
from apiclient import discovery
from oauth2client.client import flow_from_clientsecrets, AccessTokenRefreshError
from oauth2client.keyring_storage import Storage

from oauth2client.client import OAuth2WebServerFlow
import random
import string

# DEVEPOPERS SETTINGS
CLIENT_ID = '275278333351-376cc9et6nalf5k90oeg69o94ks8ofht.apps.googleusercontent.com'
CLIENT_SECRET = 'JMXylVjI7K6qr0KpSRXZrvAe'
REDIRECT_URI = 'http://localhost:8081/google_auth'
cookie_domain = 'localhost'


# SERVER TEST SETTINGS
# CLIENT_ID = '314413490003-cnu2jm0apdtbp05900r9v1l2it2ftikr.apps.googleusercontent.com'
# CLIENT_SECRET = 'igBHSgjXRZFk6bbW19H3_Jb8'
# REDIRECT_URI = 'http://oasis.hashmind.com/google_auth'
# cookie_domain = 'oasis.hashmind.com'




def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def property_to_dict(properties):
        """

        :rtype : dict
        """
        result = {}
        if not properties:
            return result
        for p in properties:
            result[p.get('key')] = p.get('value')
        return result


class GoogleDrive(object):
    __service = None

    __scope = " ".join([
        'email',
        'profile',
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/drive.file'
    ])


    def __init__(self, user):
        self.__service = self.__createDriveService(user)

    def __createDriveService(self, user):
        """Builds and returns a Drive service object authorized with the given service account.

        Returns:
          Drive service object.
        """
        service = None
        try:
            storage = Storage('oasis_app', user)
            credentials = storage.get()

            http = httplib2.Http()
            http = credentials.authorize(http)
            service = build('drive', 'v2', http=http)
        except Exception as ex:
            logging.exception("Error in init Google drive")

        return service

    def get_refresh_access_token(self, user):
        storage = Storage('oasis_app', user)
        credentials = storage.get()
        # if you need new access_token ?
        if credentials.access_token_expired:
            credentials.refresh(httplib2.Http())

        return credentials.access_token

    def retrieve_all_files(self, user):
        """Retrieve a list of File resources.

        Returns:
          List of File resources.
        """

        if self.__service is None:
            self.__createDriveService(user)

        result = []
        page_token = None

        while True:
            try:
                param = {}
                if page_token:
                    param['pageToken'] = page_token

                files = self.__service.files().list(**param).execute()

                result.extend(files['items'])
                page_token = files.get('nextPageToken')
                if not page_token:
                    break
            except errors.HttpError, error:
                print 'An error occurred: %s' % error
                break
        return result

    def retrieve_spreadsheet_by_id(self, user, id_document):
        """
        Get document by ID.
        :param user: current user
        :param id_document: id document
        """
        if self.__service is None:
            self.__createDriveService(user)
        document = self.__service.files().get(fileId=id_document).execute()
        if not document or document.get('isDeleted'):
            return False

        return document

    def retrieve_spreadsheets(self, user, case="title contains 'oasis_service_master__' and trashed=false"):
        """
        get only spreadsheets
        """
        if self.__service is None:
            self.__createDriveService(user)

        result = []
        page_token = None

        #finding master docs
        while True:
            try:
                param = {'q': case}  #{'q': "mimeType='application/vnd.google-apps.drive-sdk'"}
                #"mimeType='application/vnd.google-apps.spreadsheet'"}

                if page_token:
                    param['pageToken'] = page_token

                files = self.__service.files().list(**param).execute()

                result.extend(files['items'])
                page_token = files.get('nextPageToken')
                if not page_token:
                    break
            except errors.HttpError, error:
                logging.exception('An error occurred: %s' % error)
                break

        return result

    def delete_spreadsheets(self, ids_document, user):
        deleted_ids = []
        for id_document in ids_document:
            try:
                document = self.retrieve_spreadsheet_by_id(user, id_document)
            except Exception as ex:
                continue
            if document.get('properties'):
                for p in document.get('properties'):
                    if not re.match('^ws__', p.get('key')):
                        continue
                    try:
                        self.__service.files().delete(fileId=p.get('value')).execute()
                    except Exception as ex:
                        pass
            try:
                self.__service.files().delete(fileId=id_document).execute()
                deleted_ids.append(id_document)
            except Exception as ex:
                pass
        return deleted_ids

    def copy_spreadsheets(self, ids_document, user):
        new_docs = []
        new_docs_for_copy = []
        for id_document in ids_document:
            document = self.retrieve_spreadsheet_by_id(user, id_document)
            if not document:
                return False
            body = {
                'mimeType': 'application/vnd.google-apps.drive-sdk',
                'title': document.get('title') + '__copy',
                'properties': [{'key': p.get('key'), 'value': p.get('value'), 'visibility': 'PRIVATE'} for p in document.get('properties')],
                'parents': document.get('parents'),
                'labels': {
                    'trashed': document.get('labels').get('trashed'),
                    'viewed': document.get('labels').get('viewed')
                }
            }
            # copy parent file
            new_document = self.__service.files().insert(body=body).execute()
            # copy list
            lists = []
            for p in document.get('properties'):
                if not re.match('^ws__', p.get('key')):
                    continue
                lists.append(p.get('value'))
            worksheets = []
            for list_id in lists:
                document = self.retrieve_spreadsheet_by_id(user, list_id)
                if not new_document.get('properties'):
                    new_document['properties'] = []
                body = {
                    'mimeType': 'application/vnd.google-apps.drive-sdk',
                    'title': document.get('title') + '__copy',
                    'properties': document.get('properties'),
                    'parents': document.get('parents'),
                    'labels': {
                        'trashed': document.get('labels').get('trashed'),
                        'viewed': document.get('labels').get('viewed')
                    }
                }
                ws_new_document = self.__service.files().insert(body=body).execute()
                property_obj = {
                    'key': 'ws__%s' + ws_new_document.get('id'),
                    'value': ws_new_document.get('id')
                }
                self.__service.properties().insert(fileId=new_document.get('id'), body=property_obj).execute()
                new_document.get('properties').append(property_obj)
                worksheets.append([list_id, ws_new_document.get('id')])
            new_docs_for_copy.append([id_document, new_document.get('id')])
            new_docs.append(new_document)
            self.copy_realtime_data(worksheets)
        self.copy_realtime_data(new_docs_for_copy)
        return new_docs

    def copy_realtime_data(self, documents):
        for id_document_from, id_document_to in documents:
            data = self.__service.realtime().get(fileId=id_document_from).execute()
            self.__service.realtime().update(
                fileId=id_document_from,
                media_body=MediaInMemoryUpload(data, 'application/json', resumable=False)
            ).execute()

    def get_worksheets_from_pros(self, properties):
        result = []
        if properties is None:
            return result
        for p in properties:
            if not re.match('^ws__', p.get('key')):
                continue
            result.append(p.get('value'))
        return result

    def create_spreadsheet(self, fName):
        """
        fName - name of new document

        Return new file
        """
        rand_name = id_generator()
        body = {
            'mimeType': 'application/vnd.google-apps.drive-sdk',
            'title': 'oasis_service_master__' + rand_name,
        }
        #create Table Master doc
        parentFile = self.__service.files().insert(body=body).execute()

        #create Table Worksheet
        fileChild = self.__service.files().insert(body={
            'mimeType': 'application/vnd.google-apps.drive-sdk',
            'title': 'oasis_service_child__' + rand_name + '__' + id_generator(),
        }).execute()

        parentFile['worksheets'] = [fileChild]
        prop = self.__service.properties().insert(
            fileId=parentFile.get('id'), body={
                'key': 'ws__%s' % fileChild.get('id'),
                'value': fileChild.get('id'),
                'visibility': 'PRIVATE'
            }).execute()


        ownerName = parentFile['ownerNames'][0]
        owner = {
            'name': ownerName,
            'role': 'owner',
            'value': ownerName
        }

        #TODO : when creating new worksheet - open access(sharing by this properties)
        propUser = self.__service.properties().insert(
            fileId=parentFile.get('id'), body={
                'key': 'user__%s' % owner['role'],
                'value': owner['value'],
                'visibility': 'PRIVATE'
            }).execute()


        #rand_name
        parentFile['properties'] = [prop, propUser]
        return parentFile

    def create_worksheet(self, masterDriveId):
        """Создание нового рабочего листа в таблице

        @param parent_doc_id - id

        return Object of child document in Drive
        """
        #get ID of parent

        #достать мастер-таблицу по Drive-id
        parentFile = self.__service.files().get(fileId=masterDriveId).execute()
        parentNameId = parentFile.get('title', '').split('__')[1]

        fileChild = self.__service.files().insert(body={
            'mimeType': 'application/vnd.google-apps.drive-sdk',
            'title': 'oasis_service_child__' + parentNameId + '__' + id_generator(),
        }).execute()
        # добавляем новый worksheet property в родительский документ
        body = {
            'key': 'ws__%s' % fileChild.get('id'),
            'value': fileChild.get('id'),
            'visibility': 'PRIVATE'
        }
        self.__service.properties().insert(fileId=parentFile.get('id'), body=body).execute()

        #расшарить доступ всем юзерам в списке доступа для новосозданного листа
        #sharing (fileId)
        spreadSheetMaster = SpreadsheetFileModel(parentFile)
        for user in spreadSheetMaster.users:
            new_permission = {
                  'value': user.get('value'),
                  'type': 'user',
                  'role': user.get('role', 'reader')
            }
            if new_permission.get('role') != 'owner':
                workPermissions = self.__service.permissions().insert(fileId=fileChild['id'],
                                                                      sendNotificationEmails=False,
                                                                      body=new_permission).execute()
        return fileChild


    def file_permission(self, id):
        persmission = self.__service.permissions().list(fileId=id).execute()
        persmission = persmission.get('items')
        return persmission

    def sharing_file(self, auth_user, table_master_id, link_to_doc, users):
        permissions = None
        document = self.retrieve_spreadsheet_by_id(auth_user, table_master_id)
        if not document:
            self.send_error(404)

        spreadSheetMaster = SpreadsheetFileModel(document)

        for user in users:
            if not user.get('added', False) and user.get('value', '') != '':
                new_permission = {
                  'value': user.get('value'),
                  'type': 'user',
                  'role': user.get('role', 'reader')
                }
                try:
                    #расшаривание доступа мастер таблице
                    descr = u'Shared for you this Excel spreadsheet <a href="'+ link_to_doc + '">Document link</a>'
                    permissions = self.__service.permissions().insert(fileId=table_master_id,
                                                                      sendNotificationEmails=True,
                                                                      emailMessage=descr,
                                                                      body=new_permission).execute()

                    #расшаривание доступа всем листам таблицы
                    for worksheetId in spreadSheetMaster.worksheets:
                        workPermissions = self.__service.permissions().insert(fileId=worksheetId,
                                                                              sendNotificationEmails=False,
                                                                              body=new_permission).execute()
                    owner = {
                        'name': new_permission.get('value'),
                        'role': new_permission.get('role'),
                        'value': new_permission.get('value')
                    }
                    self.__service.properties().insert(fileId=table_master_id, body={
                                                                    'key': 'user__%s' % owner['role'],
                                                                    'value': owner['value'],
                                                                    'visibility': 'PRIVATE'
                                                                }).execute()

                except errors.HttpError, error:
                    logging.exception('An error occurred: %s' % error)
                    permissions = None
        return permissions


class SpreadsheetFileModel(object):
    id = None
    filename = None
    lastEdit = None
    owner = None
    isDeleted = None
    worksheets = []
    defaultWorksheetId = None
    users = []
    def __init__(self, gModel):
        self.id = gModel['id']
        self.filename = gModel['title']
        self.lastEdit = gModel['modifiedDate']
        self.isDeleted = gModel['labels']['trashed']
        self.owner = gModel['ownerNames'][0]
        self.worksheets = self.get_worksheets_from_pros(gModel.get('properties'))
        #по дефолту - первым загружается начальный лист
        self.defaultWorksheetId = len(self.worksheets) > 0 and self.worksheets[0] or None
        self.users = self.get_users_from_props(gModel.get('properties'))

    def toJSON(self):
        return json.dumps(self, default=lambda o: self.__dict__)

    def get_users_from_props(self, properties):
        result = []
        if properties is None:
            return result
        for p in properties:
            if not re.match('^user__', p.get('key')):
                continue
            onlyRole = p.get('key')
            onlyRole = onlyRole.split("__")[1]
            result.append({'role': onlyRole,
                           'value': p.get('value'),
                           'added': True}
            )
        return result

    def get_worksheets_from_pros(self, properties):
        result = []
        if properties is None:
            return result
        for p in properties:
            if not re.match('^ws__', p.get('key')):
                continue
            result.append(p.get('value'))
        return result

class SpreadsheetStorage(RequestHandler):
    test_files = [
        '0B_dYv0Q_Y9SLdWp4S2FiZlgxN1E',
        '0B_dYv0Q_Y9SLMUhsRmYtZnZqYzQ',
        '0B_dYv0Q_Y9SLa2Y5azlUQzhsVTg'
    ]

    def initialize(self, action):
        self.action = action

    def get(self):
        id_document = self.get_argument('id', default=False)
        gDrive = GoogleDrive(self.get_secure_cookie("user"))

        #fId = gDrive.create_spreadsheet('')
        if id_document:
            try:
                document = gDrive.retrieve_spreadsheet_by_id(self.get_secure_cookie("user"), id_document)
            except AccessTokenRefreshError:
                self.redirect('/google_auth/')
            if not document:
                self.send_error(404)
            else:
                self.write(json.dumps(SpreadsheetFileModel(document).__dict__))
        else:
            try:
                fileList = gDrive.retrieve_spreadsheets(self.get_secure_cookie("user"))
            except AccessTokenRefreshError:
                self.redirect('/google_auth/')
            fileList = self.processGoogleList(fileList)
            #logging.debug('spreadsheet list %s' % fileList)
            self.write(fileList)

        self.finish()

    def post(self):
        """
        Create new spreadsheet or copy/delete by id_document
        """
        ids_document = self.get_argument('ids', default=False)
        if ids_document:
            ids_document = ids_document.split(',')
        gDrive = GoogleDrive(self.get_secure_cookie("user"))
        if self.action == 'copy' and ids_document:
            result = self.processGoogleList(gDrive.copy_spreadsheets(ids_document, self.get_secure_cookie("user")) or [])
        elif self.action == 'delete' and ids_document:
            result = json.dumps(gDrive.delete_spreadsheets(ids_document, self.get_secure_cookie("user")) or [])
        else:
            spreadSheet = gDrive.create_spreadsheet('')
            logging.debug('Spreadsheets created success!')
            result = SpreadsheetFileModel(spreadSheet).toJSON()
        self.write(result)

    def processGoogleList(self, list):
        result = []
        for item in list:
            result.append(SpreadsheetFileModel(item).__dict__)
        return json.dumps(result)

    def check_xsrf_cookie(self):
        pass


class WorksheetHandler(RequestHandler):
    def check_xsrf_cookie(self):
        pass

    def post(self):
        masterDriveId = self.get_argument('masterDriveId', None)
        workSheet = None
        if masterDriveId:
            gDrive = GoogleDrive(self.get_secure_cookie("user"))
            workSheet = gDrive.create_worksheet(masterDriveId)
            #TODO workSheetId['id']
        self.write(json.dumps({'workSheetId': workSheet.get('id')}))


class GoogleOAuth(RequestHandler):
    """
    User auth and obtaining permissions to perform API requests
    """
    __scope = [
        'email',
        'profile',
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/drive.file'
    ]


    def get(self):
        flow = OAuth2WebServerFlow(client_id=CLIENT_ID,
                                   client_secret=CLIENT_SECRET,
                                   scope=self.__scope,
                                   redirect_uri=REDIRECT_URI,
                                   access_type='offline',
                                   include_granted_scopes='true')

        code = self.get_argument('code', None)
        if not code:
            error = self.get_argument('error', None)
            if error:
                self.write(error)
                return

            auth_url = flow.step1_get_authorize_url()
            self.redirect(auth_url)
        else:
            credentials = flow.step2_exchange(code)

            http = httplib2.Http()
            http = credentials.authorize(http)
            serve = discovery.build("plus", "v1", http=http)
            user = serve.people().get(userId='me').execute(http=http)
            email = user.get('emails')[0]['value']
            storage = Storage('oasis_app', email)
            old_credentials = False
            try:
                old_credentials = storage.get()
            except Exception:
                pass
            if not credentials.refresh_token and old_credentials:
                if not old_credentials.refresh_token:
                    http.request('https://accounts.google.com/o/oauth2/revoke?token=%s' % credentials.access_token, method='GET')
                    self.redirect('/google_auth/')
                    return
                credentials.refresh_token = old_credentials.refresh_token
            storage.put(credentials)

            self.set_secure_cookie("user", str(email))
            self.set_secure_cookie("token", str(credentials.access_token))
            self.redirect("/application", {'user': {'email': email, 'token': str(credentials.access_token)}}
            )
            #self.redirect("/spreadsheets/list")

    def post(self, *args, **kwargs):
        #return refresh token
        gDrive = GoogleDrive(self.get_secure_cookie("user"))
        try:
            r_token = gDrive.get_refresh_access_token(self.get_secure_cookie("user"))
        except AccessTokenRefreshError:
            self.redirect('/google_auth/')
        self.write(json.dumps({'access_token': r_token}))

    def check_xsrf_cookie(self):
        pass


class ShareHandler(RequestHandler):
    def check_xsrf_cookie(self):
        pass
    def post(self, *args, **kwargs):

        #TODO: fixed in future, now hardcoded by link to doc
        table_master_id = self.get_argument('table_master_id', None)
        link_to_doc = self.get_argument('link_to_doc', None)

        users = self.get_argument('users', None)
        if table_master_id is None or users is None:
            self.write({'error': 'true',
                        'msg': 'Parameters are empty'})
            return

        users = json.loads(users)
        gDrive = GoogleDrive(self.get_secure_cookie("user"))
        gDrive.sharing_file(self.get_secure_cookie("user"), table_master_id, link_to_doc, users)
        self.write({'error': 'false', 'msg': 'Success'})
